var config = {
  module: {
    loaders: [
      {
        test: /\.(css|scss)$/,
        loaders: ['style', 'css', 'sass'],
      },
      {
        test: /\.(ttf|eot|svg|woff2|woff)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: 'file-loader',
      },
      {
        test: /\.(jpe?g|png|gif|svg)$/i,
        loaders: [
          'file?hash=sha512&digest=hex&name=[hash].[ext]',
          'image-webpack?bypassOnDebug&optimizationLevel=7&interlaced=false',
        ],
      },
    ],
  },
  resolve: {
    alias: []
  }
};

config.resolve.alias['react-router-original'] = require.resolve('react-router');
// this `rr.js` will replace the Link with our own mock component.
config.resolve.alias['react-router'] = require.resolve('./rr.js');

module.exports = config;
