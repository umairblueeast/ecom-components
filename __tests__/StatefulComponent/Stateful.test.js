/**
 * Created by amna on 1/6/17.
 */

import React from 'react';
import ReactDOM from 'react-dom';

import { mount } from 'enzyme';

import {
  loadedData,
  noData,
  loadingData,
  errorData,
  loadingAndErrorData,
  customDataField,
  customCheckDataFunctionBad,
  customCheckDataFunctionEmpty,
  customCheckDataFunctionFilled
} from '../../__mocks__/StatefulComponent.mock';

import StatefulComponennt from '../../src/StatefulComponent/StatefulComponent.component';

const TestComponent = props => (<div>{props.data.query}</div>);
const ViewerComponent = props => (<div>{props.data.viewer}</div>);
const NestedDataComponent = props => {

  const count = props.data.viewer.sites.edges.length;

  return (<div>{`I have ${count} items`}</div>);
};

describe('StatefulComponennt Test Specifications', () => {

  const checkData = function(props, dataProp) {
    try {
      const data = props.data.viewer.sites.edges;

      if (data && data.length > 0) {
        return true;
      }
    } catch (error) {
      return false;
    }

    return false;
  };

  it('should render successfully with required props', () => {
    const div = document.createElement('div');
    ReactDOM.render(<StatefulComponennt component={TestComponent} data={loadedData} />, div);
  });

  it('when loadedData is passed, state.hasData should be true',  () => {
    const wrapper = mount(<StatefulComponennt
      component={TestComponent}
      data={loadedData}
    />);
    expect(wrapper.state('hasData')).toBeTruthy();
    expect(wrapper.state('error')).toBe(null);
  });

  it('when noData is passed, state.hasData should be false',  () => {
    const wrapper = mount(<StatefulComponennt
      component={TestComponent}
      data={noData}
    />);
    expect(wrapper.state('hasData')).toBeFalsy();
    expect(wrapper.state('error')).toBe(null); // default value of props.error is null
  });

  it('when loadingData is passed, state.loading should be true',  () => {
    const wrapper = mount(<StatefulComponennt
      component={TestComponent}
      data={loadingData}
    />);
    expect(wrapper.state('loading')).toBeTruthy();
    expect(wrapper.state('error')).toBe(null); // default value of props.error is null
  });

  it('when errorData is passed, state.error should NOT be null',  () => {
    const wrapper = mount(<StatefulComponennt
      component={TestComponent}
      data={errorData}
    />);
    expect(wrapper.state('loading')).toBeFalsy();
    expect(wrapper.state('error')).not.toBe(null); // default value of props.error is null
  });

  it('when loadingAndErrorData is passed, state.error should NOT be null' +
    'and state.loading should be true',  () => {
    const wrapper = mount(<StatefulComponennt
      component={TestComponent}
      data={loadingAndErrorData}
    />);
    expect(wrapper.state('loading')).toBeTruthy();
    expect(wrapper.state('error')).not.toBe(null); // default value of props.error is null
  });

  it('when customDataField is passed, state.hasData should be true',  () => {
    const wrapper = mount(<StatefulComponennt
      component={ViewerComponent}
      data={customDataField}
      dataField="viewer"
    />);
    expect(wrapper.state('hasData')).toBeTruthy();
    expect(wrapper.state('error')).not.toBe(null); // default value of props.error is null
  });

  it('when customCheckDataFunctionEmpty is passed, state.hasData should be false ',  () => {
    const wrapper = mount(<StatefulComponennt
      component={NestedDataComponent}
      data={customCheckDataFunctionEmpty}
      checkData={checkData}
    />);
    expect(wrapper.state('hasData')).toBeFalsy();
    expect(wrapper.state('error')).not.toBe(null); // default value of props.error is null
  });

  it('when customCheckDataFunctionFilled is passed, state.hasData should be true ',  () => {
    const wrapper = mount(<StatefulComponennt
      component={NestedDataComponent}
      data={customCheckDataFunctionFilled}
      checkData={checkData}
    />);
    expect(wrapper.state('hasData')).toBeTruthy();
    expect(wrapper.state('error')).not.toBe(null); // default value of props.error is null
  });

  it('when customCheckDataFunctionBad is passed, state.hasData should be false ',  () => {
    const wrapper = mount(<StatefulComponennt
      component={NestedDataComponent}
      data={customCheckDataFunctionBad}
      checkData={checkData}
    />);
    expect(wrapper.state('hasData')).toBeFalsy();
    expect(wrapper.state('error')).not.toBe(null); // default value of props.error is null
  });
});
