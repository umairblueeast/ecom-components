/**
 * Created by amna on 1/5/17.
 */

import React from 'react';
import ReactDOM from 'react-dom';
import TestUtils from 'react-addons-test-utils';

import { shallow } from 'enzyme';

import data from '../../__mocks__/ComponentState.mock';

import ComponentState from '../../src/ComponentState/ComponentState.component';

describe('ComponentState Component Test Specifications', () => {
  it('should render successfully without passing any prop', () => {
    const div = document.createElement('div');
    ReactDOM.render(<ComponentState />, div);

  });

  it('should render successfully without passing any prop', () => {
    const div = document.createElement('div');
    ReactDOM.render(<ComponentState
      image={data.image}
      imageSize={data.imageSize}
      card={data.card}
      title={data.title}
      description={data.description}
      linkTag={data.linkTag}
      linkText={data.linkText}
      linkOnClick={() => console.log("I'm clicked!")}
    />, div);

  });
  it('should contain Card tag', () => {
    const cardWrapper = shallow(<ComponentState
      image={data.image}
      imageSize={data.imageSize}
      card={data.card}
      title={data.title}
      description={data.description}
      linkTag={data.linkTag}
      linkText={data.linkText}
      linkOnClick={() => console.log("I'm clicked!")}
    />);
    expect(cardWrapper.find('Card').length).toEqual(1);

  });
  it('should NOT contain CardImg tag, when image is not passed', () => {
    const card = false;
    const cardWrapper = shallow(<ComponentState
      imageSize={data.imageSize}
      card={card}
      title={data.title}
      description={data.description}
      linkTag={data.linkTag}
      linkText={data.linkText}
      linkOnClick={() => console.log("I'm clicked!")}
    />);
    expect(cardWrapper.find('CardImg').length).toEqual(0);

  });
  it('div with card-title class should not exist when title prop is not passed.', () => {
    const component = TestUtils.renderIntoDocument( <ComponentState
      imageSize={data.imageSize}
      card={data.card}
      description={data.description}
      linkTag={data.linkTag}
      linkText={data.linkText}
      linkOnClick={() => console.log("I'm clicked!")}
    />);

    const titleDiv = TestUtils.scryRenderedDOMComponentsWithClass(component, 'card-title');

    expect(TestUtils.isCompositeComponent(component)).toBeTruthy();
    expect(titleDiv.length).toEqual(0);
  });

  it('element with card-text class should not exist when description prop is not passed.', () => {
    const component = TestUtils.renderIntoDocument( <ComponentState
      imageSize={data.imageSize}
      card={data.card}
      linkTag={data.linkTag}
      linkText={data.linkText}
      linkOnClick={() => console.log("I'm clicked!")}
    />);

    const descriptionDiv = TestUtils.scryRenderedDOMComponentsWithClass(component, 'card-text');

    expect(TestUtils.isCompositeComponent(component)).toBeTruthy();
    expect(descriptionDiv.length).toEqual(0);
  });

  it('Link element should not exist when Link prop is not passed.', () => {
    const cardWrapper = shallow(<ComponentState
      imageSize={data.imageSize}
      card={data.card}
      title={data.title}
      description={data.description}
      linkTag={data.linkTag}
      linkText={data.linkText}
      linkOnClick={() => console.log("I'm clicked!")}
    />);

    expect(cardWrapper.find('Links').length).toEqual(0);
  });

});
