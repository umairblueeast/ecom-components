/**
 * Created by anser on 12/30/16.
 */

import React from 'react';
import expect from 'expect';
import { shallow } from 'enzyme';
import ListViewItemImage from '../../src/ListView/ListViewItemImage';

describe('List View Item Image', () => {

  it('should find `div` in ListViewItemImage component', () => {
    const wrapper = shallow(<ListViewItemImage />);
    expect(wrapper.find('div').length).toEqual(1);
  });

});
