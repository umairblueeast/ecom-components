/**
 * Created by anser on 12/30/16.
 */

import React from 'react';
import expect from 'expect';
import { shallow } from 'enzyme';
import Image from '../../src/ListView/Image';

describe('Image', () => {

  it('should find `Media` in Image component', () => {
    const wrapper = shallow(<Image />);
    expect(wrapper.find('Media').length).toEqual(1);
  });

});
