/**
 * Created by anser on 12/30/16.
 */

import React from 'react';
import expect from 'expect';
import { shallow } from 'enzyme';
import ListViewItem from '../../src/ListView/ListViewItem';

describe('List View Item', () => {

  it('should find `ListGroupItem` in ListViewItem component', () => {
    const wrapper = shallow(<ListViewItem />);
    expect(wrapper.find('ListGroupItem').length).toEqual(1);
  });

});
