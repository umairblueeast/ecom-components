import React from 'react';
import expect from 'expect';
import { shallow } from 'enzyme';
import ListView from '../../src/ListView/ListView';

describe('List View', () => {

  it('should find `ListGroup` in ListView component', () => {
    const wrapper = shallow(<ListView />);
    expect(wrapper.find('ListGroup').length).toEqual(1);
  });

});
