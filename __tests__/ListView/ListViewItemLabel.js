/**
 * Created by anser on 12/30/16.
 */

import React from 'react';
import expect from 'expect';
import { shallow } from 'enzyme';
import ListViewItemLabel from '../../src/ListView/ListViewItemLabel';

describe('List View Item Label', () => {

  it('should find `div` in ListViewItemLabel component', () => {
    const wrapper = shallow(<ListViewItemLabel />);
    expect(wrapper.find('div').length).toEqual(1);
  });

});
