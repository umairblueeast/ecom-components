/**
 * Created by anser on 12/30/16.
 */

import React from 'react';
import expect from 'expect';
import { shallow } from 'enzyme';
import DataListItemLabel from '../../src/DataList/DataListItemLabel';

describe('Data List Item Label', () => {

  it('should find `div` in DataListItemLabel component', () => {
    const wrapper = shallow(<DataListItemLabel />);
    expect(wrapper.find('div').length).toEqual(1);
  });

});
