/**
 * Created by anser on 12/30/16.
 */

import React from 'react';
import expect from 'expect';
import { shallow } from 'enzyme';
import DataListItemValue from '../../src/DataList/DataListItemValue';

describe('Data List Item Image', () => {

  it('should find `div` in DataListItemValue component', () => {
    const wrapper = shallow(<DataListItemValue />);
    expect(wrapper.find('div').length).toEqual(1);
  });

});
