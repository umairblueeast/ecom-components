import React from 'react';
import expect from 'expect';
import { shallow } from 'enzyme';
import DataList from '../../src/DataList/DataList';

describe('Data List', () => {

  it('should find `ListGroup` in DataList component', () => {
    const wrapper = shallow(<DataList />);
    expect(wrapper.find('ListGroup').length).toEqual(1);
  });

});
