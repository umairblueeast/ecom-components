import React from 'react';
import expect from 'expect';
import { shallow } from 'enzyme';
import { Icon } from 'react-fa';

import { withConnectedStatus, withDisConnectedStatus, withoutAction, wifiProtected, withoutStatusIcon, withoutNavigation, withoutLabel, withoutValue } from '../../__mocks__/DataList.mock';

import DataList from '../../src/DataList/DataList';
import DataListItem from '../../src/DataList/DataListItem';
import DataListItemLabel from '../../src/DataList/DataListItemLabel';
import DataListItemValue from '../../src/DataList/DataListItemValue';


describe('Custom structure unit test specificatons', () => {

  it('datalist with connected status should render successfully', () => {

    const DataListComponentWrapper = shallow(
      <DataList onClick={() => 'this should be returned'}>
        <DataListItem tag={withConnectedStatus.tag} to={withConnectedStatus.url} action={withConnectedStatus.action} chevronRight={withConnectedStatus.chevronRight}>
          <DataListItemLabel>{withConnectedStatus.label}</DataListItemLabel>
          <DataListItemValue>{withConnectedStatus.value} <Icon className={withConnectedStatus.iconClass} /></DataListItemValue>
        </DataListItem>
      </DataList>
    );
    expect(DataListComponentWrapper.instance().props.onClick()).toBe('this should be returned');

  });

  it('datalist with disconnected status should render successfully', () => {

    const DataListComponentWrapper = shallow(
      <DataList onClick={() => 'this should be returned'}>
        <DataListItem tag={withDisConnectedStatus.tag} to={withDisConnectedStatus.url} action={withDisConnectedStatus.action} chevronRight={withConnectedStatus.chevronRight}>
          <DataListItemLabel>{withDisConnectedStatus.label}</DataListItemLabel>
          <DataListItemValue>{withDisConnectedStatus.value} <Icon className={withConnectedStatus.iconClass} /></DataListItemValue>
        </DataListItem>
      </DataList>
    );
    expect(DataListComponentWrapper.instance().props.onClick()).toBe('this should be returned');

  });

  it('datalist without action should render successfully', () => {

    const DataListComponentWrapper = shallow(
      <DataList onClick={() => 'this should be returned'}>
        <DataListItem tag={withoutAction.tag} to={withoutAction.url} action={withoutAction.action} chevronRight={withoutAction.chevronRight}>
          <DataListItemLabel>{withoutAction.label}</DataListItemLabel>
          <DataListItemValue>{withoutAction.value} <Icon className={withoutAction.iconClass} /></DataListItemValue>
        </DataListItem>
      </DataList>
    );
    expect(DataListComponentWrapper.instance().props.onClick()).toBe('this should be returned');

  });

  it('Wi-fi protected datalist should render successfully', () => {

    const DataListComponentWrapper = shallow(
      <DataList onClick={() => 'this should be returned'}>
        <DataListItem tag={wifiProtected.tag} to={wifiProtected.url} action={wifiProtected.action} chevronRight={wifiProtected.chevronRight}>
          <DataListItemLabel>{wifiProtected.label}</DataListItemLabel>
          <DataListItemValue>{wifiProtected.value} <Icon className={wifiProtected.iconClass} /></DataListItemValue>
        </DataListItem>
      </DataList>
    );
    expect(DataListComponentWrapper.instance().props.onClick()).toBe('this should be returned');

  });

  it('datalist without status icon should render successfully', () => {

    const DataListComponentWrapper = shallow(
      <DataList onClick={() => 'this should be returned'}>
        <DataListItem tag={withoutStatusIcon.tag} to={withoutStatusIcon.url} action={withoutStatusIcon.action} chevronRight={withoutStatusIcon.chevronRight}>
          <DataListItemLabel>{withoutStatusIcon.label}</DataListItemLabel>
          <DataListItemValue>{withoutStatusIcon.value} <Icon className={withoutStatusIcon.iconClass} /></DataListItemValue>
        </DataListItem>
      </DataList>
    );
    expect(DataListComponentWrapper.instance().props.onClick()).toBe('this should be returned');

  });

  it('datalist without navigation should render successfully', () => {

    const DataListComponentWrapper = shallow(
      <DataList onClick={() => 'this should be returned'}>
        <DataListItem tag={withoutNavigation.tag} to={withoutNavigation.url} action={withoutNavigation.action} chevronRight={withoutNavigation.chevronRight}>
          <DataListItemLabel>{withoutNavigation.label}</DataListItemLabel>
          <DataListItemValue>{withoutNavigation.value} <Icon className={withoutNavigation.iconClass} /></DataListItemValue>
        </DataListItem>
      </DataList>
    );
    expect(DataListComponentWrapper.instance().props.onClick()).toBe('this should be returned');

  });

  it('datalist without label should render successfully', () => {

    const DataListComponentWrapper = shallow(
      <DataList onClick={() => 'this should be returned'}>
        <DataListItem tag={withoutLabel.tag} to={withoutLabel.url} action={withoutLabel.action} chevronRight={withoutLabel.chevronRight}>
          <DataListItemLabel>{withoutLabel.label}</DataListItemLabel>
          <DataListItemValue>{withoutLabel.value} <Icon className={withoutLabel.iconClass} /></DataListItemValue>
        </DataListItem>
      </DataList>
    );
    expect(DataListComponentWrapper.instance().props.onClick()).toBe('this should be returned');

  });

  it('datalist without value should render successfully', () => {

    const DataListComponentWrapper = shallow(
      <DataList onClick={() => 'this should be returned'}>
        <DataListItem tag={withoutValue.tag} to={withoutValue.url} action={withoutValue.action} chevronRight={withoutValue.chevronRight}>
          <DataListItemLabel>{withoutValue.label}</DataListItemLabel>
          <DataListItemValue>{withoutValue.value} <Icon className={withoutValue.iconClass} /></DataListItemValue>
        </DataListItem>
      </DataList>
    );
    expect(DataListComponentWrapper.instance().props.onClick()).toBe('this should be returned');

  });

});
