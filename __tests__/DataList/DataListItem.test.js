/**
 * Created by anser on 12/30/16.
 */

import React from 'react';
import expect from 'expect';
import { shallow } from 'enzyme';
import DataListItem from '../../src/DataList/DataListItem';

describe('Data List Item', () => {

  it('should find `ListGroupItem` in DataListItem component', () => {
    const wrapper = shallow(<DataListItem />);
    expect(wrapper.find('ListGroupItem').length).toEqual(1);
  });

});
