/**
 * Created by amna on 1/4/17.
 */

import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';

import data from '../../../__mocks__/IconCard.mock';

import IconCard from '../../../src/Card/IconCard/IconCard.component';

describe('IconCard Component Test Specifications', () => {

  it('should render successfully without passing any prop', () => {
    const div = document.createElement('div');
    ReactDOM.render(<IconCard />, div);

  });

  it('CardTitle element count should be 0 when title is not passed.', () => {
    const cardWrapper = shallow(<IconCard
      color={data.color}
      icon={data.icon}
      description={data.description}
    />);
    expect(cardWrapper.find('CardTitle').length).toEqual(0);
  });

  it('CardText element count should be 0 when description is not passed.', () => {
    const cardWrapper = shallow(<IconCard
      title={data.title}
      color={data.color}
      icon={data.icon}
    />);
    expect(cardWrapper.find('CardText').length).toEqual(0);
  });
});
