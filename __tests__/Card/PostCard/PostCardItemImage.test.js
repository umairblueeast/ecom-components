/**
 * Created by amna on 01/06/17.
 */

import React from 'react';
import expect from 'expect';
import { shallow } from 'enzyme';
import PostCardItemImage from '../../../src/Card/PostCard/PostCardItemImage';

describe('PostCardItem test secifications', () => {

  it('should find PostCardItemImage in PostCardItemContent component', () => {
    const roundImage = true;
    const component = shallow(<PostCardItemImage
      image="https://s3-us-west-2.amazonaws.com/bluerainimages/default-user.jpg"
      roundImage={roundImage} />
    );
    expect(component.instance().props.image).toEqual('https://s3-us-west-2.amazonaws.com/bluerainimages/default-user.jpg');
  });

});
