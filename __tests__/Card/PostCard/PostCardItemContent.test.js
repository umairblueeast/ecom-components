/**
 * Created by amna on 01/06/17.
 */

import React from 'react';
import expect from 'expect';
import TestUtils from 'react-addons-test-utils';
import { shallow } from 'enzyme';
import PostCardItemContent from '../../../src/Card/PostCard/PostCardItemContent';
import PostCardItemImage from '../../../src/Card/PostCard/PostCardItemImage';

describe('PostCardItem test secifications', () => {

  it('should find PostCardItemImage in PostCardItemContent component', () => {
    const roundImage = true;
    const component = shallow( <PostCardItemContent>
      <PostCardItemImage
        image="https://s3-us-west-2.amazonaws.com/bluerainimages/default-user.jpg"
        roundImage={roundImage}
      />
    </PostCardItemContent>);

    expect(component.find('PostCardItemImage').length).toEqual(1);
  });

  // it('should find Icon in PostCardItemContent component', () => {
  //   const roundImage = true;
  //   const component = TestUtils.renderIntoDocument( <PostCardItemContent chevronRight>
  //     <PostCardItemImage
  //       image="https://s3-us-west-2.amazonaws.com/bluerainimages/default-user.jpg"
  //       roundImage={roundImage}
  //     />
  //   </PostCardItemContent>);
  //
  //   const imageDiv = TestUtils.scryRenderedDOMComponentsWithClass(component, 'card-content');
  //
  //  // expect(TestUtils.isCompositeComponent(component)).toBeTruthy();
  //   expect(imageDiv.length).toEqual(1);
  // });

});
