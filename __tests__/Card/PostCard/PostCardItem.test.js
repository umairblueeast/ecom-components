/**
 * Created by amna on 1/5/17.
 */

import React from 'react';
import expect from 'expect';
import TestUtils from 'react-addons-test-utils';
import PostCardItem from '../../../src/Card/PostCard/PostCardItem';

describe('PostCardItem test secifications', () => {

  it('should find `PostCardItem` in PostCardItem component', () => {

    const component = TestUtils.renderIntoDocument( <PostCardItem />);

    const cardDiv = TestUtils.scryRenderedDOMComponentsWithClass(component, 'timeline-card');

    expect(TestUtils.isCompositeComponent(component)).toBeTruthy();
    expect(cardDiv.length).toEqual(1);
  });

});
