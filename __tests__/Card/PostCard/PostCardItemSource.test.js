/**
 * Created by amna on 01/06/17.
 */

import React from 'react';
import expect from 'expect';
import { shallow } from 'enzyme';
import PostCardItemSource from '../../../src/Card/PostCard/PostCardItemSource';

describe('PostCardItemSource test secifications', () => {

  it('should contain div with className="text-muted"', () => {
    const component = shallow(<PostCardItemSource />
    );

    expect(component.containsMatchingElement(
      <div className="text-muted" />
    )).toEqual(true);
  });

});
