/**
 * Created by amna on 01/06/17.
 */

import React from 'react';
import expect from 'expect';
import  { Link } from 'react-router';
import { shallow } from 'enzyme';
import PostCardItemBody from '../../../src/Card/PostCard/PostCardItemBody';

describe('PostCardItem test secifications', () => {

  it('should find Link tag in PostCardItemContent component', () => {
    const component = shallow( <PostCardItemBody>
      <div><strong><Link href="#">Amna Mukhtar</Link></strong></div>
    </PostCardItemBody>);

    expect(component.find('Link').length).toEqual(1);
  });

});
