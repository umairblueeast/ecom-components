/**
 * Created by amna on 1/5/17.
 */

import React from 'react';
import ReactDOM from 'react-dom';
import TestUtils from 'react-addons-test-utils';
import { Link } from 'react-router';
import { Icon } from 'react-fa';
import moment from 'moment';
import { shallow } from 'enzyme';

import { dataWithRoundImage, dataWithImage } from '../../../__mocks__/PostCard.mock';

import PostCard from '../../../src/Card/PostCard/PostCard.component';

// imorts for custom structure
import PostCardItem from '../../../src/Card/PostCard/PostCardItem';
import PostCardItemContent from '../../../src/Card/PostCard/PostCardItemContent';
import PostCardItemImage from '../../../src/Card/PostCard/PostCardItemImage';
import PostCardItemBody from '../../../src/Card/PostCard/PostCardItemBody';
import PostCardItemSource from '../../../src/Card/PostCard/PostCardItemSource';

describe('PostCard Component Test Specifications', () => {

  describe('Fixed structure unit test specifications', () => {
    it('should render successfully without passing any prop', () => {
      const div = document.createElement('div');
      ReactDOM.render(<PostCard />, div);

    });

    it('should render successfully when all props are passed', () => {
      const div = document.createElement('div');
      ReactDOM.render(<PostCard
        initiator={dataWithRoundImage.initiator}
        description={dataWithRoundImage.description}
        time={dataWithRoundImage.time}
        image={dataWithRoundImage.image}
        roundImage={dataWithRoundImage.roundImage}
        source={dataWithRoundImage.source}
        userProfileLink={dataWithRoundImage.userProfileLink}
        linkTag={Link}
        to={dataWithRoundImage.to}
        onClick={() => 'action called'}  // todo: it is being called twice
      />, div);
    });

    it('PostCardItemImage element should exist when image prop is passed', () => {
      const cardWrapper = shallow(<PostCard
        initiator={dataWithRoundImage.initiator}
        description={dataWithRoundImage.description}
        time={dataWithRoundImage.time}
        image={dataWithRoundImage.image}
        roundImage={dataWithRoundImage.roundImage}
        source={dataWithRoundImage.source}
        userProfileLink={dataWithRoundImage.userProfileLink}
        linkTag={Link}
        to={dataWithRoundImage.to}
        onClick={() => 'action called'}
      />);
      expect(cardWrapper.find('PostCardItemImage').length).toEqual(1);
    });
    it('card-image class should exist if roundImage prop is true', () => {

      const component = TestUtils.renderIntoDocument( <PostCard
        initiator={dataWithRoundImage.initiator}
        description={dataWithRoundImage.description}
        time={dataWithRoundImage.time}
        image={dataWithRoundImage.image}
        roundImage={dataWithRoundImage.roundImage}
        source={dataWithRoundImage.source}
        userProfileLink={dataWithRoundImage.userProfileLink}
        linkTag={Link}
        to={dataWithRoundImage.to}
        onClick={() => 'action called'}
      />);

      const imageDiv = TestUtils.scryRenderedDOMComponentsWithClass(component, 'card-image');

      expect(TestUtils.isCompositeComponent(component)).toBeTruthy();
      expect(imageDiv.length).toEqual(1);
    });

    it('card-image class should NOT exist if roundImage prop is false', () => {

      const component = TestUtils.renderIntoDocument( <PostCard
        initiator={dataWithImage.initiator}
        description={dataWithImage.description}
        time={dataWithImage.time}
        image={dataWithImage.image}
        roundImage={dataWithImage.roundImage}
        source={dataWithImage.source}
        userProfileLink={dataWithImage.userProfileLink}
        linkTag={Link}
        to={dataWithImage.to}
        onClick={() => 'action called'}
      />);

      const imageDiv = TestUtils.scryRenderedDOMComponentsWithClass(component, 'card-image');

      expect(TestUtils.isCompositeComponent(component)).toBeTruthy();
      expect(imageDiv.length).toEqual(0);
    });

    it('strong tag should not exist if initiator prop is not passed', () => {
      const cardWrapper = shallow(<PostCard
        description={dataWithRoundImage.description}
        time={dataWithRoundImage.time}
        image={dataWithRoundImage.image}
        roundImage={dataWithRoundImage.roundImage}
        source={dataWithRoundImage.source}
        userProfileLink={dataWithRoundImage.userProfileLink}
        linkTag={Link}
        to={dataWithRoundImage.to}
        onClick={() => 'action called'}  // todo: it is being called twice
      />);
      expect(cardWrapper.find('strong').length).toEqual(0);
    });

    it('PostCardItemImage tag should NOT exist if image is not passed', () => {
      const cardWrapper = shallow(<PostCard
        description={dataWithRoundImage.description}
        time={dataWithRoundImage.time}
        roundImage={dataWithRoundImage.roundImage}
        source={dataWithRoundImage.source}
        userProfileLink={dataWithRoundImage.userProfileLink}
        linkTag={Link}
        to={dataWithRoundImage.to}
        onClick={() => 'action called'}  // todo: it is being called twice
      />);
      expect(cardWrapper.find('PostCardItemImage').length).toEqual(0);
    });

    it('Link tag should NOT exist if linkTag is not passed', () => {
      const cardWrapper = shallow(<PostCard
        description={dataWithRoundImage.description}
        time={dataWithRoundImage.time}
        roundImage={dataWithRoundImage.roundImage}
        source={dataWithRoundImage.source}
        userProfileLink={dataWithRoundImage.userProfileLink}
        onClick={() => 'action called'}  // todo: it is being called twice
      />);
      expect(cardWrapper.find('Link').length).toEqual(0);
    });

    it('provided method definition should be called through onClick', () => {

      const cardWrapper = shallow(<PostCard
        description={dataWithRoundImage.description}
        time={dataWithRoundImage.time}
        image={dataWithRoundImage.image}
        roundImage={dataWithRoundImage.roundImage}
        source={dataWithRoundImage.source}
        userProfileLink={dataWithRoundImage.userProfileLink}
        linkTag={Link}
        to={dataWithRoundImage.to}
        onClick={() => 'action called'}
      />);
      expect(cardWrapper.instance().props.onClick()).toBe('action called');

    });


    describe('Custom structure unit test specificatons', () => {

      it('custom structure 1 should render successfully', () => {

        const cardWrapper = shallow(<PostCardItem onClick={() => 'this should be returned'} >
          <PostCardItemContent tag={Link} to="/toSomePage" chevronRight>
            <PostCardItemImage image={dataWithRoundImage.image} roundImage={dataWithRoundImage.roundImage} />
            <PostCardItemBody>
              <div>
                <strong>
                  <Link href="#">{dataWithRoundImage.initiator}</Link>
                </strong> {dataWithRoundImage.description}
              </div>
              <PostCardItemSource>
                <small><Icon name="clock-o" /><Link to="/toSomePage"> {moment(dataWithRoundImage.time).fromNow()}</Link> · {dataWithRoundImage.source}</small>
              </PostCardItemSource>
            </PostCardItemBody>
          </PostCardItemContent>
        </PostCardItem>);
        expect(cardWrapper.instance().props.onClick()).toBe('this should be returned');

      });

      it('custom structure 2 should render successfully', () => {

        const cardWrapper = shallow(<PostCardItem>
          <PostCardItemContent tag={Link} to="/toSomeLink" chevronRight>
            <PostCardItemImage image={dataWithImage.image} roundImage={dataWithImage.roundImage} />
            <PostCardItemBody>
              <PostCardItemSource>
                <small><Icon name="clock-o" /><Link to="/toSomeLink"> {moment(dataWithImage.time).fromNow()}</Link> · {dataWithImage.source}</small>
              </PostCardItemSource>
              <div>
                <strong>
                  <Link to="#">{dataWithImage.initiator}</Link>
                </strong> {dataWithImage.description}
              </div>
            </PostCardItemBody>
          </PostCardItemContent>
        </PostCardItem>);
        expect(cardWrapper.instance().props.onClick()).not.toBe('this should be returned');

      });
    });

  });

});
