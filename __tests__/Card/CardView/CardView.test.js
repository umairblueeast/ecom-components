/**
 * Created by amna on 1/4/17.
 */

import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';

import data from '../../../__mocks__/CardView.mock';

import CardView from '../../../src/Card/CardView/CardView.component';

describe('CardView Component Test Specifications', () => {

  it('should render successfully without passing any prop', () => {
    const div = document.createElement('div');
    ReactDOM.render(<CardView />, div);

  });

  it('should render successfully when complete card data is provided', () => {
    const div = document.createElement('div');
    ReactDOM.render(<CardView
      title={data.title}
      subtitle={data.subtitle}
      image={data.image}
      linkTag={data.linkTag}
      to={data.to}
    />, div);
  });

  it('props passed should render correctly', () => {

    const cardWrapper = shallow(<CardView
      title={data.title}
      subtitle={data.subtitle}
      image={data.image}
      linkTag={data.linkTag}
      to={data.to}
    />);
    expect(cardWrapper.instance().props.title).toBe(data.title);
    expect(cardWrapper.instance().props.subtitle).toBe(data.subtitle);
    expect(cardWrapper.instance().props.image).toBe(data.image);

  });

  it('CardText element count should be equal to 1 when title is not passed', () => {
    const cardWrapper = shallow(<CardView
      subtitle={data.subtitle}
      image={data.image}
      linkTag={data.linkTag}
      to={data.to}
    />);
    expect(cardWrapper.find('CardText').length).toEqual(1);
  });

  it('CardText element count should be equal to 2 when title is passed', () => {
    const cardWrapper = shallow(<CardView
      title={data.title}
      subtitle={data.subtitle}
      image={data.image}
      linkTag={data.linkTag}
      to={data.to}
    />);
    expect(cardWrapper.find('CardText').length).toEqual(2);
  });

  it('CardText element count should be equal to 1 when subtitle is not passed', () => {
    const cardWrapper = shallow(<CardView
      title={data.title}
      image={data.image}
      linkTag={data.linkTag}
      to={data.to}
    />);
    expect(cardWrapper.find('CardText').length).toEqual(1);
  });

  it('should not contain image, image prop on rendered component will be empty', () => {

    const cardWrapper = shallow(<CardView
      title={data.title}
      linkTag={data.linkTag}
      to={data.to}
    />);
    expect(cardWrapper.instance().props.image).toEqual('');

  });
});
