import React from 'react';
import ReactDOM from 'react-dom';

import { shallow } from 'enzyme';

import LineMiniChartCard from '../../../src/Card/MiniChartCard/LineMiniChartCard.component';
import data from './../../../__mocks__/MiniChartCard.mock';

it('should render successfully when complete card data is provided', () => {
  const div = document.createElement('div');
  ReactDOM.render( <LineMiniChartCard
    label={data.label}
    value={data.value}
    data={data.data}
    onClick={() => 'click me'}
  />, div);
});

it('props passed should render correctly', () => {

  const cardWrapper = shallow(<LineMiniChartCard
    label={data.label}
    value={data.value}
    data={data.data}
    onClick={() => 'click me'}
  />);

  expect(cardWrapper.instance().props.label).toBe(data.label);
  expect(cardWrapper.instance().props.value).toBe(data.value);
  expect(cardWrapper.instance().props.data).toBe(data.data);
});

it('props passed should render correctly', () => {

  const cardWrapper = shallow(<LineMiniChartCard
    value={data.value}
    data={data.data}
    onClick={() => 'click me'}
  />);

  expect(cardWrapper.find('div').length).toEqual(0);
  expect(cardWrapper.instance().props.value).toBe(data.value);
  expect(cardWrapper.instance().props.data).toBe(data.data);
});

it(' Successfull  if Label Prop is not Passed', () => {
  const cardWrapper = shallow(<LineMiniChartCard
    value={data.value}
    data={data.data}
    onClick={() => 'click me'}
  />);
  expect(cardWrapper.containsMatchingElement(
    <div>Average Duration</div>
  )).toEqual(false);
});

it('Successfull if value Prop is not Passed', () => {
  const cardWrapper = shallow(<LineMiniChartCard
    value={data.value}
    data={data.data}
    onClick={() => 'click me'}
  />);
  expect(cardWrapper.containsMatchingElement(
    <div>12.1 h</div>
  )).toEqual(false);
});

it(' Successfull if value Prop is  matched', () => {
  const cardWrapper = shallow(<LineMiniChartCard
    value={data.value}
    onClick={() => 'click me'}
  />);
  expect(cardWrapper.containsMatchingElement(
    <div>12.1 h</div>
  )).toEqual(false);
});

it('provided definition of onClick method should be called', () => {

  const cardWrapper = shallow(<LineMiniChartCard
    label={data.label}
    value={data.value}
    data={data.data}
    onClick={() => 'See i am being called!!'}
  />);
  expect(cardWrapper.instance().props.onClick()).toBe('See i am being called!!');
});
