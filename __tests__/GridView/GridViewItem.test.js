/**
 * Created by amna on 12/7/16.
 */

import React from 'react';
import expect from 'expect';
import { shallow } from 'enzyme';
import GridViewItem from '../../src/GridView/GridViewItem.component';
import CardView from '../../src/Card/CardView/CardView.component';
import data from '../../__mocks__/CardView.mock';

describe('GridViewItem test secifications', () => {

  it('', () => {
    const component = shallow( <GridViewItem>
      <CardView
        title={data.title}
        subtitle={data.subtitle}
        image={data.image}
        linkTag={data.linkTag}
        to={data.to}
        onClick={() => 'Click me!'}
      />
    </GridViewItem>);

    expect(component.find('CardView').length).toEqual(1);
  });

});
