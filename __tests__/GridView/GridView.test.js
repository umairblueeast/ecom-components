/**
 * Created by amna on 1/5/17.
 */

import React from 'react';
import ReactDOM from 'react-dom';

import { shallow } from 'enzyme';

import data from '../../__mocks__/CardView.mock';

import GridViewItem from '../../src/GridView/GridViewItem.component';
import CardView from '../../src/Card/CardView/CardView.component';
import GridView from '../../src/GridView/GridView.component';

describe('GridView Component Test Specifications', () => {
  it('should render successfully without passing any prop', () => {
    const div = document.createElement('div');
    ReactDOM.render(<GridView />, div);

  });

  it('should render successfully without passing any prop', () => {
    const gridView = shallow(<GridView>
      <GridViewItem>
        <CardView
          title={data.title}
          subtitle={data.subtitle}
          image={data.image}
          linkTag={data.linkTag}
          to={data.to}
          onClick={() => 'Click me!'}
        />
      </GridViewItem>
      <GridViewItem>
        <CardView
          title={data.title}
          subtitle={data.subtitle}
          image={data.image}
          linkTag={data.linkTag}
          to={data.to}
          onClick={() => 'Click me!'}
        />
      </GridViewItem>
      <GridViewItem>
        <CardView
          title={data.title}
          subtitle={data.subtitle}
          image={data.image}
          linkTag={data.linkTag}
          to={data.to}
          onClick={() => 'Click me!'}
        />
      </GridViewItem>
      <GridViewItem>
        <CardView
          title={data.title}
          subtitle={data.subtitle}
          image={data.image}
          linkTag={data.linkTag}
          to={data.to}
          onClick={() => 'Click me!'}
        />
      </GridViewItem>
    </GridView>);

    expect(gridView.find('CardView').length).toEqual(4);

  });
});
