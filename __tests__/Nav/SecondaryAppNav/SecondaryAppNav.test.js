/**
 * Created by amna on 1/6/17.
 */

import React from 'react';
import ReactDOM from 'react-dom';
import TestUtils from 'react-addons-test-utils';
import Icon from 'react-fa';

import { shallow } from 'enzyme';

import items from '../../../__mocks__/SecondaryAppNav.mock';

import SecondaryAppNav from '../../../src/Nav/SecondaryAppNav/SecondaryAppNav.component';

describe('PrimaryAppNav Component Test Specifications', () => {

  it('should render successfully without passing any prop', () => {
    const div = document.createElement('div');
    ReactDOM.render(<SecondaryAppNav />, div);

  });

  it('should render successfully without passing any prop', () => {

    const cardWrapper = shallow(<SecondaryAppNav items={items} /> );
    expect(cardWrapper.find('NavItem').length).toEqual(items.length);

  });
});
