/**
 * Created by amna on 1/6/17.
 */

import React from 'react';
import ReactDOM from 'react-dom';
import TestUtils from 'react-addons-test-utils';
import Icon from 'react-fa';

import { shallow } from 'enzyme';

import data from '../../../__mocks__/PrimaryAppNav.mock';

import PrimaryAppNav from '../../../src/Nav/PrimaryAppNav/PrimaryAppNav.component';

describe('PrimaryAppNav Component Test Specifications', () => {

  it('should render successfully without passing any prop', () => {
    const div = document.createElement('div');
    ReactDOM.render(<PrimaryAppNav />, div);

  });

  it('should contain page-title span, when title is passed.', () => {

    const navAppWrapper = shallow(<PrimaryAppNav
      title={data.title}
    />);
    expect(navAppWrapper.containsMatchingElement(
      <span className="page-title">{data.title}</span>
    )).toEqual(true);

  });

  it('should contain prev-page-sperator span, when previous prop is passed.', () => {

    const navAppWrapper = shallow(<PrimaryAppNav
      title={data.title}
      previous={data.previous}
    />);
    expect(navAppWrapper.containsMatchingElement(
      <span className="page-title">{data.title}</span>
    )).toEqual(true);
    expect(navAppWrapper.containsMatchingElement(
      <span className="prev-page-sperator">/</span>
    )).toEqual(true);

  });

  it('should contain page-icon class, when icon prop is passed.', () => {

    const component = TestUtils.renderIntoDocument( <PrimaryAppNav
      title={data.title}
      icon={data.icon}
    /> );

    const iconDiv = TestUtils.scryRenderedDOMComponentsWithClass(component, 'page-icon');

    expect(iconDiv.length).toEqual(1);

  });

  it('should NOT contain Icon with name="arrow-left" when previous prop is not passsed./>', () => {

    const navAppWrapper = shallow(<PrimaryAppNav
      title={data.title}
      icon={data.icon}
    />);
    expect(navAppWrapper.containsMatchingElement(
      <Icon name="arrow-left" />
    )).toEqual(false);
  });

  it('should contain page-title, prev-page-separator and page-icon class, when title, previoud and icon props are passed.', () => {

    const navAppWrapper = shallow(<PrimaryAppNav
      title={data.title}
      previous={data.previous}
      icon={data.icon}
    />);
    expect(navAppWrapper.containsMatchingElement(
      <span className="page-title">{data.title}</span>
    )).toEqual(true);
    expect(navAppWrapper.containsMatchingElement(
      <span className="prev-page-sperator">/</span>
    )).toEqual(true);
    expect(navAppWrapper.containsMatchingElement(
      <span className="page-icon">
        <img src={data.icon} width="30" height="30" className="rounded" alt="Page Icon" />
      </span>
    )).toEqual(true);

  });

  it('should contain page-title, prev-page-separator and page-icon class, when title, previoud and icon props are passed.', () => {

    const navAppWrapper = shallow(<PrimaryAppNav
      title={data.title}
      previous={data.previous}
      icon={data.icon}
    >
      <div className="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
        <div className="btn-group" role="group" aria-label="First group">
          <button type="button" className="btn btn-secondary">1</button>
          <button type="button" className="btn btn-secondary">2</button>
        </div>
        <div className="btn-group" role="group" aria-label="Second group">
          <button type="button" className="btn btn-secondary">5</button>
        </div>
      </div>
    </PrimaryAppNav>);

    expect(navAppWrapper.containsMatchingElement(
      <span className="page-title">{data.title}</span>
    )).toEqual(true);
    expect(navAppWrapper.containsMatchingElement(
      <span className="prev-page-sperator">/</span>
    )).toEqual(true);
    expect(navAppWrapper.containsMatchingElement(
      <span className="page-icon">
        <img src={data.icon} width="30" height="30" className="rounded" alt="Page Icon" />
      </span>
    )).toEqual(true);
    expect(navAppWrapper.containsMatchingElement(
      <div className="btn-group" role="group" aria-label="Second group">
        <button type="button" className="btn btn-secondary">5</button>
      </div>
    )).toEqual(true);

  });
});
