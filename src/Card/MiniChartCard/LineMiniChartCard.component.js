
import React, { PropTypes } from 'react';
import 'bluerain-bootstrap-theme/dist/css/components/mini-chart-card.css';
import { VictoryLine } from 'victory';
import { Card, CardBlock } from 'reactstrap';
import CardBlockLabel from '../CardBlockLabel';
import CardBlockValue from '../CardBlockValue';
import ChartContainer from '../ChartContainer';
import CustomCardLink from '../CustomCardLink';


const propTypes = {
  label: PropTypes.string,
  value: PropTypes.string,
  data: PropTypes.array,
  chartProps: PropTypes.array,
  linkTag: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
  to: PropTypes.string,
  href: PropTypes.string,
  onClick: PropTypes.func
};

const defaultProps = {
  label: '',
  value: '',
  data: [],
  chartProps: [],
  linkTag: 'a',
  to: '',
  href: '',
  onClick: () => {
  }
};


export default class LineMiniChartCard extends React.Component {
  constructor(props) {
    super(props);
    this.handleClickAction = this.handleClickAction.bind(this);
  }

  handleClickAction() {
    this.props.onClick();
  }


  render() {
    const {
    label,
    value,
    data,
    href,
    linkTag,
    to
  } = this.props;

    return (
      <Card  onClick={this.handleClickAction}  className="mini-chart-card card-link-block">
        <CustomCardLink
          tag={linkTag}
          to={to}
          href={href}
        ><CardBlock>
          <CardBlockLabel>{label}</CardBlockLabel>
          <CardBlockValue>{value}</CardBlockValue>
          <ChartContainer>
            <VictoryLine
              data={data}
              interpolation="cardinal"
              height={30}
              padding={{ top:2, bottom:2 }}
              style={{
                data: {
                  stroke:'tomato',
                  strokeWidth: 1,
                }
              }}
            />
          </ChartContainer>
        </CardBlock>
        </CustomCardLink>
      </Card>
    );
  }
}

LineMiniChartCard.propTypes = propTypes;
LineMiniChartCard.defaultProps = defaultProps;

