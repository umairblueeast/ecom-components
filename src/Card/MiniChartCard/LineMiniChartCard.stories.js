import React from 'react';
import { withKnobs, text , } from '@kadira/storybook-addon-knobs';
import { storiesOf, action } from '@kadira/storybook';
import 'bluerain-bootstrap-theme/dist/css/components/mini-chart-card.css';
import LineMiniChartCard from './LineMiniChartCard.component';
import   data     from '../../../__mocks__/MiniChartCard.mock';
const stories = storiesOf('LineMiniChartCard', module);
stories.addDecorator(withKnobs);

const style = {
  width: 300,
};
stories.addWithInfo('LineMiniCardwith  -Knobs', () => {
  const label = text('label', 'Average Duration');
  const value = text('value', '12.1 h');
  return (
    <div style={style}>

      <LineMiniChartCard
        label={label}
        value={value}
        data={data.data}
        linkTag={data.linkTag}
        to={data.to}
        onClick={action('card-click')}
      />
    </div>
  );
});


stories.addWithInfo('Complete Card', () => (
  <div style={style}>

    <LineMiniChartCard
      label={data.label}
      value={data.value}
      data={data.data}
      linkTag={data.linkTag}
      to={data.to}
      onClick={action('card-click')}
    />
  </div>
));


stories.addWithInfo('LineMiniCard without Label', () => (
  <div style={style}>

    <LineMiniChartCard
      value="12.1 h"
      data={data.data}
      linkTag={data.linkTag}
      to={data.to}
      onClick={action('card-click')}
    />
  </div>
));

stories.addWithInfo('LineMiniCard without value ', () => (
  <div style={style}>

    <LineMiniChartCard
      label={data.label}
      data={data.data}
      linkTag={data.linkTag}
      to={data.to}
      onClick={action('card-click')}
    />
  </div>
));

stories.addWithInfo('LineMiniCard without data', () => (
  <div style={style}>
    <LineMiniChartCard
      label={data.label}
      value={data.value}
      linkTag={data.linkTag}
      to={data.to}
      onClick={action('card-click')}
    />
  </div>
));

stories.addWithInfo('LineMiniCard with href', () => (
  <div style={style}>
    <LineMiniChartCard
      label={data.label}
      value={data.value}
      data={data.data}
      href={data.href}
      onClick={action('card-click')}
    />
  </div>
));
