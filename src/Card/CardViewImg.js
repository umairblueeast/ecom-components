/**
 * Created by amna on 12/7/16.
 */


import React, { PropTypes } from 'react';

const propTypes = {
  tag: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
  className: PropTypes.string,
  image: PropTypes.string
};

const defaultProps = {
  tag: 'div',
  image: ''
};

const CardViewImg = (props) => {
  const {
    tag: Tag,
    image,
    ...attributes
  } = props;
  return (
    <Tag {...attributes} className="card-img-square card-img-top" style={{ backgroundImage: `url( ${image} )` }} />
  );
};

CardViewImg.propTypes = propTypes;
CardViewImg.defaultProps = defaultProps;

export default CardViewImg;
