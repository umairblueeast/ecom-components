/**
 * Created by anser on 12/7/16.
 */


import React, { PropTypes } from 'react';

const propTypes = {
  tag: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
  onClick: PropTypes.func
};

const defaultProps = {
  tag: 'div',
  onClick: () => {
  }
};

export default class PostCardItem extends React.Component {

  constructor(props) {
    super(props);
    this.handleClickAction = this.handleClickAction.bind(this);
  }

  handleClickAction() {
    this.props.onClick();
  }

  render() {
    const {
      tag: Tag,
      ...attributes
    } = this.props;
    return (
      <Tag
        {...attributes}
        className="card timeline-card"
        onClick={this.handleClickAction}
      />
    );
  }

};

PostCardItem.propTypes = propTypes;
PostCardItem.defaultProps = defaultProps;
