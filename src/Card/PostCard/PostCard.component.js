import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import moment from 'moment';
import { Icon } from 'react-fa';
import PostCardItem from './PostCardItem';
import PostCardItemContent from './PostCardItemContent';
import PostCardItemImage from './PostCardItemImage';
import PostCardItemBody from './PostCardItemBody';
import PostCardItemSource from './PostCardItemSource';

const propTypes = {
  initiator: PropTypes.string,
  description: PropTypes.string,
  time: PropTypes.string,
  source: PropTypes.string,
  image: PropTypes.string,
  roundImage: PropTypes.bool,
  userProfileLink: PropTypes.string,
  linkTag: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
  to: PropTypes.string,
  href: PropTypes.string,
  onClick: PropTypes.func
};
const defaultProps = {
  initiator: '',
  description: '',
  time: '',
  source: '',
  image: '',
  roundImage: false,
  userProfileLink: undefined,
  linkTag: undefined,
  to: '',
  href: '#',
  onClick: () => {
  }
};

export default class PostCard extends React.Component {

  static renderInitiator(initiator, userProfileLink) {
    if (initiator) {
      return (
        userProfileLink ? <strong><Link href={userProfileLink}>{initiator}</Link></strong>
                        : <strong>{initiator}</strong>
      );
    }
  }
  static  handleNavigation(html, linkTag, to, href) {
    const Tag = linkTag;
    return  (
      linkTag ?
        <Tag href={href} to={to}>{html}</Tag>
          :
        { html }
    );
  }

  static renderTimeAndSource(time, source, linkTag, to, href) {
    let html;
    let finalhtml;
    if (time) {
      html = <span> {moment(time).fromNow()}</span>;
      if (linkTag) {
        html = PostCard.handleNavigation(html, linkTag, to, href);
      }
      if (!source) {
        finalhtml = <small>{html}</small>;
      }
    }
    if (time && source) {
      finalhtml = <small><Icon name="clock-o" /> {html} . {source}</small>;
    } else {
      finalhtml = <small>{source}</small>;
    }
    return finalhtml;
  }

  static renderImage(image, roundImage) {
    if (image) {
      return (<PostCardItemImage image={image} roundImage={roundImage} />);
    }
  }

  render() {
    const {
      initiator,
      description,
      time,
      image,
      roundImage,
      userProfileLink,
      source,
      linkTag,
      to,
      href,
      ...props
    } = this.props;

    return (
      <PostCardItem onClick={this.props.onClick} {...props}>
        <PostCardItemContent tag={linkTag} to={to} href={href} chevronRight>
          {PostCard.renderImage(image, roundImage)}
          <PostCardItemBody>
            <div>{PostCard.renderInitiator(initiator, userProfileLink)} {description}</div>
            <PostCardItemSource>
              {PostCard.renderTimeAndSource(time, source, linkTag, to, href)}
            </PostCardItemSource>
          </PostCardItemBody>
        </PostCardItemContent>
      </PostCardItem>
    );
  }
}

PostCard.propTypes = propTypes;
PostCard.defaultProps = defaultProps;
