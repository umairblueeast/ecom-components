/**
 * Created by anser on 12/7/16.
 */


import React, { PropTypes } from 'react';

const propTypes = {
  tag: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
  className: PropTypes.string
};

const defaultProps = {
  tag: 'div'
};

const PostCardItemSource = (props) => {
  const {
    tag: Tag,
    className,
    ...attributes
  } = props;


  return (
    <Tag {...attributes} className="text-muted" />
  );
};

PostCardItemSource.propTypes = propTypes;
PostCardItemSource.defaultProps = defaultProps;

export default PostCardItemSource;
