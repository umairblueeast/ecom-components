/**
 * Created by anser on 12/7/16.
 */

import React, { PropTypes } from 'react';

const propTypes = {
  tag: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
};

const defaultProps = {
  tag: 'div'
};

const PostCardItemBody = (props) => {
  const {
    tag: Tag,
    ...attributes
  } = props;


  return (
    <Tag {...attributes} className="card-body" />
  );
};

PostCardItemBody.propTypes = propTypes;
PostCardItemBody.defaultProps = defaultProps;

export default PostCardItemBody;
