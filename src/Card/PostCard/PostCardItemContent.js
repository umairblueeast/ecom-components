/**
 * Created by anser on 12/7/16.
 */


import React, { PropTypes } from 'react';
import { Icon } from 'react-fa';

const propTypes = {
  tag: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
  children: PropTypes.oneOfType([PropTypes.func, PropTypes.string, PropTypes.array]),
  holder: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
  chevronRight: PropTypes.bool,
  to: PropTypes.string,
  href: PropTypes.string
};

const defaultProps = {
  tag: undefined,
  children: {},
  holder: 'div',
  chevronRight: true,
  to: '',
  href: ''
};

const PostCardItemContent = (props) => {
  const {
    tag: Tag,
    holder: Holder,
    to,
    href,
    chevronRight,
    children,
    ...attributes
  } = props;
  // Chevron
  let chevron = '';
  if (chevronRight === true && Tag) {
    chevron =
      (<Tag to={to} href={href} className="card-chevron">
        <Icon name="chevron-right" style={{ alignSelf: 'center' }} />
      </Tag>);
  }

  return (
    <Holder {...attributes} className="card-content" >
      {children}
      {chevron}
    </Holder>
  );
};

PostCardItemContent.propTypes = propTypes;
PostCardItemContent.defaultProps = defaultProps;

export default PostCardItemContent;
