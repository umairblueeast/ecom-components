/**
 * Created by amna on 12/23/16.
 */

import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import { withKnobs, text, boolean } from '@kadira/storybook-addon-knobs';
import 'bluerain-bootstrap-theme/dist/css/components/timeline.css';
import { Link } from 'react-router';
import { Icon } from 'react-fa';
import moment from 'moment';
import PostCard from './PostCard.component';
import { dataWithRoundImage, dataWithImage } from '../../../__mocks__/PostCard.mock';

// imorts for custom structure
import PostCardItem from './PostCardItem';
import PostCardItemContent from './PostCardItemContent';
import PostCardItemImage from './PostCardItemImage';
import PostCardItemBody from './PostCardItemBody';
import PostCardItemSource from './PostCardItemSource';

const stories = storiesOf('PostCard', module);
stories.addDecorator(withKnobs);

stories.addWithInfo('Fix Structure: complete card', () => {
  const initiator = text('initiator', 'Amna Mukhtar');
  const description = text('description', 'changed the temperature to 26 degrees.');
  const time = text('time', '2016-11-03T09:14:51Z');
  const image = text('image', 'https://s3-us-west-2.amazonaws.com/bluerainimages/default-user.jpg');
  const source = text('source', 'Web');
  const userProfileLink = text('userProfileLink', '/toUserProfileLink');
  const to = text('to', '/toSomePath');
  const roundImage = boolean('rundImage', true);
  return (
    <PostCard
      initiator={initiator}
      description={description}
      time={time}
      image={image}
      roundImage={roundImage}
      source={source}
      userProfileLink={userProfileLink}
      linkTag={Link}
      to={to}
      onClick={action('on click called')}  // todo: it is being called twice
    />
  );
});
stories.addWithInfo('Fix Structure: without user profile link', () => (
  <PostCard
    initiator={dataWithRoundImage.initiator}
    description={dataWithRoundImage.description}
    time={dataWithRoundImage.time}
    image={dataWithRoundImage.image}
    roundImage={dataWithRoundImage.roundImage}
    source={dataWithRoundImage.source}
  />
));
stories.addWithInfo('Fix Structure: without source', () => (
  <PostCard
    initiator={dataWithRoundImage.initiator}
    description={dataWithRoundImage.description}
    time={dataWithRoundImage.time}
    image={dataWithRoundImage.image}
    roundImage={dataWithRoundImage.roundImage}
    userProfileLink={dataWithRoundImage.userProfileLink}
  />
));
stories.addWithInfo('Fix Structure: without time', () => (
  <PostCard
    initiator={dataWithRoundImage.initiator}
    description={dataWithRoundImage.description}
    image={dataWithRoundImage.image}
    roundImage={dataWithRoundImage.roundImage}
    source={dataWithRoundImage.source}
  />
));
stories.addWithInfo('Fix Structure: without initiator', () => (
  <PostCard
    description={dataWithRoundImage.description}
    time={dataWithRoundImage.time}
    image={dataWithRoundImage.image}
    roundImage={dataWithRoundImage.roundImage}
    source={dataWithRoundImage.source}
  />
));
stories.addWithInfo('Fix Structure: without description', () => (
  <PostCard
    initiator={dataWithRoundImage.initiator}
    time={dataWithRoundImage.time}
    image={dataWithRoundImage.image}
    roundImage={dataWithRoundImage.roundImage}
    source={dataWithRoundImage.source}
  />
));
stories.addWithInfo('Fix Structure: without image', () => (
  <PostCard
    initiator={dataWithRoundImage.initiator}
    description={dataWithRoundImage.description}
    time={dataWithRoundImage.time}
    roundImage={dataWithRoundImage.roundImage}
    source={dataWithRoundImage.source}
  />
));
stories.addWithInfo('Fix Structure: with squareImage', () => (
  <PostCard
    initiator={dataWithImage.initiator}
    description={dataWithImage.description}
    image={dataWithRoundImage.image}
    time={dataWithImage.time}
    roundImage={dataWithImage.roundImage}
    source={dataWithImage.source}
  />
));
stories.addWithInfo('Fix Structure: with href and anchor', () => (
  <PostCard
    initiator={dataWithImage.initiator}
    description={dataWithImage.description}
    image={dataWithRoundImage.image}
    time={dataWithImage.time}
    roundImage={dataWithImage.roundImage}
    source={dataWithImage.source}
  />
));

stories.addWithInfo('Custom structure 1', () => (
  <PostCardItem onClick={action('on click called')} >
    <PostCardItemContent tag={Link} to="/toSomePage" chevronRight>
      <PostCardItemImage image={dataWithRoundImage.image} roundImage={dataWithRoundImage.roundImage} />
      <PostCardItemBody>
        <div>
          <strong>
            <Link href="#">{dataWithRoundImage.initiator}</Link>
          </strong> {dataWithRoundImage.description}
        </div>
        <PostCardItemSource>
          <small><Icon name="clock-o" /><Link to="/toSomePage"> {moment(dataWithRoundImage.time).fromNow()}</Link> · {dataWithRoundImage.source}</small>
        </PostCardItemSource>
      </PostCardItemBody>
    </PostCardItemContent>
  </PostCardItem>
));
stories.addWithInfo('Custom structure 2', () => (
  <PostCardItem>
    <PostCardItemContent tag={Link} to="/toSomeLink" chevronRight>
      <PostCardItemImage image={dataWithImage.image} roundImage={dataWithImage.roundImage} />
      <PostCardItemBody>
        <PostCardItemSource>
          <small><Icon name="clock-o" /><Link to="/toSomeLink"> {moment(dataWithImage.time).fromNow()}</Link> · {dataWithImage.source}</small>
        </PostCardItemSource>
        <div>
          <strong>
            <Link to="#">{dataWithImage.initiator}</Link>
          </strong> {dataWithImage.description}
        </div>
      </PostCardItemBody>
    </PostCardItemContent>
  </PostCardItem>
));
