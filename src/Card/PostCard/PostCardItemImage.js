/**
 * Created by anser on 12/7/16.
 */

import React, { PropTypes } from 'react';
import Media from 'reactstrap/lib/Media';

const propTypes = {
  tag: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
  image: PropTypes.string,
  roundImage: PropTypes.bool
};

const defaultProps = {
  tag: 'div',
  image: '',
  roundImage: undefined
};

const PostCardItemImage = (props) => {
  const {
    tag: Tag,
    image,
    roundImage,
    ...attributes
  } = props;

  return (
    <Tag className={roundImage ? 'card-image' : ''} {...attributes}>
      <Media object src={image} />
    </Tag>
  );
};

PostCardItemImage.propTypes = propTypes;
PostCardItemImage.defaultProps = defaultProps;

export default PostCardItemImage;
