/**
 * Created by amna on 12/7/16.
 */

import React, { PropTypes } from 'react';
import CardBlock from 'reactstrap/lib/CardBlock';
import CardText from 'reactstrap/lib/CardText';
import 'bluerain-bootstrap-theme/dist/css/components/card-view.css';
import CustomCardLink from '../CustomCardLink';
import CardViewImg from '../CardViewImg';
import CardViewBody from '../CardView';

const propTypes = {
  title: PropTypes.string,
  subtitle: PropTypes.string,
  image: PropTypes.string,
  linkTag: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
  to: PropTypes.string,
  href: PropTypes.string,
  onClick: PropTypes.func
};

const defaultProps = {
  title: '',
  subtitle: '',
  image: '',
  linkTag: 'a',
  to: '',
  href: '',
  onClick: () => {
  }
};

export default class CardView extends React.Component {

  static renderTitle(title) {
    if (title) {
      return (
        <CardText tag="span" className="text-truncate">
          {title}
        </CardText>
      );
    }
  }
  static renderSubtitle(subtitle) {
    if (subtitle) {
      return (
        <CardText tag="span">
          {subtitle}
        </CardText>
      );
    }
  }
  constructor(props) {
    super(props);
    this.handleClickAction = this.handleClickAction.bind(this);
  }

  handleClickAction() {
    this.props.onClick();
  }


  render() {
    const {
      title,
      subtitle,
      image,
      linkTag,
      href,
      to,
      ...props
    } = this.props;
    return (
      <CardViewBody onClick={this.handleClickAction} {...props}>
        <CustomCardLink
          tag={linkTag}
          to={to}
          href={href}
        >
          <CardViewImg image={image} />
          <CardBlock>
            <div className="primary-text">
              {CardView.renderTitle(title)}
            </div>
            <div className="text-muted secondary-text">
              {CardView.renderSubtitle(subtitle)}
            </div>
          </CardBlock>
        </CustomCardLink>
      </CardViewBody>
    );
  }
}

CardView.propTypes = propTypes;
CardView.defaultProps = defaultProps;
