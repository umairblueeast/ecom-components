/**
 * Created by Title on 12/20/16.
 */

import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import CardView from './CardView.component';
import data from '../../../__mocks__/CardView.mock';
const stories = storiesOf('CardView', module);
const style = {
  width: 150
};

stories.addWithInfo('Complete Card', () => (
  <div style={style}>
    <CardView
      title={data.title}
      subtitle={data.subtitle}
      image={data.image}
      linkTag={data.linkTag}
      to={data.to}
      onClick={action('card-click')}
    />
  </div>
));

stories.addWithInfo('Without title', () => (
  <div style={style}>
    <CardView
      subtitle={data.subtitle}
      image={data.image}
      linkTag={data.linkTag}
      to={data.to}
      onClick={action('card-click')}
    />
  </div>
));

stories.addWithInfo('Without subtitle', () => (
  <div style={style}>
    <CardView
      title={data.title}
      image={data.image}
      href={data.href}
    />
  </div>
));

stories.addWithInfo('without image', () => (
  <div style={style}>
    <CardView
      title={data.title}
      subtitle={data.subtitle}
      href={data.href}
    />
  </div>
));
