/**
 * @author atif
 */
import React, { PropTypes } from 'react';

const propTypes = {
  tag: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
  className: PropTypes.string
};

const defaultProps = {
  tag: 'div'
};

const CardBlockLabel = (props) => {
  const {
    tag: Tag,
    className,
    ...attributes
  } = props;


  return (
    <Tag {...attributes} className="label text-truncate" />
  );
};

CardBlockLabel.propTypes = propTypes;
CardBlockLabel.defaultProps = defaultProps;

export default CardBlockLabel;
