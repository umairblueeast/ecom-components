/**
 * @author atif
 */
import React, { PropTypes } from 'react';

const propTypes = {
  tag: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
  className: PropTypes.string
};

const defaultProps = {
  tag: 'div'
};

const CardBlockValue = (props) => {
  const {
    tag: Tag,
    className,
    ...attributes
  } = props;


  return (
    <Tag {...attributes} className="value text-truncate" />
  );
};

CardBlockValue.propTypes = propTypes;
CardBlockValue.defaultProps = defaultProps;

export default CardBlockValue;
