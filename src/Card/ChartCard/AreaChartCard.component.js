/**
 * Created by atif on 12/5/16.
 */
import React, { PropTypes } from 'react';
import 'bluerain-bootstrap-theme/dist/css/components/chart-card.css';
import CardBlock from 'reactstrap/lib/CardBlock';
import CustomCardLink from '../CustomCardLink';
import CardView from './ChartCard';
import  AreaChart from './AreaChart.component';

const propTypes = {
  label: PropTypes.string,
  value: PropTypes.string,
  unit: PropTypes.string,
  data: PropTypes.array,
  color: PropTypes.string,
  linkTag: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
  to: PropTypes.string,
  href: PropTypes.string,
  onClick: PropTypes.func.isRequired,
  x: PropTypes.string,
  y: PropTypes.func,
  tooltip: PropTypes.bool,
  tooltipIsBoolean: PropTypes.bool,
  tooltipBooleanTrueLabel: PropTypes.string,
  tooltipBooleanFalseLabel: PropTypes.string,
  interpolation: PropTypes.string
};

const defaultProps = {
  label: '',
  value: '',
  unit: '',
  data: [],
  color: 'pink',
  linkTag: 'a',
  to: '',
  href: '#',
  onClick: () => {

  },
  tooltip: undefined,
  interpolation: "step",
  tooltipIsBoolean: false,
  tooltipBooleanTrueLabel: 'Boolean On',
  tooltipBooleanFalseLabel: 'Boolean False'
};

const _ = require('lodash');
export default class AreaChartCard extends React.Component {

  static drawChart(data, x, y, tooltip, unit, interpolation, tooltipIsBoolean,
                   tooltipBooleanTrueLabel, tooltipBooleanFalseLabel) {
    if (!_.isEmpty(data)) {
      return (
        <AreaChart
          data={data}
          x={x}
          y={y}
          tooltip={tooltip}
          unit={unit}
          interpolation={interpolation}
          tooltipIsBoolean={tooltipIsBoolean}
          tooltipBooleanTrueLabel={tooltipBooleanTrueLabel}
          tooltipBooleanFalseLabel={tooltipBooleanFalseLabel}
        />
      );
    }
  }
  static drawLabel(label) {
    if (label) {
      return (
        <div className="label">
          {label}
        </div>
      );
    }
  }
  static drawValue(value) {
    if (value) {
      return (
        <div className="value">
          {value}
        </div>
      );
    }
  }
  constructor(props) {
    super(props);
    this.handleClickAction = this.handleClickAction.bind(this);
  }

  handleClickAction() {
    this.props.onClick();
  }

  render() {
    const {
      data,
      x,
      y,
      linkTag,
      to,
      href,
      label,
      value,
      tooltip,
      unit,
      interpolation,
      tooltipIsBoolean,
      tooltipBooleanTrueLabel,
      tooltipBooleanFalseLabel,
      ...props
    } = this.props;
    return (
      <CardView {...props}>
        <CustomCardLink
          tag={linkTag}
          to={to}
          href={href}
        >
          <CardBlock>
            {AreaChartCard.drawLabel(label)}
            {AreaChartCard.drawValue(value)}
          </CardBlock>
          {AreaChartCard.drawChart(data, x, y, tooltip, unit, interpolation, tooltipIsBoolean,
            tooltipBooleanTrueLabel, tooltipBooleanFalseLabel)}
        </CustomCardLink>
      </CardView>
    );
  }
}

AreaChartCard.propTypes = propTypes;
AreaChartCard.defaultProps = defaultProps;
