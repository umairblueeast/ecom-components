/**
 * Created by amna on 12/22/16.
 */
import React, { PropTypes } from 'react';
import { VictoryArea, VictoryGroup, VictoryVoronoiTooltip, VictoryAxis } from 'victory';
import moment from 'moment';

const propTypes = {
  data: PropTypes.array,
  x: PropTypes.string,
  y: PropTypes.func,
  unit: PropTypes.string,
  tooltip: PropTypes.bool,
  tooltipIsBoolean: PropTypes.bool,
  tooltipBooleanTrueLabel: PropTypes.string,
  tooltipBooleanFalseLabel: PropTypes.string,
  fillColor: PropTypes.string,
  strokeColor: PropTypes.string,
  dateFormat: PropTypes.string,
  interpolation: PropTypes.string
};

const defaultProps = {
  data: [],
  x: '',
  y: () => {},
  tooltip: undefined,
  tooltipIsBoolean: false,
  tooltipBooleanTrueLabel: 'Boolean On',
  tooltipBooleanFalseLabel: 'Boolean False',
  unit: '',
  fillColor: 'rgba(255,255,255,0.3)',
  strokeColor: 'white',
  dateFormat: 'hh:mm:ss a',
  interpolation: "step"
};

export default class AreaChart extends React.Component {

  static renderTooltip(tooltip, dateFormat, unit, tooltipIsBoolean, tooltipBooleanTrueLabel, tooltipBooleanFalseLabel) {
    if (tooltip) {
      if(tooltipIsBoolean){
        return (
          <VictoryVoronoiTooltip labels={d => ` ${d.y === true ? tooltipBooleanTrueLabel : tooltipBooleanFalseLabel } at ${moment(d.x).format(dateFormat)}`} />
        );
      }
      return (
        <VictoryVoronoiTooltip labels={d => ` ${Math.round(d.y * 100) / 100}${unit} at ${moment(d.x).format(dateFormat)}`} />
      );

    }
  }

  render() {
    const {
      data,
      x,
      y,
      tooltip,
      dateFormat,
      fillColor,
      unit,
      strokeColor,
      interpolation,
      tooltipIsBoolean,
      tooltipBooleanTrueLabel,
      tooltipBooleanFalseLabel,
      ...attributes
    } = this.props;
    return (
      <div className="chart-container" {...attributes}>
        <VictoryGroup
          data={data}
          x={x}
          y={y}
          scale={{x: "time", y: "linear"}}
          padding={{ top: 4 }} height={125} zIndex={20}
          style={{ parent: { overflow: 'visible' } }}
        >
          <VictoryArea
            data={data}
            interpolation={interpolation}
            x={x}
            y={y}
            style={{
              data: {
                fill: fillColor,
                stroke: strokeColor,
                strokeWidth: 2,
                strokeLinecap: 'round',
                strokeLinejoin: 'round'
              }
            }}
          />
          {AreaChart.renderTooltip(tooltip, dateFormat, unit, tooltipIsBoolean,
            tooltipBooleanTrueLabel, tooltipBooleanFalseLabel)}
        </VictoryGroup>
      </div>
    );
  }
}

AreaChart.propTypes = propTypes;
AreaChart.defaultProps = defaultProps;
