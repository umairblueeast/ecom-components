/**
 * Created by amna on 12/22/16.
 */

import { Link } from 'react-router';

const measurementData = [
  {
    x: '2016-12-18T15:46:17.189ZZ',
    y: 1
  },
  {
    x: '2016-12-18T15:50:17.189Z',
    y: 2
  },
  {
    x: '2016-12-18T15:55:17.189Z',
    y: 3
  },
  {
    x: '2016-12-18T16:46:17.189Z',
    y: 4
  },
  {
    x: '2016-12-18T16:50:17.189Z',
    y: 5
  },
  {
    x: '2016-12-18T15:50:17.189Z',
    y: 3
  },
];

const data = {
  label: 'Energy',
  value: '12 KWh',
  data: measurementData,
  color: 'yellow',
  linkTag: Link,
  to: '/path',
  href: '/href',
  x: '',
  y: '',
  onClick() {
    console.log('click performed');
  }
};

module.exports = data;
