/**
 * Created by amna on 12/23/16.
 */
import React from 'react';
import { storiesOf } from '@kadira/storybook';
import { withKnobs, array, text } from '@kadira/storybook-addon-knobs';

import AreaChart from './AreaChart.component';

const stories = storiesOf('AreaChart', module);
stories.addDecorator(withKnobs);
const style = {
  width: 300
};

stories.addWithInfo('AreaChart with tooltip', () => {

  const measurementData = [
    {
      x: '2016-06-03T09:10:51Z',
      y: 1,
    },
    {
      x: '2016-07-03T09:15:51Z',
      y: 5,
    },
    {
      x: '2016-08-03T09:20:51Z',
      y: 8,
    },
    {
      x: '2016-09-03T09:25:51Z',
      y: 10,
    },
    {
      x: '2016-10-03T09:30:51Z',
      y: 12,
    },
    {
      x: '2016-11-03T09:35:51Z',
      y: 18,
    },
  ];
  const unit = text('unit', 'W');
  const tooltip = true;
  const dateformat = text('dateformat', 'DD MMM YYYY, hh:mm:ss a');
  const data = array('data', measurementData);
  return (
    <div style={style}>
      <AreaChart
        data={data}
        x="x"
        y={datum => (datum.y)}
        tooltip={tooltip}
        unit={unit}
        dateFormat={dateformat}
      />
    </div>
  );
});

stories.addWithInfo('AreaChart without tooltip', () => {

  const measurementData = [
    {
      x: '2016-06-03T09:10:51Z',
      y: 1,
    },
    {
      x: '2016-07-03T09:15:51Z',
      y: 5,
    },
    {
      x: '2016-08-03T09:20:51Z',
      y: 8,
    },
    {
      x: '2016-09-03T09:25:51Z',
      y: 10,
    },
    {
      x: '2016-10-03T09:30:51Z',
      y: 12,
    },
    {
      x: '2016-11-03T09:35:51Z',
      y: 18,
    },
  ];
  const data = array('data', measurementData);
  return (
    <div style={style}>
      <AreaChart
        data={data}
        x="x"
        y={datum => (datum.y)}
      />
    </div>
  );
});
