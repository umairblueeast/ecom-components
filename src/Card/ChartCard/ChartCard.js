/**
 * Created by amna on 12/7/16.
 */


import React, { PropTypes } from 'react';

const propTypes = {
  tag: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
  onClick: PropTypes.func,
  color: PropTypes.string
};

const defaultProps = {
  tag: 'div',
  onClick: () => {},
  color: 'pink'
};

export default class CardView extends React.Component {
  constructor(props) {
    super(props);
    this.handleClickAction = this.handleClickAction.bind(this);
  }

  handleClickAction() {
    this.props.onClick();
  }
  render() {
    const {
      tag: Tag,
      color,
      ...attributes
    } = this.props;
    const className = `card card-link-block chart-card ${color}-gradient`;

    return (
      <Tag
        {...attributes}
        className={className}
        onClick={this.handleClickAction}
      />
    );
  }
}

CardView.propTypes = propTypes;
CardView.defaultProps = defaultProps;
