/**
 * Created by amna on 12/23/16.
 */

import React from 'react';
import { storiesOf } from '@kadira/storybook';
import { withKnobs, array, text } from '@kadira/storybook-addon-knobs';
import moment from 'moment';

import AreaChartAxis from './AreaChartAxis.component';

const stories = storiesOf('AreaChartAxis', module);
stories.addDecorator(withKnobs);
const style = {
  width: 300
};

stories.addWithInfo('AreaChartAxis with tooltip', () => {
  const fillColor = text('fillcolor', 'rgba(255.0, 200.0, 150.0, 0.3)');
  const strokeColor = text('fillcolor', 'red');
  const tooltip = true;
  const measurementData = [
    {
      x: new Date('2016-12-18T15:10:17.189Z'),
      y: 0,
    },
    {
      x: new Date('2016-12-18T15:20:17.189Z'),
      y: 2
    },
    {
      x: new Date('2016-12-18T15:30:17.189Z'),
      y: 4.4444444
    },
    {
      x: new Date('2016-12-18T16:40:17.189Z'),
      y: 6.6666666
    },
    {
      x: new Date('2016-12-19T17:50:17.189Z'),
      y: 4.88888
    },
    {
      x: new Date('2016-12-19T18:55:17.189Z'),
      y: 2.2222222
    },
  ];
  const data = array('data', measurementData);

  return (
    <div style={style}>
      <AreaChartAxis
        data={data}
        x="x"
        y={datum => (datum.y)}
        fillColor={fillColor}
        strokeColor={strokeColor}
        tooltip={tooltip}
        dateFormat="YYYY-MM-DD hh:mm:ss a"
        height={500}
        unit="W"
      />
    </div>
  );
});

stories.addWithInfo('AreaChartAxis without tooltip', () => {

  const measurementData = [
    {
      x: '2016-06-03T09:10:51Z',
      y: 1,
    },
    {
      x: '2016-07-03T09:15:51Z',
      y: 2,
    },
    {
      x: '2016-08-03T09:20:51Z',
      y: 3,
    },
    {
      x: '2016-09-03T09:25:51Z',
      y: 4,
    },
    {
      x: '2016-10-03T09:30:51Z',
      y: 5,
    },
    {
      x: '2016-11-03T09:35:51Z',
      y: 3,
    },
  ];
  const data = array('data', measurementData);
  const tickformat = array('tickformat', ['on']);
  return (
    <div style={style}>
      <AreaChartAxis
        xTickCount={5}
        yTickCount={1}
        tickFormat={tickformat}
        data={data}
        x="x"
        y={datum => (datum.y)}
      />
    </div>
  );
});
