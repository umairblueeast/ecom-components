/**
 * Created by amna on 12/22/16.
 */

import React, { PropTypes } from 'react';
import { VictoryArea, VictoryChart, VictoryGroup, VictoryVoronoiTooltip, VictoryAxis } from 'victory';
import moment from 'moment';

const propTypes = {
  data: PropTypes.array,
  x: PropTypes.string,
  y: PropTypes.func,
  xTickCount: PropTypes.number,
  yTickCount: PropTypes.number,
  tickFormat: PropTypes.array,
  tooltip: PropTypes.bool,
  tooltipIsBoolean: PropTypes.bool,
  tooltipBooleanTrueLabel: PropTypes.string,
  tooltipBooleanFalseLabel: PropTypes.string,
  dateFormat: PropTypes.string,
  fillColor: PropTypes.string,
  strokeColor: PropTypes.string,
  unit: PropTypes.string,
  height: PropTypes.number,
  width: PropTypes.number,
  interpolation: PropTypes.string
};

const defaultProps = {
  data: [],
  x: '',
  y: () => {},
  xTickCount: 5,
  yTickCount: 5,
  tickFormat: undefined,
  tooltip: undefined,
  tooltipIsBoolean: false,
  tooltipBooleanTrueLabel: 'Boolean On',
  tooltipBooleanFalseLabel: 'Boolean False',
  fillColor: 'rgba(0,255,0,0.3)',
  strokeColor: 'black',
  unit: '',
  dateFormat: 'hh:mm:ss a',
  height: 400,
  width: 600,
  interpolation: "step"
};

export default class AreaChartAxis extends React.Component {

  static renderTooltip(tooltip, dateFormat, unit, tooltipIsBoolean, tooltipBooleanTrueLabel, tooltipBooleanFalseLabel) {
    if (tooltip) {
      if(tooltipIsBoolean){
        return (
          <VictoryVoronoiTooltip labels={d => ` ${d.y === true ? tooltipBooleanTrueLabel : tooltipBooleanFalseLabel } at ${moment(d.x).format(dateFormat)}`} />
        );
      }
      return (
        <VictoryVoronoiTooltip labels={d => ` ${Math.round(d.y * 100) / 100}${unit} at ${moment(d.x).format(dateFormat)}`} />
      );

    }
  }

  static renderYAxis(tickCount, tickformat) {
    if (tickformat && tickformat.length > 0) {
      return (<VictoryAxis tickCount={tickCount} tickFormat={tickformat} dependentAxis />);
    }
    return (<VictoryAxis tickCount={tickCount}  dependentAxis />);
  }
  render() {
    const {
      data,
      x,
      y,
      fillColor,
      strokeColor,
      tooltip,
      dateFormat,
      unit,
      height,
      width,
      xTickCount,
      yTickCount,
      tickFormat,
      interpolation,
      tooltipIsBoolean,
      tooltipBooleanTrueLabel,
      tooltipBooleanFalseLabel,
      ...attributes
    } = this.props;

    return (
      <div className="chart-container" {...attributes}>
        <VictoryChart style={{ parent: { overflow: 'inherit' } }} height={height}
                      scale={{x: "time", y: "linear"}} width={width} >
          <VictoryGroup data={data} x={x} y={y} >
            <VictoryArea
              interpolation={interpolation}
              style={{
                data: {
                  fill: fillColor,
                  stroke: strokeColor,
                  strokeWidth: 1,
                  strokeLinecap: 'round',
                  strokeLinejoin: 'round'
                }
              }}
            />
            {AreaChartAxis.renderTooltip(tooltip, dateFormat, unit, tooltipIsBoolean,
              tooltipBooleanTrueLabel, tooltipBooleanFalseLabel)}
          </VictoryGroup>
          <VictoryAxis tickCount={xTickCount} label="Time" />
          {AreaChartAxis.renderYAxis(yTickCount, tickFormat)}

        </VictoryChart>
      </div>
    );
  }
}

AreaChartAxis.propTypes = propTypes;
AreaChartAxis.defaultProps = defaultProps;
