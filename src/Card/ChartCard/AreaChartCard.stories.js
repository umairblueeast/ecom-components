/**
 * Created by amna on 12/20/16.
 */
import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import { withKnobs, text, array } from '@kadira/storybook-addon-knobs';
import GridView from '../../GridView/GridView.component';
import GridViewItem from '../../GridView/GridViewItem.component';
import AreaChartCard from './AreaChartCard.component';
import data from './AreaChartCard.mock';
const stories = storiesOf('AreaChartCard', module);
stories.addDecorator(withKnobs);
const style = {
  width: 300
};

stories.addWithInfo('AreaChart with tooltip - Knobs', () => {
  const color = text('color', 'blue');
  const label = text('label', 'Energy');
  const value = text('value', '22 KWh');
  const unit = text('unit', 'W');
  const dateformat = text('dateformat', 'DD MMM YYYY, hh:mm:ss a');
  const measurementData = [
    {
      x: '2016-06-03T09:10:51Z',
      y: 1,
    },
    {
      x: '2016-07-03T09:15:51Z',
      y: 5,
    },
    {
      x: '2016-08-03T09:20:51Z',
      y: 8,
    },
    {
      x: '2016-09-03T09:25:51Z',
      y: 10,
    },
    {
      x: '2016-10-03T09:30:51Z',
      y: 12,
    },
    {
      x: '2016-11-03T09:35:51Z',
      y: 18,
    },
  ];
  const data = array('data', measurementData);
  const tooltip = true;
  return (
    <div style={style}>
      <AreaChartCard
        data={data}
        x="x"
        y={datum => (datum.y)}
        color={color}
        label={label}
        value={value}
        href={data.href}
        tooltip={tooltip}
        unit={unit}
        dateFormat={dateformat}
        interpolation="linear"
      />
    </div>
  );
});

stories.addWithInfo('AreaChart - Color options', () => (
  <GridView>
    <GridViewItem>
      <AreaChartCard
        data={data.data}
        x="x"
        y={datum => (datum.y)}
        color="red"
        label={data.label}
        value={data.value}
        href={data.href}
      />
    </GridViewItem>
    <GridViewItem>
      <AreaChartCard
        data={data.data}
        x="x"
        y={datum => (datum.y)}
        color="pink"
        label={data.label}
        value={data.value}
        href={data.href}
      />
    </GridViewItem>
    <GridViewItem>
      <AreaChartCard
        data={data.data}
        x="x"
        y={datum => (datum.y)}
        color="purple"
        label={data.label}
        value={data.value}
        href={data.href}
      />
    </GridViewItem>
    <GridViewItem>
      <AreaChartCard
        data={data.data}
        x="x"
        y={datum => (datum.y)}
        color="deep-purple"
        label={data.label}
        value={data.value}
        href={data.href}
      />
    </GridViewItem>
    <GridViewItem>
      <AreaChartCard
        data={data.data}
        x="x"
        y={datum => (datum.y)}
        color="indigo"
        label={data.label}
        value={data.value}
        href={data.href}
      />
    </GridViewItem>
    <GridViewItem>
      <AreaChartCard
        data={data.data}
        x="x"
        y={datum => (datum.y)}
        color="blue"
        label={data.label}
        value={data.value}
        href={data.href}
      />
    </GridViewItem>
    <GridViewItem>
      <AreaChartCard
        data={data.data}
        x="x"
        y={datum => (datum.y)}
        color="light-blue"
        label={data.label}
        value={data.value}
        href={data.href}
      />
    </GridViewItem>
    <GridViewItem>
      <AreaChartCard
        data={data.data}
        x="x"
        y={datum => (datum.y)}
        color="cyan"
        label={data.label}
        value={data.value}
        href={data.href}
      />
    </GridViewItem>
    <GridViewItem>
      <AreaChartCard
        data={data.data}
        x="x"
        y={datum => (datum.y)}
        color="teal"
        label={data.label}
        value={data.value}
        href={data.href}
      />
    </GridViewItem>
    <GridViewItem>
      <AreaChartCard
        data={data.data}
        x="x"
        y={datum => (datum.y)}
        color="green"
        label={data.label}
        value={data.value}
        href={data.href}
      />
    </GridViewItem>
    <GridViewItem>
      <AreaChartCard
        data={data.data}
        x="x"
        y={datum => (datum.y)}
        color="light-green"
        label={data.label}
        value={data.value}
        href={data.href}
      />
    </GridViewItem>
    <GridViewItem>
      <AreaChartCard
        data={data.data}
        x="x"
        y={datum => (datum.y)}
        color="lime"
        label={data.label}
        value={data.value}
        href={data.href}
      />
    </GridViewItem>
    <GridViewItem>
      <AreaChartCard
        data={data.data}
        x="x"
        y={datum => (datum.y)}
        color="yellow"
        label={data.label}
        value={data.value}
        href={data.href}
      />
    </GridViewItem>
    <GridViewItem>
      <AreaChartCard
        data={data.data}
        x="x"
        y={datum => (datum.y)}
        color="amber"
        label={data.label}
        value={data.value}
        href={data.href}
      />
    </GridViewItem>
    <GridViewItem>
      <AreaChartCard
        data={data.data}
        x="x"
        y={datum => (datum.y)}
        color="orange"
        label={data.label}
        value={data.value}
        href={data.href}
      />
    </GridViewItem>
    <GridViewItem>
      <AreaChartCard
        data={data.data}
        x="x"
        y={datum => (datum.y)}
        color="deep-orange"
        label={data.label}
        value={data.value}
        href={data.href}
      />
    </GridViewItem>
    <GridViewItem>
      <AreaChartCard
        data={data.data}
        x="x"
        y={datum => (datum.y)}
        color="brown"
        label={data.label}
        value={data.value}
        href={data.href}
      />
    </GridViewItem>
    <GridViewItem>
      <AreaChartCard
        data={data.data}
        x="x"
        y={datum => (datum.y)}
        color="grey"
        label={data.label}
        value={data.value}
        href={data.href}
      />
    </GridViewItem>
    <GridViewItem>
      <AreaChartCard
        data={data.data}
        x="x"
        y={datum => (datum.y)}
        color="blue-grey"
        label={data.label}
        value={data.value}
        href={data.href}
      />
    </GridViewItem>
  </GridView>
));

stories.addWithInfo('AreaChart without graph', () => (
  <div style={style}>
    <AreaChartCard
      color={data.color}
      label={data.label}
      value={data.value}
      href={data.href}
    />
  </div>
));

stories.addWithInfo('AreaChart with Link and to', () => (
  <div style={style}>
    <AreaChartCard
      data={data.data}
      x="x"
      y={datum => (datum.y)}
      color={data.color}
      label={data.label}
      value={data.value}
      to={data.to}
      linkTag={data.linkTag}
    />
  </div>
));

stories.addWithInfo('AreaChart with onClick', () => (
  <div style={style}>
    <AreaChartCard
      data={data.data}
      x="x"
      y={datum => (datum.y)}
      color={data.color}
      label={data.label}
      value={data.value}
      to={data.to}
      linkTag={data.linkTag}
      onClick={action('on click called')}
    />
  </div>
));
