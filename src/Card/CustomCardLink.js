/**
 * Created by atif on 12/7/16.
 */

import React, { PropTypes } from 'react';

const propTypes = {
  tag: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
  href: PropTypes.string,
  to: PropTypes.string
};

const defaultProps = {
  tag: 'a',
  href: '',
  to: ''
};

const CustomCardLink = (props) => {

  const {
    tag: Tag,
    children,
    href,
    to,
    ...attributes
  } = props;

  return (
    <Tag {...attributes} href={href} to={to}>
      {children}
    </Tag>
  );
};

CustomCardLink.propTypes = propTypes;
CustomCardLink.defaultProps = defaultProps;

export default CustomCardLink;
