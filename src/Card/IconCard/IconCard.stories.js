/**
 * Created by amna on 12/20/16.
 */

import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import { withKnobs, text } from '@kadira/storybook-addon-knobs';
import 'bluerain-bootstrap-theme/dist/css/components/icon-card.css';

import { Link } from 'react-router';
import Icon from 'react-fa';
import IconCard from './IconCard.component';

const stories = storiesOf('IconCard', module);
stories.addDecorator(withKnobs);
const style = {
  width: 300
};

stories.addWithInfo('Icon Card - Knobs', () => {
  const color = text('color', 'deep-orange');
  const title = text('title', 'Some title');
  const icon = text('icon', 'sun-o');
  return (
    <div style={style}>
      <IconCard
        title={title}
        color={color}
        icon={icon}
        description={<span><Icon name="clock-o" /> Last Activity 4 hours</span>}
        linkTag={Link}
        to="/toSomePath"
        onClick={action('card-click')}
      />
    </div>
  );
});

stories.addWithInfo('Icon Card - Without title', () => {
  const color = text('color', 'teal');
  const icon = text('icon', 'sun-o');
  return (
    <div style={style}>
      <IconCard
        color={color}
        icon={icon}
        description={<span><Icon name="clock-o" /> Last Activity 4 hours</span>}
      />
    </div>
  );
});

stories.addWithInfo('Icon Card - without description', () => {
  const color = text('color', 'yellow');
  const title = text('title', 'Some title');
  const icon = text('icon', 'sun-o');
  return (
    <div style={style}>
      <IconCard
        title={title}
        color={color}
        icon={icon}
      />
    </div>
  );
});
