/**
 * Created by amna on 12/6/16.
 */

import React, { PropTypes } from 'react';
import { CardBlock, CardTitle, CardText } from 'reactstrap';
import 'bluerain-bootstrap-theme/dist/css/components/icon-card.css';
import CustomCardLink from '../CustomCardLink';
import IconCardBody from './IconCard';
import IconCardIcon from './IconCardIcon';

const propTypes = {
  title: PropTypes.string,
  description: PropTypes.oneOfType([PropTypes.func, PropTypes.object, PropTypes.string]),
  icon: PropTypes.string,
  color: PropTypes.string,
  linkTag: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
  to: PropTypes.string,
  href: PropTypes.string,
  onClick: PropTypes.func
};
const defaultProps = {
  title: '',
  description: {},
  icon: 'music',
  color: 'purple',
  linkTag: 'a',
  to: '',
  href: '',
  onClick: () => {
  }
};

const _ = require('lodash');

export default class IconCard extends React.Component {

  static renderTitle(title) {
    return (title ? <CardTitle tag="h5" className="text-truncate">{title}</CardTitle> : '');
  }
  static renderDescription(description) {
    return (!_.isEmpty(description) ? <CardText tag="p">
      {description}
    </CardText> : '');
  }

  constructor(props) {
    super(props);
    this.handleClickAction = this.handleClickAction.bind(this);
  }

  handleClickAction() {
    this.props.onClick();
  }

  render() {
    const {
      color,
      title,
      description,
      icon,
      linkTag,
      to,
      href
    } = this.props;
    return (
      <IconCardBody onClick={this.handleClickAction} color={color}>
        <CustomCardLink
          tag={linkTag}
          to={to} href={href}
        >
          <CardBlock>
            <IconCardIcon image={icon} />
            {IconCard.renderTitle(title)}
            {IconCard.renderDescription(description)}
          </CardBlock>
        </CustomCardLink>
      </IconCardBody>
    );
  }
}

IconCard.propTypes = propTypes;
IconCard.defaultProps = defaultProps;
