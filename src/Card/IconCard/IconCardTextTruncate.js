/**
 * Created by amna on 12/7/16.
 */


import React, { PropTypes } from 'react';
import { CardText } from 'reactstrap';
const propTypes = {
  tag: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
  className: PropTypes.string,
  title: PropTypes.string
};

const defaultProps = {
  tag: 'div',
  className: '',
  title: ''
};

const IconCardTextTruncate = (props) => {
  const {
    tag: Tag,
    ...attributes
  } = props;


  return (
    <Tag {...attributes} className="primary-text">
      <CardText tag="span" className="text-truncate">
        {props.title}
      </CardText>
    </Tag>
  );
};

IconCardTextTruncate.propTypes = propTypes;
IconCardTextTruncate.defaultProps = defaultProps;

export default IconCardTextTruncate;
