/**
 * Created by amna on 12/7/16.
 */


import React, { PropTypes } from 'react';
import Icon from 'react-fa';
const propTypes = {
  image: PropTypes.string
};

const defaultProps = {
  image: ''
};

const CardViewImg = (props) => {
  return (
    <div className="card-icon">
      <Icon name={props.image} />
    </div>
  );
};

CardViewImg.propTypes = propTypes;
CardViewImg.defaultProps = defaultProps;

export default CardViewImg;
