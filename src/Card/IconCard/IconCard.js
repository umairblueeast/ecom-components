/**
 * Created by amna on 12/7/16.
 */


import React, { PropTypes } from 'react';

const propTypes = {
  tag: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
  onClick: PropTypes.func,
  color: PropTypes.string
};

const defaultProps = {
  tag: 'div',
  color: 'pink'
};

export default class IconCardBody extends React.Component {
  constructor(props) {
    super(props);
    this.handleClickAction = this.handleClickAction.bind(this);
  }

  handleClickAction() {
    this.props.onClick();
  }
  render() {
    const {
      tag: Tag,
      color,
      ...attributes
    } = this.props;

    const cardClasses = `card card-inverse card-link-block icon-card 
      ${color}-gradient`;
    return (
      <Tag
        {...attributes}
        className={cardClasses}
        onClick={this.handleClickAction}
      />
    );
  }
}

IconCardBody.propTypes = propTypes;
IconCardBody.defaultProps = defaultProps;
