/**
 * Created by atif on 12/7/16.
 */

import React, { PropTypes } from 'react';

const propTypes = {
  tag: PropTypes.string
};

const defaultProps = {
  tag: 'div'
};

const ChartContainer = (props) => {

  const {
    tag: Tag,
    children,
    href,
    ...attributes
  } = props;

  return (
    <Tag {...attributes} className="chart-container">
      {children}
    </Tag>
  );
};

ChartContainer.propTypes = propTypes;
ChartContainer.defaultProps = defaultProps;

export default ChartContainer;
