/**
 * Created by amna on 12/7/16.
 */


import React, { PropTypes } from 'react';

const propTypes = {
  tag: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
  onClick: PropTypes.func
};

const defaultProps = {
  tag: 'div'
};

export default class CardView extends React.Component {
  constructor(props) {
    super(props);
    this.handleClickAction = this.handleClickAction.bind(this);
  }

  handleClickAction() {
    this.props.onClick();
  }
  render() {
    const {
      tag: Tag,
      ...attributes
    } = this.props;

    return (
      <Tag
        {...attributes}
        className="card card-view card-link-block"
        onClick={this.handleClickAction}
      />
    );
  }
}

CardView.propTypes = propTypes;
CardView.defaultProps = defaultProps;
