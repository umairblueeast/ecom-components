import React, { PropTypes } from 'react';
// import '../cssFile.css';



let image = '';
let cartItemRight = '';
class CartItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      quantity: props.Quantity,
    };
  }
  static propTypes = {
  ProdName: PropTypes.string,
  ProdImg: PropTypes.string,
  ProdPrice: PropTypes.number,
  Quantity: PropTypes.number,
};
static defaultProps = {
  ProdName: '',
  ProdImg: '',
  ProdPrice: 0,
  Quantity: 1,
};
  onQuantityChange(e) {
    this.setState({
      quantity: e.target.value,
    });
  }
  render() {
    const {
      ProdName,
      ProdImg,
      ProdPrice,
      Quantity,
      summary,
    } = this.props;
    image = '';
    cartItemRight = '';
    if (ProdImg) image = <img className="cart-thumb d-flex mr-3" src={ProdImg} alt={ProdName} />;

    if (summary) {
      return (
        <li className="media align-items-center cart-list-item">
          {image}
          <div className="media-body d-flex">
            <div className="mr-auto">
              {ProdName}
            </div>
            <div className="mr-5">
              {Quantity}
            </div>
            <div className>Rs.{ProdPrice * this.state.quantity}</div>
          </div>
        </li>
      );
    }
    return (
      <li className="media align-items-center cart-list-item">
        {image}
        <div className="media-body d-flex">
          <div className="mr-auto">
            {ProdName}
          </div>
          <div className="mr-4">
            Rs. {ProdPrice}
          </div>
          <div className="mr-4">
            <input className="form-control" type="number" value={this.state.quantity} onChange={this.onQuantityChange.bind(this)} name style={{ width: 70 }} />
          </div>
          <div className> <b>Rs.{ProdPrice * this.state.quantity}</b></div>
        </div>
      </li>
    );
  }
}
export default CartItem;
