import React from 'react';
import { storiesOf } from '@kadira/storybook';

import CartItem from './cartitem.component';
import CartTotal from './cartTotal.component';
import ProductCard from './productCard.component';
import Question from './question.component';
import { withKnobs,  number, text } from '@kadira/storybook-addon-knobs';

const stories = storiesOf('Product Cart', module);
stories.addDecorator(withKnobs);

stories.addWithInfo('Cart Item', () => (
  <div className="cart-list">
    <ul className="list-unstyled">
      <CartItem ProdName="Beats Studio Wireless Over-Ear Headphones - Titanium" ProdImg="http://52.43.131.169:5000/templates/assets/img/ecom/thumb.jpg" ProdPrice={20000} Quantity={1} />
    </ul>
  </div>
));
stories.addWithInfo('Cart Item without Img', () => (
  <div className="cart-list">
    <ul className="list-unstyled">
      <CartItem ProdName="Beats Studio Wireless Over-Ear Headphones - Titanium" ProdPrice={20000} Quantity={1} />
    </ul>
  </div>
));
stories.addWithInfo('Cart Item without title ', () => (
  <div className="cart-list">
    <ul className="list-unstyled">
      <CartItem ProdImg="http://52.43.131.169:5000/templates/assets/img/ecom/thumb.jpg" ProdPrice={20000} Quantity={1} />
    </ul>
  </div>
));
stories.addWithInfo('Uneditable Cart Item', () => (
  <div className="cart-list">
    <ul className="list-unstyled">
      <CartItem ProdName="Beats Studio Wireless Over-Ear Headphones - Titanium" ProdImg="http://52.43.131.169:5000/templates/assets/img/ecom/thumb.jpg" ProdPrice={20000} Quantity={1} summary />
    </ul>
  </div>
));
stories.addWithInfo('Cart Total', () => <CartTotal total={20000} discount={300} />);

stories.addWithInfo('Product Card', () => {
  const name = text('Product Name', 'Beats Studio Wireless Over-Ear Headphones - Titanium');
  const image = text('image', 'http://52.43.131.169:5000/templates/assets/img/ecom/thumb.jpg');
  const prodLink = text('Product Link', '#');
  const prodPrice = number('Product Price', 20000);
  const currency = text('Currency', 'Rs.');
  return <ProductCard ProdName={name} ProdImg={image} ProdLink={prodLink} ProdPrice={prodPrice} Currency={currency} />;
});

stories.addWithInfo('Product Card without img', () => {
  const name = text('Product Name', 'Beats Studio Wireless Over-Ear Headphones - Titanium');
  const prodLink = text('Product Link', '#');
  const prodPrice = number('Product Price', 20000);
  const currency = text('Currency', 'Rs.');
  return <ProductCard ProdName={name} ProdLink={prodLink} ProdPrice={prodPrice} Currency={currency} />;
});
stories.addWithInfo('Product Card without title', () => {
  const image = text('image', 'http://52.43.131.169:5000/templates/assets/img/ecom/thumb.jpg');
  const prodLink = text('Product Link', '#');
  const prodPrice = number('Product Price', 20000);
  const currency = text('Currency', 'Rs.');
  return <ProductCard ProdImg={image} ProdLink={prodLink} ProdPrice={prodPrice} Currency={currency} />;
});
stories.addWithInfo('Product Cardwithout price', () => (
  <ProductCard ProdName="Beats Studio Wireless Over-Ear Headphones - Titanium" ProdImg="http://52.43.131.169:5000/templates/assets/img/ecom/thumb.jpg" ProdLink="#" />
));
const link = { label: 'Shipping & Pickup', url: '#' };
stories.addWithInfo('Question', () => (
  <Question
    question="When will I get my items?"
    answer="We need your shipping address before we can give you an accurate delivery estimate. We’ll provide delivery dates when you enter your shipping information during checkout. Learn more about"
    qsInRow={2}
    link={link}
  />
  ));
