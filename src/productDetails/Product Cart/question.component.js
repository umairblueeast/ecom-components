import React, { PropTypes } from 'react';

class Question extends React.Component {
    static propTypes = {
        question:PropTypes.string,
        answer:PropTypes.string,
        qsInrow: PropTypes.number,
        link: PropTypes.object
    }
    static defaultProps = {
        question:'',
        answer:'',
        qsInrow: 1,
        link: {}
    }
  render() {
    const {
      question,
      answer,
      qsInRow,
      link,
    } = this.props;
    return (
      <div className={`col-12 col-xs-12 col-md-${12 / qsInRow} col-lg-${12 / qsInRow}`}>
        <h6>{question}</h6>
        <p>{answer}{' '}<a href={link.url}>{link.label}</a>.</p>
      </div>
    );
  }
}
export default Question;
