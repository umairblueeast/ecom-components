import React, { PropTypes } from 'react';

const propTypes = {
  total: PropTypes.number,
  discount: PropTypes.number,
};
const defaultProps = {
  total: 0,
  discount: 0,
};
const CartTotal = (props) => {
  const {
    total,
    discount,
  } = props;
  return (
    <div className="d-flex">
      <div className="ml-auto text-muted">
        <table>
          <tbody>
            <tr>
              <td className="pr-5">Subtotal</td>
              <td className="text-right">Rs.{total}</td>
            </tr>
            <tr>
              <td className="pr-5">
                Discount
                <a
                  tabIndex={0}
                  className
                  role="button"
                  data-toggle="popover"
                  data-trigger="focus"
                  title="Dismissible popover"
                  data-content="1% for Advance Payment and here's some amazing content. It's very engaging. Right?"
                >
                  <i className="fa fa-question-circle-o" aria-hidden="true" />
                </a>
              </td>
              <td className="text-right">-Rs.{discount}</td>
            </tr>
            <tr>
              <td className="pr-5 text-success"><b>Free Shipping</b></td>
              <td className="text-right text-success"><b>Rs.0</b></td>
            </tr>
            <tr>
              <td className="pr-5">Grand Total</td>
              <td className="text-right"><b>Rs.{total - discount}</b></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  );
};
CartTotal.PropTypes = propTypes;
CartTotal.defaultProps = defaultProps;
export default CartTotal;
