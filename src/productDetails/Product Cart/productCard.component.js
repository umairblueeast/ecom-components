import React, { PropTypes } from 'react';

import '../cssFile.css';

let image = '';
let price = '';
class ProductCard extends React.Component {
  static propTypes = {
    ProdName: PropTypes.string,
    ProdImg: PropTypes.string,
    ProdPrice: PropTypes.number,
    ProdLink: PropTypes.string,
  };
  static defaultProps = {
    ProdName: '',
    ProdImg: '',
    ProdPrice: 0,
    ProdLink: '#',
  };
  render() {
    const {
      ProdName,
      ProdImg,
      ProdPrice,
      ProdLink,
      Currency,
    } = this.props;
    image = '';
    price = '';
    if (ProdImg) image = <a href={ProdLink}><img className="card-img-top" src={ProdImg} alt={ProdName} /></a>;
    if (ProdPrice) price = <span> {Currency} {ProdPrice}</span>;
    return (
      <div className="col-12 col-sm-6 col-md-4 col-lg-4">
        <div className="product-card add-outline ">
          {image}
          <div className="product-card-text">
            <a href={ProdLink}>{ProdName}</a>
            {price}
          </div>
        </div>
      </div>
    );
  }
}

export default ProductCard;
