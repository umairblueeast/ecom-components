import React, { PropTypes } from 'react';

const propTypes = {
  stars: PropTypes.number,
};
const defaultProps = {
  stars: 4
};
const Rating = (props) => {
  const { stars } = props;
  let RatingStars = [];
  for (let i = 0; i < stars; i++) {
    RatingStars.push(<span className="star-filled" key={i}><span className="icon icon-star" /></span>);
  }
  for (let i = stars; i < 5; i++) {
    RatingStars.push(<span className="star-empty" key={i}><span className="icon icon-star" /></span>);
  }
  return (
    <div className="as-starrating">
      {RatingStars}
    </div>
  );
};
Rating.PropTypes = propTypes;
Rating.defaultProps = defaultProps;
export default Rating;
