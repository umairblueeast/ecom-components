import React, { PropTypes } from 'react';
import Icon from 'react-fa';

import '../cssFile.css';
import Rating from './ratingStar';

class Review extends React.Component {
  static propTypes = {
    rating: PropTypes.number,
    reviewHeading: PropTypes.string,
    reviewContent: PropTypes.string,
  };

  static defaultProps = {
    rating: 4,
    reviewHeading: 'Review Heading',
    reviewContent: 'This is sample review',
  };
  render() {
    const {
      rating,
      reviewHeading,
      reviewContent,
    } = this.props;

    return (
      <li className="as-reviewlist-items">
        <div className="as-starrating">
          <Rating stars={rating} />
        </div>
        <div className="row">
          <div className="col-md-9">
            <h3>{reviewHeading}</h3>
            <p>{reviewContent}</p>
          </div>
          <div className="col-md-3 as-reviewlist-controls">
            <form method="post b-l-1">
              <p>Is this review useful?</p>
              <button type="button" className="btn btn-secondary btn-sm">Yes</button>
              <button type="button" className="btn btn-secondary btn-sm">No</button>
            </form>
          </div>
        </div>
      </li>
    );
  }
}
export default Review;
