import React, { PropTypes } from 'react';
import '../cssFile.css';
import Rating from './ratingStar';

const propTypes = {
  five: PropTypes.number,
  four: PropTypes.number,
  three: PropTypes.number,
  two: PropTypes.number,
  one: PropTypes.number,
};
const defaultProps = {
  five: 1,
  four: 1,
  three: 1,
  two: 1,
  one: 1,
};
let total;
let average;
let avg;

class totalRating extends React.Component {
  render() {
    const {
      five,
      four,
      three,
      two,
      one,
    } = this.props;
    total = five + four + three + two + one;
    average = (five * 5 + four * 4 + three * 3 + two * 2 + one * 1) / total;
    avg = Math.round(average, 0);
    return (
      <div className="column large-8 small-10">
        <div className="as-histogram">
          <table className="as-histogram-ratingtable">
            <caption className="as-histogram-caption">
              <div>{average.toFixed(1)} Average</div>
              <span className="hidden">
                <span>4</span> out of <span>5</span> stars
              </span>
              <Rating stars={avg} />
            </caption>
            <thead>
              <tr>
                <th><span className="hidden">Number of stars</span></th>
                <th><span className="hidden">Percentage</span></th>
                <th><span className="hidden">Number of reviews</span></th>
              </tr>
            </thead>
            <tbody>
              <tr className="as-histogram-link">
                <th className="as-histogram-ratingrank">
                  <a className="as-histogram-ratinglink" href="#">
                    5 Star{five <= 1 ? '' : 's'}
                  </a>
                </th>
                <td className="as-histogram-progressbar">
                  <div className="as-histogram-progress">
                    <progress className="progress" value={five / total * 100} max="100" />
                  </div>
                </td>
                <td className="as-histogram-ratingcount">
                  {five}
                </td>
              </tr>
              <tr className="as-histogram-link">
                <th className="as-histogram-ratingrank">
                  <a className="as-histogram-ratinglink" href="#">
                    4 Star{four <= 1 ? '' : 's'}
                  </a>
                </th>
                <td className="as-histogram-progressbar">
                  <div className="as-histogram-progress">
                    <progress className="progress" value={four / total * 100} max="100" />
                  </div>
                </td>
                <td className="as-histogram-ratingcount">
                  {four}
                </td>
              </tr>
              <tr className="as-histogram-link">
                <th className="as-histogram-ratingrank">
                  <a className="as-histogram-ratinglink" href="#">
                    3 Star{three <= 1 ? '' : 's'}
                  </a>
                </th>
                <td className="as-histogram-progressbar">
                  <div className="as-histogram-progress">
                    <progress className="progress" value={three / total * 100} max="100" />
                  </div>
                </td>
                <td className="as-histogram-ratingcount">
                  {three}
                </td>
              </tr>
              <tr className="as-histogram-link">
                <th className="as-histogram-ratingrank">
                  <a className="as-histogram-ratinglink" href="#">
                    2 Star{two <= 1 ? '' : 's'}
                  </a>
                </th>
                <td className="as-histogram-progressbar">
                  <div className="as-histogram-progress">
                    <progress className="progress" value={two / total * 100} max="100" />
                  </div>
                </td>
                <td className="as-histogram-ratingcount">
                  {two}
                </td>
              </tr>
              <tr className="as-histogram-link">
                <th className="as-histogram-ratingrank">
                  <a className="as-histogram-ratinglink" href="#">
                    1 Star{one <= 1 ? '' : 's'}
                  </a>
                </th>
                <td className="as-histogram-progressbar">
                  <div className="as-histogram-progress">
                    <progress className="progress" value={one / total * 100} max="100" />
                  </div>
                </td>
                <td className="as-histogram-ratingcount">
                  {one}
                </td>
              </tr>
            </tbody>
          </table>
        </div>
        <div className="as-pdp-rating-writereview">
          <a className="more" href="#">Write a review</a>
        </div>
      </div>
    );
  }
}
totalRating.PropTypes = propTypes;
totalRating.defaultProps = defaultProps;
export default totalRating;
