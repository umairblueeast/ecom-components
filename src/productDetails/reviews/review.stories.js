import React from 'react';
import { storiesOf } from '@kadira/storybook';

import Review from './Review.component';
import Totalrating from './totalrating.component';
import { withKnobs, number, text } from '@kadira/storybook-addon-knobs';

const stories = storiesOf('Reviews', module);
stories.addDecorator(withKnobs);
stories.addWithInfo('Total Rating', () => {
  const five = number('five', 20);
  const four = number('four', 20);
  const three = number('three', 10);
  const two = number('two', 10);
  const one = number('one', 5);
  return <Totalrating five={five} four={four} three={three} two={two} one={one} />;
});

stories.addWithInfo('Single Review', () => {
  const rating = number('rating', 3);
  const reviewHeading = text('Review Heading', 'Review Heading');
  const reviewContent = text('Review Content', 'This is a sample review');
  return <Review rating={rating} reviewHeading={reviewHeading} reviewContent={reviewContent} />;
});
