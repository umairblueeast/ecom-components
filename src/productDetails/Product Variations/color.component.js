import React, { PropTypes } from 'react';
import ColorSwatchBtn from './color-swatch-btn';


let swatchBtnColor = [];
let swatchBtnImg = [];
class ColorComponent extends React.Component {
    static propTypes ={
  color: PropTypes.array,
  img: PropTypes.array,
};

static defaultProps = {
  color: [''],
  img: [],
};
  render() {
    const {
      color,
      img,
    } = this.props;
    swatchBtnColor = [];
    swatchBtnImg = [];
    color.forEach((color, i) => {
      swatchBtnColor.push(<ColorSwatchBtn color={color} key={color} />);
    });
    img.forEach((imgsrc, i) => {
      swatchBtnImg.push(<ColorSwatchBtn img={imgsrc} key={imgsrc} />);
    });
    return (
      <div className="as-pdp-prodvariation">
        <label className="as-pdp-prodvariations-title">Color</label>
        <div className="btn-group-color-swatch" data-toggle="buttons">
          {swatchBtnColor}
          {swatchBtnImg}
        </div>
      </div>
    );
  }
}
export default ColorComponent;
