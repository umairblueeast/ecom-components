import React from 'react';
import { storiesOf } from '@kadira/storybook';


import ProductOverview from './productOverview.component';
import { withKnobs, array, number, text } from '@kadira/storybook-addon-knobs';

const stories = storiesOf('Product variations', module);
stories.addDecorator(withKnobs);
const sizeData = [
  { value: '12macbook', label: '12-Inch MacBook' },
  { value: '11mba', label: '11-inch MacBook Air' },
  { value: '13mba', label: '13-inch MacBook Air' },
  { value: '15mbp_retina', label: '15-inch MacBook Pro with Retina dis...' },
  { value: '13mbp_retina', label: '13-inch MacBook Pro with Retina dis...' },
];
stories.addWithInfo('Product ', () => {
  const title = text('title', 'Beats Studio Wireless Over-Ear Headphones - Titanium');
  const oldPrice = number('oldPrice', 76000);
  const newPrice = number('newPrice', 56000);
  const stars = number('stars', 3);
  const capacity = array('Capacity', ['1 ton', '1.5 ton']);
  const size = array('size data', sizeData);
  const img = text('Image', 'http://localhost:8080/templates/assets/img/ecom/Headphones.jpeg');
  const swatchbtncolor = array('Swatch Btn color', ['', 'red', 'blue']);
  const swatchbtnimg = array('Swatch Btn images', ['http://localhost:8080/templates/assets/img/ecom/color-swatch-black.jpeg', 'http://localhost:8080/templates/assets/img/ecom/color-swatch-khaki.jpeg']);
  const currency = text('Currency', 'Rs.');

  return (
    <ProductOverview title={title} oldPrice={oldPrice} newPrice={newPrice} stars={stars} capacity={capacity} size={size} image={img} swatchbtncolor={swatchbtncolor} swatchbtnimg={swatchbtnimg} currency={currency} />
  );
});
