import React from 'react';
import '../cssFile.css';

const ProductDeliveryInfo = props => (
  <div className="as-pdp-deliverydates col-xs-12">
    <span className="icon icon-shipping as-purchaseinfo-availabilityicons" style={{ display: 'inline-block' }} />
    <div className="shipdeliverydates as-pdp-shipdeliverydates" data-deliverypartnumber="MHAK2AM/B">
      <span>Delivers:</span>
      <ul className="as-purchaseinfo-dudeinfo list-unstyled">
        <li>
          <span>Tue Aug 2 – Fastest</span>
        </li>
        <li>
          <span>Wed Aug 3 – Free</span>
        </li>
      </ul>
      <a href="#">Get more delivery options</a>
    </div>
  </div>
  );
export default ProductDeliveryInfo;
