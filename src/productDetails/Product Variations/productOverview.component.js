import React, { PropTypes } from 'react';

import '../cssFile.css';
import ProductTitle from './producttitle.component';
import ProductPrice from './productprice.compnent';
import Rating from '../reviews/ratingStar';
import SizeComponent from './size.component';
import CapacityComponent from './capacity.component';
import ColorComponent from './color.component';
import ProductCart from './productcart.component';
import ProductDeliveryInfo from './productdeliveryinfo';
import ProductSocial from './productsocialshare.component';
import ProductImage from './productimage.component';

class ProductOverview extends React.Component {
  static propTypes = {
    title: PropTypes.string,
    oldPrice: PropTypes.number,
    newPrice: PropTypes.number,
    stars: PropTypes.number,
    capacity: PropTypes.array,
    size: PropTypes.array,
    image: PropTypes.string,
    swatchbtncolor: PropTypes.array,
    swatchbtnimg: PropTypes.array,
    currency: PropTypes.string,
  };
  static defaultProps = {
    title: 'title',
    oldPrice: 56000,
    newPrice: 76000,
    stars: 3,
    capacity: [],
    size: [],
    image: '',
    swatchbtncolor: [],
    swatchbtnimg: [],
    currency: 'Rs.',
  };
  render() {
    const {
      title,
      oldPrice,
      newPrice,
      stars,
      capacity,
      size,
      image,
      swatchbtncolor,
      swatchbtnimg,
      currency,
    } = this.props;
    return (
      <div className="row">
        <div className="col-md-4">
          <div className="as-pdp-displayarea">
            <ProductTitle title={title} />
            <div className="as-pdp-purchaseinfosection">
              <ul className="as-pdp-purchaseinfo" id="purchase-info-primary">
                <ProductPrice currency={currency} old={oldPrice} new={newPrice} />
                <li>
                  <Rating stars={stars} />
                </li>
                <li className="as-promotions" />
                <li className="as-pdp-prodvariations">
                  <SizeComponent data={size} />
                  <CapacityComponent data={capacity} />
                  <ColorComponent color={swatchbtncolor} img={swatchbtnimg} />
                </li>
                <li className="as-pdp-addtocart">
                  <ProductCart />
                </li>
                <li className="as-purchaseinfo-availabilityinfo row">
                  <ProductDeliveryInfo />
                </li>
                <li className="as-purchaseinfo-share">
                  <ProductSocial />
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div className="col-md-7 offset-md-1">
          <ProductImage src={image} />
        </div>
      </div>
    );
  }
}
export default ProductOverview;
