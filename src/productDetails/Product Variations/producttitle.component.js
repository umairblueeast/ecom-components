import React, { PropTypes } from 'react';
import '../cssFile.css';

const propTypes = {
  title: PropTypes.string,
};
const defaultProps = {
  title: '',
};
const ProductTitle = props => (
  <div id="title" className="as-pdp-title">
    <h1 itemProp="name">{props.title}</h1>
  </div>
);
ProductTitle.PropTypes = propTypes;
ProductTitle.defaultProps = defaultProps;
export default ProductTitle;
