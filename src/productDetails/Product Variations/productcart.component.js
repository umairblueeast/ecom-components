import React from 'react';
import '../cssFile.css';

const ProductCart = props => (
  <div className="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
    <div className="btn-group add-to-cart-btn-group" role="group" aria-label="Second group">
      <button type="button" className="btn btn-primary btn-block">Add to Cart</button>
    </div>
    <div className="btn-group btn-group-icon" role="group" aria-label="Third group">
      <button type="button" className="btn btn-link" data-toggle="tooltip" data-placement="top" title="" data-original-title="Add to Favorite">
        <span className="icon icon-favorite" style={{ display: 'inline-block' }} />
      </button>
      <button type="button" className="btn btn-link" data-toggle="tooltip" data-placement="top" title="" data-original-title="Compare Product">
        <span className="icon icon-compare" style={{ display: 'inline-block' }} />
      </button>
    </div>
  </div>
  );
export default ProductCart;
