import React, { PropTypes } from 'react';

import '../cssFile.css';
const propTypes = {
  data: PropTypes.array,
};
const defaultProps = {
  data: [],
};
class CapacityComponent extends React.Component {
  render() {
    const { data } = this.props;
    let buttons = [];
    data.forEach((data, i) => {
      buttons.push(<button type="button" className="btn btn-secondary btn-sm" key={data}>{data}</button>);
    });
    return (
      <div className="as-pdp-prodvariation">
        <label htmlFor="Capacity" className="as-pdp-prodvariations-title">Capacity</label>
        {buttons}
      </div>
    );
  }
}
CapacityComponent.PropTypes = propTypes;
CapacityComponent.defaultProps = defaultProps;
export default CapacityComponent;
