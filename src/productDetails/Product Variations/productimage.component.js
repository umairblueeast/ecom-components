import React, { PropTypes } from 'react';
import '../cssFile.css';

const propTypes = {
  src:PropTypes.string
};
const defaultProps = {
  src:''
};
const ProductImage = props => (
  <div className="product-info">
    <div className="gallery">
      <img className="img-fluid" src={props.src} />
    </div>
  </div>
  );
ProductImage.PropTypes = propTypes;
ProductImage.defaultProps = defaultProps;
export default ProductImage;
