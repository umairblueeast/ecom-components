import React, { PropTypes } from 'react';
import '../cssFile.css';

const propTypes = {
  currency : PropTypes.string,
  old: PropTypes.number,
  new: PropTypes.number,
};
const defaultProps = {
  currency : 'Rs.',
  old: 0,
  new: 0,
};
const ProductPrice = props => (
  <li className="as-pdp-price">
    <span className="as-price-currentprice">
      <span itemScope="itemscope" itemProp="offers" itemType="http://schema.org/Offer">
        <span itemProp="price">{props.currency} {props.new.toLocaleString('en-IN')}</span>
      </span>
    </span>
    <span className="as-price-previousprice">
      {props.currency} {props.old.toLocaleString('en-IN')}
    </span>
  </li>
  );
ProductPrice.PropTypes = propTypes;
ProductPrice.defaultProps = defaultProps;
export default ProductPrice;
