import React, { PropTypes } from 'react';

import '../cssFile.css';

let options = [];
class SizeComponent extends React.Component {
    static propTypes = {
        data : PropTypes.array
    }
    static defaultProps = {
        data: []
    }
  render() {
    const { data } = this.props;
    data.forEach((option, i) => {
      options.push(<option value={option.value} key={option.value}>{option.label}</option>);
    });

    return (
      <div className="as-pdp-prodvariation">
        <label htmlFor="size" className="as-pdp-prodvariations-title">Size</label>

        <select className="product-variation-list form-dropdown form-textbox custom-select">
          {options}
        </select>
      </div>
    );
  }
}
export default SizeComponent;
