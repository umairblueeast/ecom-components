import React, { PropTypes } from 'react';
import '../cssFile.css';

const propTypes = {
  color: PropTypes.string,
  img:PropTypes.string
};
const defaultProps = {
  color: '',
  img:''
};
const ColorSwatchBtn = (props) => {
  const {
    color,
    img,
  } = props;
  if (img) {
    return (
      <label htmlFor="SwatchBTn" className="btn btn-color-swatch colornav-link" aria-pressed="true">
        <input type="radio" name="options" id="option2" autoComplete="off" />
        <figure className="btn-color-swatch-img">
          <img src={img} alt="" width="32" height="32" />
        </figure>
      </label>
    );
  }
  const style = {};
  style.backgroundColor = color;
  return (
    <label htmlFor="SwatchBTncolor" className="btn btn-color-swatch" aria-pressed="true">
      <div className="btn-color-swatch-wrapper" style={style}>
        <input type="radio" name="options" id="option3" autoComplete="off" />
      </div>
    </label>
  );

};
ColorSwatchBtn.propTypes = propTypes;
ColorSwatchBtn.defaultProps = defaultProps;
export default ColorSwatchBtn;
