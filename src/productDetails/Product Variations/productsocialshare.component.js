import React from 'react';
import '../cssFile.css';

const ProductSocial = props => (
  <div className="btn-group btn-group-icon" role="group" aria-label="Third group">
    <button type="button" className="btn btn-link">
      <span className="icon icon-facebook" style={{ display: 'inline-block' }} />
    </button>
    <button type="button" className="btn btn-link">
      <span className="icon icon-twitter" style={{ display: 'inline-block' }} />
    </button>
    <button type="button" className="btn btn-link">
      <span className="icon icon-linkedin" style={{ display: 'inline-block' }} />
    </button>
  </div>
  );
export default ProductSocial;
