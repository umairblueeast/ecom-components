import React, { PropTypes } from 'react';

const propTypes = {
  value: PropTypes.string,
};
const DefaultProps = {
  value: '',
};

const SpecsPropertyHeading = props => <dt>{props.value}</dt>;

SpecsPropertyHeading.propTypes = propTypes;
SpecsPropertyHeading.defaultProps = DefaultProps;

export default SpecsPropertyHeading;
