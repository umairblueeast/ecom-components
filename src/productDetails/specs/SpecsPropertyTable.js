import React, { PropTypes } from 'react';

const propTypes = {
  heading:PropTypes.string,
  value: PropTypes.string,
};
const DefaultProps = {
  heading:'',
  value: '',
};

const SpecsPropertyTable = props => (
  <tr>
    <th>{props.heading}</th>
    <td>{props.value}</td>
  </tr>
  );

SpecsPropertyTable.propTypes = propTypes;
SpecsPropertyTable.defaultProps = DefaultProps;

export default SpecsPropertyTable;
