import React, { PropTypes } from 'react';

const propTypes = {
  value: PropTypes.string,
};
const DefaultProps = {
  value: '',
};

const SpecsPropertyValue = props => <dd>{props.value}</dd>;

SpecsPropertyValue.propTypes = propTypes;
SpecsPropertyValue.defaultProps = DefaultProps;

export default SpecsPropertyValue;
