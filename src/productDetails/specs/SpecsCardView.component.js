import React, { PropTypes } from 'react';

import SpecsPropertyHeading from './SpecsPropertyHeading';
import SpecsPropertyValue from './SpecsPropertyValue';
import SpecsIcon from './SpecsIcon';

import '../cssFile.css';

let properties = [];

class SpecsCardView extends React.Component {
  static propTypes = {
    heading: PropTypes.string,
    data: PropTypes.array,
    iconName: PropTypes.string,
    iconGradient: PropTypes.string,
    iconColor: PropTypes.string,
  };

  static defaultProps = {
    heading: '',
    data: [],
    iconName: '',
    iconGradient: '',
    iconColor: '',
  };
  render() {
    const {
      data,
      heading,
      iconName,
      iconGradient,
      iconColor,
    } = this.props;
    data.forEach(property => {
      properties.push(<SpecsPropertyHeading value={property.label} key={property.label} />);
      properties.push(<SpecsPropertyValue value={property.value} key={property.value} />);
    });
    if (iconName) var icon = <SpecsIcon name={iconName} gradient={iconGradient} color={iconColor} />;
    return (
      <div className="card">
        <div className="card-block">
          {icon}
          <h3 className="card-title">{heading}</h3>
          <dl>
            {properties}
          </dl>
        </div>
      </div>
    );
  }
}
export default SpecsCardView;
