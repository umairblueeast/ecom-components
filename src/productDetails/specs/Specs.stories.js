import React from 'react';
import { storiesOf } from '@kadira/storybook';

import { withKnobs, array,  text } from '@kadira/storybook-addon-knobs';
import SpecsCardView from './SpecsCardView.component';
import SpecsTableView from './SpecsTableView.component';

const stories = storiesOf('Specs', module);
stories.addDecorator(withKnobs);
const properties = [{ label: 'Rated Capacity', value: '3500 (1500-3900) W' }, { label: 'Rated Power Consumption', value: '1100 (420-1560) W' }, { label: 'Running Current', value: '4.8 (2.0-6.8) A' }];

stories.addWithInfo('Specs card view', () => {
  const iconname = text('IconName', 'sun-o');
  const iconcolor = text('IconColor', '#FF2A68');
  const icongradient = text('IconGradient', '#FF5E3A');
  const property = array('Properties', properties);
  const heading = text('Heading', 'Heating');
  return <SpecsCardView iconName={iconname} iconColor={iconcolor} iconGradient={icongradient} data={property} heading={heading} />;
});
stories.addWithInfo('Specs table view', () => {
  const iconname = text('IconName', 'sun-o');
  const iconcolor = text('IconColor', '#FF2A68');
  const icongradient = text('IconGradient', '#FF5E3A');
  const property = array('Properties', properties);
  const heading = text('Heading', 'Heating');
  return <SpecsTableView iconName={iconname} iconColor={iconcolor} iconGradient={icongradient} data={property} heading={heading} />;
});
stories.addWithInfo('Specs table without icon', () => <SpecsTableView iconColor="green" iconGradient="yellow" data={properties} heading="Cooling" />);
stories.addWithInfo('Specs table without heading', () => <SpecsTableView iconName="television" iconColor="green" iconGradient="yellow" data={properties} />);
stories.addWithInfo('Specs card without icon', () => <SpecsCardView iconColor="green" iconGradient="yellow" data={properties} heading="Cooling" />);
stories.addWithInfo('Specs card without heading', () => <SpecsCardView iconName="television" iconColor="green" iconGradient="yellow" data={properties} />);
