import React from 'react';

const SpecsIcon = (props) => {
  const iconStyle = {
    backgroundImage: `linear-gradient(to bottom, ${props.gradient} 0%, ${props.color} 100%)`,
  };
  return (
    <span className="app-icon" style={iconStyle}>
      <i className={`fa fa-${props.name}`} aria-hidden="true" />
    </span>
  );
};
export default SpecsIcon;
