import React, { PropTypes } from 'react';
import Icon from 'react-fa';

import SpecsPropertyTable from './SpecsPropertyTable';
import SpecsIcon from './SpecsIcon';
import '../cssFile.css';

let properties = [];

class SpecsTableView extends React.Component {
  static propType = {
    heading: PropTypes.string,
    data: PropTypes.array,
    iconName: PropTypes.string,
    iconGradient: PropTypes.string,
    iconColor: PropTypes.string,
  };

  static defaultProps = {
    heading: '',
    data: [],
    iconName: '',
    iconGradient: '',
    iconColor: '',
  };
  render() {
    const {
      data,
      heading,
      iconName,
      iconGradient,
      iconColor,
    } = this.props;

    data.forEach((property, i) => {
      properties.push(<SpecsPropertyTable heading={property.label} value={property.value} key={i} />);
    });
    let icon = '';
    if (iconName) {
      icon = <SpecsIcon name={iconName} gradient={iconGradient} color={iconColor} />;
    }
    return (
      <div className="as-productinfosection-panel row">
        <div className="col-md-3">
          <div className="as-productinfosection-sidepanel">
            {icon}
            <h3>{heading}</h3>
          </div>
        </div>
        <div className="col-md-9 ">
          <div className="as-productinfosection-mainpanel">
            <table className="table spec-table">
              <tbody>
                {properties}
              </tbody>
            </table>

          </div>
        </div>
      </div>
    );
  }
}
export default SpecsTableView;
