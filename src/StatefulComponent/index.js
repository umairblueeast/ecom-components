import React from 'react';
import StatefulComponent from './StatefulComponent.component';

export default StatefulComponent;

export function stateful(component, options) {

  options = options || {};
  options.component = component;

  const StatefulComponentWrapper = props => (<StatefulComponent component={component} {...options} {...props} />);

  return StatefulComponentWrapper;
}
