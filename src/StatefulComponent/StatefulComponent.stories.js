import React from 'react';
import { storiesOf } from '@kadira/storybook';

import StatefulComponent from './StatefulComponent.component';

// Mock Data
import {
  loadedData,
  noData,
  loadingData,
  errorData,
  loadingAndErrorData,
  customDataField,
  customCheckDataFunctionBad,
  customCheckDataFunctionEmpty,
  customCheckDataFunctionFilled
} from '../../__mocks__/StatefulComponent.mock';

const stories = storiesOf('Stateful Component', module);
// stories.addDecorator(withKnobs);


const TestComponent = props => (<div>{props.data.query}</div>);
const ViewerComponent = props => (<div>{props.data.viewer}</div>);
const NestedDataComponent = props => {

  const count = props.data.viewer.sites.edges.length;

  return (<div>{`I have ${count} items`}</div>);
};

const checkData = function(props, dataProp) {
  try {
    const data = props.data.viewer.sites.edges;

    if (data && data.length > 0) {
      return true;
    }
  } catch (error) {
    return false;
  }

  return false;
};

stories.addWithInfo('Loaded Data', () => (
  <StatefulComponent component={TestComponent} data={loadedData} />
  ));

stories.addWithInfo('No Data (null)', () => (
  <StatefulComponent component={TestComponent} data={noData} />
));

stories.addWithInfo('Loading State', () => (
  <StatefulComponent component={TestComponent} data={loadingData} />
));

stories.addWithInfo('Error State', () => (
  <StatefulComponent component={TestComponent} data={errorData} />
));

stories.addWithInfo('Loading & Error State', () => (
  <StatefulComponent component={TestComponent} data={loadingAndErrorData} />
));

stories.addWithInfo('Custom Data Field', () => (
  <StatefulComponent component={ViewerComponent} data={customDataField} dataField="viewer" />
));

stories.addWithInfo('Custom Data Checking (empty data)', () => (
  <StatefulComponent component={NestedDataComponent} data={customCheckDataFunctionEmpty} checkData={checkData} />
));

stories.addWithInfo('Custom Data Checking (filled data)', () => (
  <StatefulComponent component={NestedDataComponent} data={customCheckDataFunctionFilled} checkData={checkData} />
));

stories.addWithInfo('Custom Data Checking (bad data)', () => (
  <StatefulComponent component={NestedDataComponent} data={customCheckDataFunctionBad} checkData={checkData} />
));

stories.addWithInfo('Child Component throws error', () => {

  // class Bomb extends React.Component {
  //   render() {
  //     throw new Error('Boom!');
  //   }
  // }

  // return (
  //   <StatefulComponent component={Bomb} data={loadedData} />
  // );
  const Bomb = () => { throw new Error('Boom!'); };

  return (
    <StatefulComponent component={Bomb} data={loadedData} />
  );
});

