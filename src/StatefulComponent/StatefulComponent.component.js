import React, { PropTypes } from 'react';
// import pickBy from 'lodash.pickBy';

import ComponentState from '../ComponentState/';

const pickBy = require('lodash.pickby');

const propTypes = {
  // Main component
  component: PropTypes.func.isRequired,

  // Data Identification
  dataField: PropTypes.string,
  data: PropTypes.object,
  checkData: PropTypes.func,

  // State Components
  errorStateComponent: PropTypes.func,
  loadingStateComponent: PropTypes.func,
  emptyStateComponent: PropTypes.func,

  // States
  hasData: PropTypes.bool,
  loading: PropTypes.bool,
  error: PropTypes.object,

  // State Component properties
  errorStateProps: PropTypes.object,
  loadingStateProps: PropTypes.object,
  emptyStateProps: PropTypes.object,

  // Event Listeners
  onData: PropTypes.func,
  onLoadingStart: PropTypes.func,
  onLoadingEnd: PropTypes.func,
  onError: PropTypes.func,
  onEmpty: PropTypes.func,
};

const defaultProps = {

  // Data Identification
  dataField: 'query',
  checkData(props, dataProp) {
    if (props.data[dataProp] === null || props.data[dataProp] === undefined) {
      return false;
    }

    return true;
  },

  // States
  hasData: false,
  loading: false,
  error: null,

  // State Components
  errorStateComponent: ComponentState,
  loadingStateComponent: ComponentState,
  emptyStateComponent: ComponentState,

  // State Component properties
  errorStateProps: {
    card: false,
    title: 'Error',
    description: 'An error occured!'
  },
  loadingStateProps: {
    card: false,
    title: 'Loading',
    description: 'Please wait, we\'re on it!'
  },
  emptyStateProps: {
    card: false,
    title: 'Nothing to show',
    description: 'We seem to have nothing to show at the moment!'
  },

  // Event Listeners
  onData: () => {},
  onLoadingStart: () => {},
  onLoadingEnd: () => {},
  onError: () => {},
  onEmpty: () => {},
};

export default class StatefulComponent extends React.Component {

  constructor(props) {
    super(props);

    const loading = (props.data && props.data.loading !== undefined) ? props.data.loading : props.loading;
    const error = (props.data && props.data.error !== undefined) ? props.data.error : props.error;

    this.state = {
      hasData: props.checkData(props, props.dataField),
      loading,
      error
    };
  }

  componentWillReceiveProps(props) {

    if (!props || !props.data) {
      return;
    }

    const newState = {};

    if (props.data.error !== undefined) {
      newState.error = props.data.error;
    }

    if (props.data.loading !== undefined) {
      newState.loading = props.data.loading;
    }

    const dataProp = this.props.dataField;

    newState.hasData = this.props.checkData(props, dataProp);

    if (!this.hasDataChanged(newState)) {
      return;
    }

    this.setState(newState);
  }

  hasDataChanged(props) {

    if (props.error !== this.state.error) {
      return true;
    }

    if (props.loading !== this.state.loading) {
      if (props.loading === false) {
        this.props.onLoadingEnd();
      }
      return true;
    }

    if (props.hasData !== this.state.hasData) {
      return true;
    }

    return false;
  }

  renderError(error) {
    this.props.onError(error);

    const { errorStateComponent: Component, errorStateProps } = this.props;
    return (<Component {...errorStateProps} />);
  }

  renderLoading() {
    // TODO: This has to be moved to a better location/logic
    this.props.onLoadingStart();

    const { loadingStateComponent: Component, loadingStateProps } = this.props;
    return (<Component {...loadingStateProps} />);
  }

  renderEmpty() {
    this.props.onEmpty();
    const { emptyStateComponent: Component, emptyStateProps } = this.props;
    return (<Component {...emptyStateProps} />);
  }

  renderComponent() {
    this.props.onData();
    const { component: Component, ...props } = this.props;

    // Need to remove this component's props before passing
    // the props to the child component
    const defaultPropKeys = Object.keys(defaultProps);
    const filteredProps = pickBy(props, (value, key) => !defaultPropKeys.includes(key));

    return (<Component {...filteredProps} />);
  }

  render() {
    const { hasData, loading, error } = this.state;

    if (error) {
      return this.renderError();

    } else if (loading && !hasData) {
      return this.renderLoading();

    } else if (hasData) {
      return this.renderComponent();
    }

    return this.renderEmpty();
  }

}

StatefulComponent.propTypes = propTypes;
StatefulComponent.defaultProps = defaultProps;
