import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import Select from 'react-select';
import 'react-select/dist/react-select.css';
import data from '../../__mocks__/SelectDropDown.mock';
import GravatarOption from './GravatarOption';
import GravatarValue from './GravatarValue';

const stories = storiesOf('Select Drop Down', module);

stories.addWithInfo('Complete Drop Down', () => (
  <div>
    <Select
      name="form-field-name"
      value={data[0].value}
      options={data}
      onChange={action('handleChange')}
      clearable
      searchable
      optionComponent={GravatarOption}
      valueComponent={GravatarValue}
    />
  </div>
));

stories.addWithInfo('Without Image', () => (
  <div>
    <Select
      name="form-field-name"
      value={data[0].value}
      options={data}
      onChange={action('handleChange')}
      clearable
      searchable
    />
  </div>
));

stories.addWithInfo('Not Clearable', () => (
  <div>
    <Select
      name="form-field-name"
      value={data[0].value}
      options={data}
      onChange={action('handleChange')}
      clearable={false}
      searchable
      optionComponent={GravatarOption}
      valueComponent={GravatarValue}
    />
  </div>
));

stories.addWithInfo('Not Searchable', () => (
  <div>
    <Select
      name="form-field-name"
      value={data[0].value}
      options={data}
      onChange={action('handleChange')}
      clearable
      searchable={false}
      optionComponent={GravatarOption}
      valueComponent={GravatarValue}
    />
  </div>
));