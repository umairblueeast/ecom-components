/**
 * Created by anser on 1/27/17.
 */

import React, { PropTypes } from 'react';

const propTypes = {
  children: PropTypes.node,
  value: PropTypes.object
};

const defaultProps = {
};

class GravatarValue extends React.Component {
  render() {
    return (
      <div className="Select-value">
        <span className="Select-value-label">
          <img
            style={{ borderRadius: 3,
              display: 'inline-block',
              marginRight: 10,
              position: 'relative',
              top: -2,
              verticalAlign: 'middle',
              width: '15px',
              height: '15px',
            }}
            src={this.props.value.icon} role="presentation"
          />
          {this.props.children}
        </span>
      </div>
    );
  }

}

export default GravatarValue;
GravatarValue.propTypes = propTypes;
GravatarValue.defaultProps = defaultProps;
