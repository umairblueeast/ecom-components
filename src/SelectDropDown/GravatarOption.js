
/**
 * Created by anser on 1/3/17.
 */
import React, { PropTypes } from 'react';

const propTypes = {
  children: PropTypes.node,
  onFocus: PropTypes.func,
  onSelect: PropTypes.func,
  option: PropTypes.object,
  className: PropTypes.string,
  isFocused: PropTypes.bool
};

const defaultProps = {
};

class GravatarOption extends React.Component {

  constructor(props) {
    super(props);

    this.handleMouseDown = this.handleMouseDown.bind(this);
    this.handleMouseEnter = this.handleMouseEnter.bind(this);
    this.handleMouseMove = this.handleMouseMove.bind(this);
  }

  handleMouseDown(event) {
    event.preventDefault();
    event.stopPropagation();
    this.props.onSelect(this.props.option, event);
  }
  handleMouseEnter(event) {
    this.props.onFocus(this.props.option, event);
  }
  handleMouseMove(event) {
    if (this.props.isFocused) return;
    this.props.onFocus(this.props.option, event);
  }
  render() {
    return (
      <div
        className={this.props.className}
        onMouseDown={this.handleMouseDown}
        onMouseEnter={this.handleMouseEnter}
        onMouseMove={this.handleMouseMove}
      >
        <img
          style={{ borderRadius: 3,
            display: 'inline-block',
            marginRight: 10,
            position: 'relative',
            top: -2,
            verticalAlign: 'middle',
            width: '15px',
            height: '15px',
          }}
          src={this.props.option.icon} role="presentation"
        />
        {this.props.children}
      </div>
    );
  }

}

export default GravatarOption;
GravatarOption.propTypes = propTypes;
GravatarOption.defaultProps = defaultProps;
