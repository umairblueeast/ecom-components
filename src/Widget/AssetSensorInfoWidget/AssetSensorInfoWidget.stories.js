/**
 * Created by anser on 12/23/16.
 */

import React from 'react';
import { storiesOf } from '@kadira/storybook';
import { withKnobs, text, object } from '@kadira/storybook-addon-knobs';
import AssetSensorInfoWidgetComponent from './AssetSensorInfoWidget.component';

const stories = storiesOf('AssetSensorInfoWidget', module);
stories.addDecorator(withKnobs);

stories.addWithInfo('Asset Sensor Info Widget', () => {
  const ambientTemperature = text('Temperature', '25');
  const current = text('Current', '512');
  const power = text('Power', '600');
  const voltage = text('Voltage', '120');
  const widgetTitle = text('Widget Title', 'Sensor Reading');
  const myData = object('Data', [
    {
      connected: true,
      ambientTemperature,
      current,
      power,
      voltage
    }
  ]);
  return (
    <AssetSensorInfoWidgetComponent myData={myData} widgetTitle={widgetTitle} />
  );
});
