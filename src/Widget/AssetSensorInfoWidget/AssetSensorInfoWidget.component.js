/**
 * Created by anser on 12/23/16.
 */

import React, { PropTypes } from 'react';
import Icon from 'react-fa';
import Widget from '../widget';
import WidgetHeader from '../widgetHeader';
import WidgetTitle from '../widgetTitle';
import WidgetContent from '../widgetContent';

const propTypes = {
  myData: PropTypes.array,
  widgetTitle: PropTypes.string
};

const defaultProps = {
  myData: [],
  widgetTitle: 'Title'
};

export default class AssetSensorInfoWidget extends React.Component {

  static renderConnectionStatus(connected) {
    let status = null;

    if (connected === undefined || connected === null) {
      return (<div />);
    } else if (connected) {
      status = (<span>Online <Icon name="circle text-success" /></span>);
    } else {
      status = (<span>Offline <Icon name="circle text-danger" /></span>);
    }
    return (
      <li className="list-group-item">
        <div className="data-label">Status</div>
        <div className="data-value">{status}</div>
      </li>
    );
  }

  static renderAmbientTemperature(ambientTemperature) {
    if (ambientTemperature === undefined || ambientTemperature === null) {
      return (<div />);
    }

    return (
      <li className="list-group-item">
        <div className="data-label">Room Temperature</div>
        <div className="data-value">{ambientTemperature} ℃</div>
      </li>
    );
  }

  static renderCurrent(current) {
    if (current === undefined || current === null) {
      return (<div />);
    }

    return (
      <li className="list-group-item">
        <div className="data-label">Current</div>
        <div className="data-value">{current} A</div>
      </li>
    );
  }

  static renderPower(power) {
    if (power === undefined || power === null) {
      return (<div />);
    }

    return (
      <li className="list-group-item">
        <div className="data-label">Power</div>
        <div className="data-value">{power} W</div>
      </li>
    );
  }

  static renderVoltage(voltage) {
    if (voltage === undefined || voltage === null) {
      return (<div />);
    }

    return (
      <li className="list-group-item">
        <div className="data-label">Voltage</div>
        <div className="data-value">{voltage} V</div>
      </li>
    );
  }

  render() {
    if (!this.props) {
      return (<div />);
    }

    const {
      myData,
      widgetTitle,
    } = this.props;

    return (
      <Widget>
        <WidgetHeader>
          <WidgetTitle>{widgetTitle}</WidgetTitle>
        </WidgetHeader>
        <WidgetContent>
          {
            myData.map((item, i) => {
              return (
                <ul key={i} className="list-group data-list">
                  { AssetSensorInfoWidget.renderConnectionStatus(item.connected) }
                  { AssetSensorInfoWidget.renderAmbientTemperature(item.ambientTemperature) }
                  { AssetSensorInfoWidget.renderCurrent(item.current) }
                  { AssetSensorInfoWidget.renderPower(item.power) }
                  { AssetSensorInfoWidget.renderVoltage(item.voltage) }
                </ul>
              );
            })
          }
        </WidgetContent>
      </Widget>
    );
  }
}

AssetSensorInfoWidget.propTypes = propTypes;
AssetSensorInfoWidget.defaultProps = defaultProps;

