/**
 * Created by anser on 12/2/16.
 */

import React, { PropTypes } from 'react';

const propTypes = {
  tag: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
  className: PropTypes.string
};

const defaultProps = {
  tag: 'div'
};

const WidgetContent = (props) => {
  const {
    tag: Tag,
    className,
    ...attributes
  } = props;

  return (
    <Tag {...attributes} />
  );
};

WidgetContent.propTypes = propTypes;
WidgetContent.defaultProps = defaultProps;

export default WidgetContent;
