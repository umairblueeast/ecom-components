import React, { PropTypes } from 'react';

import Widget from '../widget';
import WidgetHeader from '../widgetHeader';
import WidgetTitle from '../widgetTitle';
import WidgetContent from '../widgetContent';

import ComponentState from '../../ComponentState/ComponentState';

const propTypes = {
  data: PropTypes.object,
  contentComponent: PropTypes.func.isRequired,
  contentComponentDataProp: PropTypes.string.isRequired,
  title: PropTypes.string
};

const defaultProps = {
  contentComponentDataProp: 'viewer'
};

class StatefulWidgetComponent extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      hasData: false,
      loading: false,
      error: null
    };

    this.onChildUpdate = this.onChildUpdate.bind(this);
  }

  onChildUpdate(props, state) {

    const newState = {};

    if (props.data.error !== undefined) {
      newState.error = props.data.error;
    }

    if (props.data.loading !== undefined) {
      newState.loading = props.data.loading;
    }

    const dataProp = this.props.contentComponentDataProp;
    console.log("checking data", props.data[dataProp])
    if (props.data[dataProp] !== null && props.data[dataProp] !== undefined) {
      newState.hasData = true;
    } else {
      newState.hasData = false;
    }

    if (!this.hasDataChanged(newState)) {
      return;
    }

    this.setState(newState);
  }

  hasDataChanged(props) {

    if (props.error !== this.state.error) {
      return true;
    }

    if (props.loading !== this.state.loading) {
      return true;
    }

    if (props.hasData !== this.state.hasData) {
      return true;
    }

    return false;
  }

  renderContent() {
    const { hasData, loading, error } = this.state;
    const { contentComponent: ContentComponent } = this.props;

    console.log('got here loading', loading);
    console.log('got here hasData', hasData);

    const componentStateProps = {};
    componentStateProps.style = {};

    const contentComponentStyle = {};

    if (error) {
      componentStateProps.card = false;
      componentStateProps.title = 'Error';
      componentStateProps.description = 'An error occured!';
      componentStateProps.style.display = 'initial';
      contentComponentStyle.display = 'none';
    }

    if (loading && !hasData) {
      componentStateProps.card = false;
      componentStateProps.title = 'Loading';
      componentStateProps.description = 'Please hold, our robots are working!';
      componentStateProps.style.display = 'initial';
      contentComponentStyle.display = 'none';
    }

    if (hasData) {
      componentStateProps.style.display = 'none';
      contentComponentStyle.display = 'initial';
    }

    console.log('componentStateProps', componentStateProps)
    console.log('contentComponentStyle', contentComponentStyle)
    return (
      <WidgetContent>
        <ComponentState {...componentStateProps} />
        <ContentComponent
          onComponentUpdate={this.onChildUpdate}
          style={contentComponentStyle}
        />
      </WidgetContent>
    );
  }

  render() {

    const { title } = this.props;

    return (
      <Widget>
        <WidgetHeader>
          <WidgetTitle>{title}</WidgetTitle>
        </WidgetHeader>
        { this.renderContent() }
      </Widget>
    );
  }
}

StatefulWidgetComponent.propTypes = propTypes;
StatefulWidgetComponent.defaultProps = defaultProps;

export default StatefulWidgetComponent;
