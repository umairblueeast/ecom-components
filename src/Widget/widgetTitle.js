/**
 * Created by anser on 12/2/16.
 */

import React, { PropTypes } from 'react';

const propTypes = {
  tag: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
  className: PropTypes.string
};

const defaultProps = {
  tag: 'h4'
};

const WidgetTitle = (props) => {
  const {
    tag: Tag,
    className,
    ...attributes
  } = props;

  return (
    <Tag {...attributes} className={`widget-title ${className}`} />
  );
};

WidgetTitle.propTypes = propTypes;
WidgetTitle.defaultProps = defaultProps;

export default WidgetTitle;
