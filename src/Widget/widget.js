/**
 * Created by anser on 12/2/16.
 */

import React, { PropTypes } from 'react';
import 'bluerain-bootstrap-theme/dist/css/components/widget.css';
const propTypes = {
  tag: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
  className: PropTypes.string
};

const defaultProps = {
  tag: 'div'
};

const Widget = (props) => {
  const {
    tag: Tag,
    className,
    ...attributes
  } = props;

  return (
    <Tag {...attributes} className="widget" />
  );
};

Widget.propTypes = propTypes;
Widget.defaultProps = defaultProps;

export default Widget;
