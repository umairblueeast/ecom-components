import React from 'react';
import { storiesOf } from '@kadira/storybook';
import { Link } from 'react-router';
import {
  withKnobs,
  text, boolean
} from '@kadira/storybook-addon-knobs';

import ComponentState from './ComponentState.component';

const stories = storiesOf('Component State', module);
stories.addDecorator(withKnobs);


stories.addWithInfo('Simple', () => {
  const image = text('Image', 'swearing-male.svg');
  const imageSize = text('Image Size', 'lg');
  const title = text('Title', 'Err..');
  const description = text('Description', 'Looks like something broke.');
  const card = boolean('Card View', true);
  const linkText = text('Link Text', 'Learn More');
  return (
    <ComponentState
      image={image}
      imageSize={imageSize}
      card={card}
      title={title}
      description={description}
      linkTag={Link}
      linkText={linkText}
      linkOnClick={() => console.log("I'm clicked!")}
    />
  );
});

stories.addWithInfo('Without Card', () => {
  const image = text('Image', 'swearing-male.svg');
  const imageSize = text('Image Size', 'lg');
  const title = text('Title', 'Err..');
  const description = text('Description', 'Looks like something broke.');
  const card = boolean('Card View', false);
  const linkText = text('Link Text', 'Learn More');
  return (
    <ComponentState
      image={image}
      imageSize={imageSize}
      card={card}
      title={title}
      description={description}
      linkTag={Link}
      linkText={linkText}
      linkOnClick={() => console.log("I'm clicked!")}
    />
  );
});

stories.addWithInfo('Without Image', () => {
  const imageSize = text('Image Size', 'lg');
  const title = text('Title', 'Err..');
  const description = text('Description', 'Looks like something broke.');
  const card = boolean('Card View', true);
  const linkText = text('Link Text', 'Learn More');
  return (
    <ComponentState
      imageSize={imageSize}
      card={card}
      title={title}
      description={description}
      linkTag={Link}
      linkText={linkText}
      linkOnClick={() => console.log("I'm clicked!")}
    />
  );
});

stories.addWithInfo('Without Title', () => {
  const image = text('Image', 'swearing-male.svg');
  const imageSize = text('Image Size', 'lg');
  const description = text('Description', 'Looks like something broke.');
  const card = boolean('Card View', true);
  const linkText = text('Link Text', 'Learn More');
  return (
    <ComponentState
      image={image}
      imageSize={imageSize}
      card={card}
      description={description}
      linkTag={Link}
      linkText={linkText}
      linkOnClick={() => console.log("I'm clicked!")}
    />
  );
});

stories.addWithInfo('Without description', () => {
  const image = text('Image', 'swearing-male.svg');
  const imageSize = text('Image Size', 'lg');
  const title = text('Title', 'Err..');
  const card = boolean('Card View', true);
  const linkText = text('Link Text', 'Learn More');
  return (
    <ComponentState
      image={image}
      imageSize={imageSize}
      card={card}
      title={title}
      linkTag={Link}
      linkText={linkText}
      linkOnClick={() => console.log("I'm clicked!")}
    />
  );
});

stories.addWithInfo('Without Link', () => {
  const image = text('Image', 'swearing-male.svg');
  const imageSize = text('Image Size', 'lg');
  const title = text('Title', 'Err..');
  const description = text('Description', 'Looks like something broke.');
  const card = boolean('Card View', true);
  return (
    <ComponentState
      image={image}
      imageSize={imageSize}
      card={card}
      title={title}
      description={description}
    />
  );
});
