import React, { PropTypes } from 'react';
import Card from 'reactstrap/lib/Card';
import CardImg from 'reactstrap/lib/CardImg';

import 'bluerain-bootstrap-theme/dist/css/components/component-state.css';

const propTypes = {
  image: PropTypes.string,
  imageSize: PropTypes.string,
  card: PropTypes.bool,
  title: PropTypes.string,
  description: PropTypes.string,
  linkText: PropTypes.string,
  linkTag: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
  linkOnClick: PropTypes.func
};

const defaultProps = {
  imageSize: '100px',
  card: true,
  linkTag: 'a'
};

export default class ComponentState extends React.Component {

  static renderImage(url, width) {
    const imagSRC = `https://s3-us-west-2.amazonaws.com/bluerainimages/${url}`;
    if (url) {
      return (<CardImg width={width} src={imagSRC} />);
    }
  }

  static renderTitle(title) {
    if (title) {
      return (<h4 className="card-title">{title}</h4>);
    }
  }

  static renderDescription(description) {
    if (description) {
      return (<p className="card-text text-muted">{description}</p>);
    }
  }

  static renderLink(Tag, text, onClick) {
    if (text) {
      return (<Tag onClick={onClick}>{text}</Tag>);
    }
  }

  render() {
    const {
      image,
      imageSize,
      card,
      title,
      description,
      linkText,
      linkTag,
      linkOnClick,
      ...props
    } = this.props;

    let imageWidth = '';

    if (imageSize === 'sm') {
      imageWidth = '50px';
    } else if (imageSize === 'lg') {
      imageWidth = '150px';
    } else {
      imageWidth = '100px';
    }

    let MainTag = 'div';

    if (card === true) {
      MainTag = Card;
    }

    return (
      <MainTag className="component-state" {...props} >
        {ComponentState.renderImage(image, imageWidth)}
        {ComponentState.renderTitle(title)}
        {ComponentState.renderDescription(description)}
        {ComponentState.renderLink(linkTag, linkText, linkOnClick)}
      </MainTag>
    );

  }

}

ComponentState.propTypes = propTypes;
ComponentState.defaultProps = defaultProps;

