/**
 * Created by anser on 11/25/16.
 */

import React, { PropTypes } from 'react';
import { Icon } from 'react-fa';
import classNames from 'classnames';
import ListGroupItem from 'reactstrap/lib/ListGroupItem';

const propTypes = {
  tag: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
  className: PropTypes.string,
  chevronRight: PropTypes.bool,
  to: PropTypes.string,
  action: PropTypes.bool,
  children: PropTypes.array
};

const defaultProps = {
  tag: 'div',
  className: '',
  chevronRight: true,
  to: '#',
  action: true,
  children: []
};

const DataListItem = (props) => {
  const {
    tag: Tag,
    chevronRight,
    to,
    action,
    children,
    ...attributes
  } = props;

  // Classes
  const className = classNames('', {
    'list-group-item-action': action
  }, props.className);

  // Chevron
  let chevron = '';
  if (chevronRight === true) {
    chevron =
      (<Tag to={to} className="data-chevron">
        <Icon name="chevron-right" />
      </Tag>);
  }
  return (
    <ListGroupItem {...attributes} className={className}>
      {children}
      {chevron}
    </ListGroupItem>
  );
};

DataListItem.propTypes = propTypes;
DataListItem.defaultProps = defaultProps;

export default DataListItem;
