/**
 * Created by anser on 11/25/16.
 */

import React from 'react';


const DataListItemLabel = props => (
  <div {...props} className="data-label" />
  );


export default DataListItemLabel;
