/**
 * Created by anser on 12/23/16.
 */

import React, { PropTypes } from 'react';
import Icon from 'react-fa';
import { Link } from 'react-router';
import DataList from './DataList';
import DataListItem from './DataListItem';
import DataListItemLabel from './DataListItemLabel';
import DataListItemValue from './DataListItemValue';

const propTypes = {
  myData: PropTypes.array,
};

const defaultProps = {
  myData: [],
};

export default class DataListComponent extends React.Component {

  static valueIcon(valueIcon) {
    if (valueIcon) {
      return (
        <Icon name={valueIcon} />
      );
    }
  }
  static valueText(valueText, valueIcon) {
    if (valueText || valueIcon) {
      return (
        <DataListItemValue>
          {valueText} {DataListComponent.valueIcon(valueIcon)}
        </DataListItemValue>
      );
    }
  }
  static labelText(label) {
    if (label) {
      return (
        <DataListItemLabel>
          {label}
        </DataListItemLabel>
      );
    }
  }
  render() {
    const {
      myData
    } = this.props;

    return (
      <DataList>
        {
          myData.map((item, i) => (
            <DataListItem key={i} tag={Link} to={item.to} action={item.action} chevronRight={item.chevronRight}>
              {DataListComponent.labelText(item.label)}
              {DataListComponent.valueText(item.valueText, item.valueIcon)}
            </DataListItem>
            ))
        }
      </DataList>
    );
  }
}

DataListComponent.propTypes = propTypes;
DataListComponent.defaultProps = defaultProps;
