/**
 * Created by anser on 12/21/16.
 */

import React from 'react';
import { storiesOf } from '@kadira/storybook';
import { withKnobs, text, boolean, object } from '@kadira/storybook-addon-knobs';
import DataListComponent from './DataList.component';
import ButtonComponent from '../Controls/ButtonControl';
import ButtonIconComponent from '../Controls/ButtonIconControl';
const stories = storiesOf('DataList', module);
stories.addDecorator(withKnobs);

stories.addWithInfo('Data List with connected status', () => {
  const label = text('Label', 'Status');
  const valueIcon = text('Icon', 'circle text-success');
  const valueText = text('Value', 'Connected');
  const chevronRight = boolean('Right Navigation', true);
  const action = boolean('Action Item', true);
  const url = text('Navigation URL', '/app');
  const myData = object('Data', [
    {
      to: url,
      action,
      chevronRight,
      label,
      valueIcon,
      valueText
    }
  ]);
  return (
    <div>
      <ButtonIconComponent />
      <DataListComponent myData={myData} />
    </div>
  );
});

stories.addWithInfo('Data List with disconnected status', () => {
  const label = text('Label', 'Status');
  const valueIcon = text('Icon', 'circle text-danger');
  const valueText = text('Value', 'Disconnected');
  const chevronRight = boolean('Right Navigation', true);
  const action = boolean('Action Item', true);
  const url = text('Navigation URL', '/app');
  const myData = object('Data', [
    {
      to: url,
      action,
      chevronRight,
      label,
      valueIcon,
      valueText
    }
  ]);
  return (
    <DataListComponent myData={myData} />
  );
});

stories.addWithInfo('Data List without Action', () => {
  const label = text('Label', 'Status');
  const valueIcon = text('Icon', 'circle text-success');
  const valueText = text('Value', 'Connected');
  const chevronRight = boolean('Right Navigation', true);
  const action = boolean('Action Item', false);
  const url = text('Navigation URL', '/app');
  const myData = object('Data', [
    {
      to: url,
      action,
      chevronRight,
      label,
      valueIcon,
      valueText
    }
  ]);
  return (
    <DataListComponent myData={myData} />
  );
});

stories.addWithInfo('Wi-Fi protected Data List', () => {
  const label = text('Label', 'Status');
  const valueIcon = text('Icon', 'lock');
  const chevronRight = boolean('Right Navigation', true);
  const action = boolean('Action Item', true);
  const url = text('Navigation URL', '/app');
  const myData = object('Data', [
    {
      url,
      action,
      chevronRight,
      label,
      valueIcon,
    }
  ]);
  return (
    <DataListComponent myData={myData} />
  );
});

stories.addWithInfo('Without status icon', () => {
  const label = text('Label', 'Brand');
  const valueText = text('Value', 'Orient');
  const chevronRight = boolean('Right Navigation', true);
  const action = boolean('Action Item', true);
  const url = text('Navigation URL', '/app');
  const myData = object('Data', [
    {
      to: url,
      action,
      chevronRight,
      label,
      valueText
    }
  ]);
  return (
    <DataListComponent myData={myData} />
  );
});

stories.addWithInfo('Without navigation', () => {
  const label = text('Label', 'Model');
  const valueText = text('Value', 'Ultron white, 1.5 Ton');
  const chevronRight = boolean('Right Navigation', false);
  const action = boolean('Action Item', true);
  const myData = object('Data', [
    {
      label,
      valueText,
      chevronRight,
      action
    }
  ]);
  return (
    <DataListComponent myData={myData} />
  );
});

stories.addWithInfo('Without label', () => {
  const valueIcon = text('Icon', 'circle text-success');
  const valueText = text('Value', 'Connected');
  const chevronRight = boolean('Right Navigation', true);
  const action = boolean('Action Item', true);
  const url = text('Navigation URL', '/app');
  const myData = object('Data', [
    {
      to: url,
      action,
      chevronRight,
      valueIcon,
      valueText
    }
  ]);
  return (
    <DataListComponent myData={myData} />
  );
});

stories.addWithInfo('Without value', () => {
  const label = text('Label', 'Status');
  const chevronRight = boolean('Right Navigation', true);
  const action = boolean('Action Item', true);
  const url = text('Navigation URL', '/app');
  const myData = object('Data', [
    {
      to: url,
      action,
      chevronRight,
      label,
    }
  ]);
  return (
    <DataListComponent myData={myData} />
  );
});
