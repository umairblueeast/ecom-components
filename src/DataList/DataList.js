/**
 * Created by anser on 11/25/16.
 */

import React from 'react';
import ListGroup from 'reactstrap/lib/ListGroup';

import 'bluerain-bootstrap-theme/dist/css/components/data-list.css';


const DataList = props => (
  <ListGroup {...props} className="data-list" />
  );

export default DataList;
