/**
 * Created by atif on 12/14/16.
 */

import React, { PropTypes } from 'react';

const propTypes = {
  className: PropTypes.string
};

const defaultProps = {
};

const AppContent = (props) => {
  const {
    className,
    ...attributes
  } = props;

  return (
    <div {...attributes} className="app-content" />
  );
};

AppContent.propTypes = propTypes;
AppContent.defaultProps = defaultProps;

export default AppContent;
