import React, { PropTypes } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Slider from 'material-ui/Slider';

import Block from './Block/Block';
import SliderItem from './SliderComponent/SliderItem';
import SliderItemLabel from './SliderComponent/SliderItemLabel';
import SliderItemValue from './SliderComponent/SliderItemValue';

const propTypes = {
  label: PropTypes.string,
  className: PropTypes.string,
  min: PropTypes.number,
  max: PropTypes.number,
  step: PropTypes.number,
  value: PropTypes.number,
  onChange: PropTypes.func,
  onDragStart: PropTypes.func,
  onDragStop: PropTypes.func,
};

const defaultProps = {
  label: '',
  className: '',
  min: 0,
  max: 100,
  step: 1,
  value: 50,
  onChange: () => {
  },
  onDragStart: () => {
  },
  onDragStop: () => {
  },
};

const style = {
  control: {
    display: 'flex'
  },
  label: {
    flexGrow: 1
  },
  value: {
    paddingLeft: 10,
    fontWeight: 'bold'
  }
};


export default class SliderControl extends React.Component {

  constructor(props) {
    super(props);

    this.onChange = this.onChange.bind(this);
    this.onDragStart = this.onDragStart.bind(this);
    this.onDragStop = this.onDragStop.bind(this);

    this.state = {
      value: props.value,
    };
  }

  onChange(ev, value) {
    this.setState({
      value,
    });

    this.props.onChange(value);
  }

  onDragStart() {
    this.props.onDragStart(this.state.value);
  }

  onDragStop() {
    this.props.onDragStop(this.state.value);
  }

  render() {
    const {
      value,
    } = this.state;

    const {
      label,
      min,
      max,
      step,
      className
    } = this.props;

    return (
      <Block className={`device-control-item slider-control ${className}`}>
        <SliderItem style={style.control}>
          <SliderItemLabel className="control-label" style={style.label}>
            {label}
          </SliderItemLabel>
          <SliderItemValue className="control-value" style={style.value}>
            {value}
          </SliderItemValue>
        </SliderItem>
        <MuiThemeProvider>
          <Slider
            value={value}
            min={min}
            max={max}
            step={step}
            className="control-slider"
            onChange={this.onChange}
            onDragStart={this.onDragStart}
            onDragStop={this.onDragStop}
          />
        </MuiThemeProvider>
      </Block>
    );
  }
}

SliderControl.propTypes = propTypes;
SliderControl.defaultProps = defaultProps;
