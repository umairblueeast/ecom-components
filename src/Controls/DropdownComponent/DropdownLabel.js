/**
 * Created by anser on 11/29/16.
 */

import React, { PropTypes } from 'react';

const propTypes = {
  tag: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
  className: PropTypes.string
};

const defaultProps = {
  tag: 'span'
};

const DropdownLabel = (props) => {
  const {
    tag: Tag,
    className,
    ...attributes
  } = props;

  return (
    <Tag {...attributes} className={className} />
  );
};

DropdownLabel.propTypes = propTypes;
DropdownLabel.defaultProps = defaultProps;

export default DropdownLabel;
