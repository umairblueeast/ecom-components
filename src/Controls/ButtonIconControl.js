import React, { PropTypes } from 'react';
import { Col } from 'reactstrap';
import ListGroupItem from 'reactstrap/lib/ListGroupItem';
import 'bluerain-bootstrap-theme/dist/css/components/data-list.css';
import  Icon from 'react-fa';
import DataList from '../DataList/DataList';

const propTypes = {
  play: PropTypes.func,
  pause: PropTypes.func,
  forward: PropTypes.func,
  backward: PropTypes.func
};

const defaultProps = {
  play: () => {},
  pause: () => {},
  forward: () => {},
  backward: () => {}
};

export default class ButtonControl extends React.Component {

  render() {
    const {
      play,
      pause,
      forward,
      backward
    } = this.props;

    return (
      <DataList>
        <ListGroupItem>
          <Col lg="3" md="3" sm="3" xs="3" style={{ textAlign: 'center' }}>
            <button type="button" onClick={backward} className="btn-link" style={{ outline: 'none', color: '#444' }}><Icon name="backward fa-2x" /></button>
          </Col>
          <Col lg="3" md="3" sm="3" xs="3" style={{ textAlign: 'center' }}>
            <button type="button" onClick={play} className="btn-link" style={{ outline: 'none', color: '#444' }}><Icon name="play fa-2x" /></button>
          </Col>
          <Col lg="3" md="3" sm="3" xs="3" style={{ textAlign: 'center' }}>
            <button type="button" onClick={pause} className="btn-link" style={{ outline: 'none', color: '#444' }}><Icon name="pause fa-2x" /></button>
          </Col>
          <Col lg="3" md="3" sm="3" xs="3" style={{ textAlign: 'center' }}>
            <button type="button" onClick={forward} className="btn-link" style={{ outline: 'none', color: '#444' }}><Icon name="forward fa-2x" /></button>
          </Col>
        </ListGroupItem>
      </DataList>
    );
  }
}

ButtonControl.propTypes = propTypes;
ButtonControl.defaultProps = defaultProps;
