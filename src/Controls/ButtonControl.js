import React, { PropTypes } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import RaisedButton from 'material-ui/RaisedButton';


const propTypes = {
    label: PropTypes.string,
    defaultValue: PropTypes.string,
    className: PropTypes.string,
};

const defaultProps = {
    label: 'Submit',
    defaultValue: '',
    className: '',
};

const style = {
    control: {
        paddingBottom: 20,
        fontWeight: 'bold',
    },
};


export default class ButtonControl extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            value: props.defaultValue,
        };
    }

    render() {
        const {
            label,
            defaultValue,
            className,
        } = this.props;

        return (
            <div className={`device-control-item slider-control ${className}`}>
                <div style={style.control}>
                    <div className="control-label" style={style.label}>
                        {label}
                    </div>
                    <div className="control-value">
                        {defaultValue}
                    </div>
                </div>
                <MuiThemeProvider>
                    <RaisedButton label="Submit" />
                </MuiThemeProvider>
            </div>
        );
    }
}

ButtonControl.propTypes = propTypes;
ButtonControl.defaultProps = defaultProps;
