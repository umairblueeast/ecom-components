/**
 * Created by anser on 11/29/16.
 */

import React, { PropTypes } from 'react';

const propTypes = {
  tag: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
  className: PropTypes.string
};

const defaultProps = {
  tag: 'div'
};

const Block = (props) => {
  const {
    tag: Tag,
    className,
    ...attributes
  } = props;

  return (
    <Tag {...attributes} className={className} />
  );
};

Block.propTypes = propTypes;
Block.defaultProps = defaultProps;

export default Block;
