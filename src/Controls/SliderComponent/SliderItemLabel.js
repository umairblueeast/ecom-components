/**
 * Created by anser on 11/29/16.
 */

import React, { PropTypes } from 'react';

const propTypes = {
  tag: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
  className: PropTypes.string
};

const defaultProps = {
  tag: 'div'
};

const SliderItemLabel = (props) => {
  const {
    tag: Tag,
    className,
    ...attributes
  } = props;

  return (
    <Tag {...attributes} className={className} />
  );
};

SliderItemLabel.propTypes = propTypes;
SliderItemLabel.defaultProps = defaultProps;

export default SliderItemLabel;
