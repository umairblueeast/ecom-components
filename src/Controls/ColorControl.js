import React, { PropTypes } from 'react';
import Block from './Block/Block';

import { SketchPicker } from 'react-color';
// const { Hue } = require('react-color/lib/components/common');
// const { Saturation } = require('react-color/lib/components/common');

const styles = {

  colorControl: {
    display: 'flex'
  },

  controlLabel: {
    flexGrow: '1'
  },

  swatch: {
    padding: '5px',
    background: '#fff',
    borderRadius: '1px',
    boxShadow: '0 0 0 1px rgba(0,0,0,.1)',
    cursor: 'pointer',
  },
  popover: {
    position: 'absolute',
    zIndex: '2',
  },
  cover: {
    position: 'fixed',
    top: '0px',
    right: '0px',
    bottom: '0px',
    left: '0px',
  },
};


const propTypes = {
  label: PropTypes.string,
  className: PropTypes.string,
  onChange: PropTypes.func,
  onChangeComplete: PropTypes.func,
  defaultValue: PropTypes.object
};

const defaultProps = {
  label: '',
  className: '',
  onChange: () => {},
  onChangeComplete: () => {},
  defaultValue: {
    r: '241',
    g: '112',
    b: '19',
    a: '1',
  }
};

const style = {
  label: {
    marginBottom: 0,
  }
};

class ColorControl extends React.Component {

  // state = {
  //   displayColorPicker: false,
  //   color: defaultProps.defaultValue,
  // }

  constructor(props) {
    super(props);

    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleChangeComplete = this.handleChangeComplete.bind(this);

    this.state = {
      displayColorPicker: false,
      color: defaultProps.defaultValue,
    };
  }

  handleClick() {
    this.setState({ displayColorPicker: !this.state.displayColorPicker });
  }

  handleClose() {
    this.setState({ displayColorPicker: false });
  }

  handleChange(color) {
    this.setState({ color: color.rgb });
    this.props.onChange(color);
  }

  handleChangeComplete(color) {
    this.setState({ color: color.rgb });
    this.props.onChangeComplete(color);
  }

  render() {
    const {
      label,
      className,
    } = this.props;

    const {
      color,
    } = this.state;

    let background = 'rgba(255,255,255,0)';

    if (this.state) {
      background = `rgba(${color.r}, ${color.g}, ${color.b}, ${color.a})`;
    }

    const colorStyle = {
      width: '36px',
      height: '14px',
      borderRadius: '2px',
      background
    };

    return (
      <Block style={styles.colorControl} className={`device-control-item color-control ${className}`}>
        <div style={styles.controlLabel} className="control-label">
          {label}
        </div>
        <div className="control-value">
          <div style={styles.swatch} onClick={this.handleClick}>
            <div style={colorStyle} />
          </div>
          { this.state.displayColorPicker ? <div style={styles.popover}>
            <div style={styles.cover} onClick={this.handleClose} />
            <SketchPicker color={this.state.color} onChange={this.handleChange} onChangeComplete={this.handleChangeComplete} />
          </div> : null }

        </div>
      </Block>
    );
  }
}

ColorControl.propTypes = propTypes;
ColorControl.defaultProps = defaultProps;

export default ColorControl;
