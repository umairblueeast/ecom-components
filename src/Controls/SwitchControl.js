import React, { PropTypes } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Toggle from 'material-ui/Toggle';
import Block from './Block/Block';

const propTypes = {
  label: PropTypes.string,
  onToggle: PropTypes.func,
  defaultValue: PropTypes.bool,
  switchState: PropTypes.bool
};

const defaultProps = {
  label: '',
  onToggle: () => {},
  defaultValue: false,
  switchState: false
};

const style = {
  label: {
    marginBottom: 0,
  }
};

const SwitchControl = () => ({

  render() {
    const {
      label,
      onToggle,
      defaultValue,
      className,
      switchState
    } = this.props;
    return (
      <Block className={`device-control-item switch-control ${className}`}>
        <MuiThemeProvider>
          <Toggle
            label={label}
            labelStyle={style.label}
            defaultToggled={defaultValue}
            onToggle={(ev, toggle) => onToggle(toggle)}
            toggled={switchState}
          />
        </MuiThemeProvider>
      </Block>
    );
  },
});

SwitchControl.propTypes = propTypes;
SwitchControl.defaultProps = defaultProps;

export default SwitchControl;
