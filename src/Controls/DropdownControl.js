import React, { PropTypes } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';

import Block from './Block/Block';
import DropdownLabel from './DropdownComponent/DropdownLabel';

import './dropdown.scss';

const propTypes = {
  label: PropTypes.string,
  defaultValue: PropTypes.string,
  className: PropTypes.string,
  onChange: PropTypes.func,
  items: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
  }))
};

const defaultProps = {
  label: '',
  className: '',
  // defaultValue: 1,
  items: [],
  onChange: () => {
  }
};

const style = {
  control: {
    display: 'flex'
  },
  label: {
    flexGrow: 1,
    lineHeight: '29px'
  },
  dropdown: {
    height: '29px'
  },
  dropdownLabel: {
    lineHeight: '29px',
    paddingRight: '32px'
  },
  dropdownIcon: {
    top: '3px',
    right: 0
  },
  dropdownUnderline: {
    position: 'relative'
  },
};
export default class DropdownControl extends React.Component {

  constructor(props) {
    super(props);

    this.onChange = this.onChange.bind(this);

    this.state = {
      value: props.defaultValue,
    };
  }

  onChange(event, index, value) {
    this.setState({
      value
    });

    this.props.onChange(value);
  }

  render() {
    const { value } = this.state;

    const {
      label,
      items,
      className,
    } = this.props;

    return (
      <MuiThemeProvider>
        <Block
          className={`device-control-item dropdown-control clearfix ${className}`}
          style={style.control}
        >
          <DropdownLabel
            className="control-label"
            style={style.label}
          >
            {label}
          </DropdownLabel>
          <DropDownMenu
            value={value}
            onChange={this.onChange}
            style={style.dropdown}
            labelStyle={style.dropdownLabel}
            iconStyle={style.dropdownIcon}
            underlineStyle={style.dropdownUnderline}
          >
            {
              items.map((item) => (
                <MenuItem
                  key={item.value}
                  value={item.value}
                  primaryText={item.label}
                  className="dropdown-control-item"
                />
              ))
            }
          </DropDownMenu>
        </Block>
      </MuiThemeProvider>
    );
  }
}

DropdownControl.propTypes = propTypes;
DropdownControl.defaultProps = defaultProps;
