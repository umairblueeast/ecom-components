import React, { PropTypes } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import TextField from 'material-ui/TextField';

const propTypes = {
    label: PropTypes.string,
    className: PropTypes.string,
    defaultValue: PropTypes.string,
    inputValue: PropTypes.string,
};

const defaultProps = {
    label: '',
    className: '',
    defaultValue: 'Input Field',
    inputValue: 'John Hopkins',
};

const style = {
    control: {
        display: 'flex',
    },
    label: {
        flexGrow: 1,
    },
    value: {
        paddingLeft: 10,
        fontWeight: 'bold',
    },
    // field: {
    //   border:'1px solid grey'
    // },
};


export default class InputControl extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            value: props.defaultValue,
        };
    }

    render() {
        const {
            label,
            defaultValue,
            inputValue,
            className,
        } = this.props;

        return (
            <div className={`device-control-item slider-control ${className}`}>
                <div style={style.control}>
                    <div className="control-label" style={style.label}>
                        {label}
                    </div>
                    <div className="control-value" style={style.value}>
                        {defaultValue}
                    </div>
                </div>
                <MuiThemeProvider>
                    <TextField
                        hintText={inputValue}
                    />
                </MuiThemeProvider>
            </div>
        );
    }
}

InputControl.propTypes = propTypes;
InputControl.defaultProps = defaultProps;
