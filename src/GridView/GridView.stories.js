/**
 * Created by anser on 12/22/16.
 */

import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import { withKnobs, text } from '@kadira/storybook-addon-knobs';
import GridView from './GridView.component';
import GridViewItem from './GridViewItem.component';
import CardView from '../Card/CardView/CardView.component';
import data from '../../__mocks__/CardView.mock';

const stories = storiesOf('GridView', module);
stories.addDecorator(withKnobs);
stories.add('Grid View for Asset Card', () => {
  return (
    <GridView>
      <GridViewItem>
        <CardView
          title={data.title}
          subtitle={data.subtitle}
          image={data.image}
          linkTag={data.linkTag}
          to={data.to}
          onClick={action('card-click')}
        />
      </GridViewItem>
      <GridViewItem>
        <CardView
          title={data.title}
          subtitle={data.subtitle}
          image={data.image}
          linkTag={data.linkTag}
          to={data.to}
          onClick={action('card-click')}
        />
      </GridViewItem>
      <GridViewItem>
        <CardView
          title={data.title}
          subtitle={data.subtitle}
          image={data.image}
          linkTag={data.linkTag}
          to={data.to}
          onClick={action('card-click')}
        />
      </GridViewItem>
      <GridViewItem>
        <CardView
          title={data.title}
          subtitle={data.subtitle}
          image={data.image}
          linkTag={data.linkTag}
          to={data.to}
          onClick={action('card-click')}
        />
      </GridViewItem>
    </GridView>
  );
});

