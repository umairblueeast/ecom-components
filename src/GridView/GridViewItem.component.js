/**
 * Created by amna on 12/7/16.
 */

import React, { PropTypes } from 'react';

const propTypes = {
  tag: PropTypes.string
};

const defaultProps = {
  tag: 'div'
};

const GridViewItem = (props) => {

  const {
    tag: Tag,
    ...attributes
  } = props;

  return (
    <Tag {...attributes} className="grid-view-item" />
  );
};

GridViewItem.propTypes = propTypes;
GridViewItem.defaultProps = defaultProps;

export default GridViewItem;
