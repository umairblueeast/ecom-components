/**
 * Created by amna on 12/7/16.
 */

import React, { PropTypes } from 'react';

import 'bluerain-bootstrap-theme/dist/css/components/grid-view.css';


const propTypes = {
  tag: PropTypes.string
};

const defaultProps = {
  tag: 'div'
};

const GridView = (props) => {

  const {
    tag: Tag,
    ...attributes
  } = props;

  return (
    <Tag {...attributes} className="grid-view" />
  );
};

GridView.propTypes = propTypes;
GridView.defaultProps = defaultProps;

export default GridView;
