/**
 * Created by anser on 12/22/16.
 */

import { Link } from 'react-router';
import React, { PropTypes } from 'react';
import ListView from './ListView';
import ListViewItem from './ListViewItem';
import ListViewItemLabel from './ListViewItemLabel';
import ListViewItemImage from './ListViewItemImage';
import Image from './Image';


const propTypes = {
  myData: PropTypes.array,
};

const defaultProps = {
  myData: []
};

const ListViewContainer = (props) => {
  const {
    myData
  } = props;

  return (
    <ListView>
      {
        myData.map((item, i) => {
          let image;
          if (item.imageSRC) {
            image = (
              <ListViewItemImage>
                <Image src={item.imageSRC} />
              </ListViewItemImage>
            );
          }
          return (
            <ListViewItem key={i} tag={Link} to={item.to} action={item.action} chevronRight={item.chevronRight}>
              {image}
              <ListViewItemLabel>
                {item.label}
              </ListViewItemLabel>
            </ListViewItem>
          );
        })
      }
    </ListView>
  );
};

ListViewContainer.propTypes = propTypes;
ListViewContainer.defaultProps = defaultProps;

export default ListViewContainer;
