/**
 * Created by anser on 12/6/16.
 */

import React, { PropTypes } from 'react';
import { ListGroup } from 'reactstrap';

const propTypes = {
  tag: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
  className: PropTypes.string
};

const defaultProps = {
  tag: 'div'
};

const ListView = (props) => {
  const {
    tag: Tag,
    className,
    ...attributes
  } = props;

  return (
    <ListGroup {...attributes} className="list-group-plus" />
  );
};

ListView.propTypes = propTypes;
ListView.defaultProps = defaultProps;

export default ListView;
