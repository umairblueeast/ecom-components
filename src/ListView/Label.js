/**
 * Created by anser on 12/6/16.
 */


import React, { PropTypes } from 'react';
import { Media } from 'reactstrap';

const propTypes = {
  tag: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
  className: PropTypes.string
};

const defaultProps = {
  tag: 'span'
};

const Label = (props) => {
  const {
    tag: Tag,
    ...attributes
  } = props;

  return (
    <tag {...attributes} className="title" />
  );
};

Label.propTypes = propTypes;
Label.defaultProps = defaultProps;

export default Label;
