/**
 * Created by anser on 12/22/16.
 */

import React from 'react';
import { storiesOf } from '@kadira/storybook';
import { withKnobs, text, boolean, object } from '@kadira/storybook-addon-knobs';
import Image from './Image';

import ListViewContainer from './ListView.container';

const stories = storiesOf('ListView', module);
stories.addDecorator(withKnobs);
stories.add('Complete List View', () => {
  const label = text('Label', 'Air Conditioner');
  const image = text('Image', 'air-conditioner');
  const imageSRC =`https://s3-us-west-2.amazonaws.com/bluerainimages/${image}.svg`;
  const chevronRight = boolean('Right Navigation', true);
  const action = boolean('Action Item', true);
  const url = text('Navigation URL', '/app');
  const myData = object('Data', [
    {
      to: url,
      action: action,
      chevronRight: chevronRight,
      imageSRC: imageSRC,
      label: label
    }
  ])
  return (
    <ListViewContainer myData={myData} />
  );
});

stories.add('Without Navigation', () => {
  const label = text('Label', 'Air Conditioner');
  const image = text('Image', 'air-conditioner');
  const imageSRC =`https://s3-us-west-2.amazonaws.com/bluerainimages/${image}.svg`;
  const chevronRight = boolean('Right Navigation', false);
  const action = boolean('Action Item', true);
  const url = text('Navigation URL', '/app');
  const myData = object('Data', [
    {
      to: url,
      action: action,
      chevronRight: chevronRight,
      imageSRC: imageSRC,
      label: label
    }
  ])
  return (
    <ListViewContainer myData={myData} />
  );
});

stories.add('Without Device Icon', () => {
  const label = text('Label', 'Air Conditioner');
  const chevronRight = boolean('Right Navigation', true);
  const action = boolean('Action Item', true);
  const url = text('Navigation URL', '/app');
  const myData = object('Data', [
    {
      to: url,
      action: action,
      chevronRight: chevronRight,
      label: label
    }
  ])
  return (
    <ListViewContainer myData={myData} />
  );
});

stories.add('Without Label', () => {
  const image = text('Image', 'air-conditioner');
  const imageSRC =`https://s3-us-west-2.amazonaws.com/bluerainimages/${image}.svg`;
  const chevronRight = boolean('Right Navigation', true);
  const action = boolean('Action Item', true);
  const url = text('Navigation URL', '/app');
  const myData = object('Data', [
    {
      to: url,
      action: action,
      chevronRight: chevronRight,
      imageSRC: imageSRC,
    }
  ])
  return (
    <ListViewContainer myData={myData} />
  );
});


