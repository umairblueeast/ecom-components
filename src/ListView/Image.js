/**
 * Created by anser on 12/6/16.
 */


import React, { PropTypes } from 'react';
import { Media } from 'reactstrap';

const propTypes = {
  tag: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
  className: PropTypes.string
};

const defaultProps = {
  tag: 'Media'
};

const Image = (props) => {
  const {
    tag: Tag,
    src,
    ...attributes
  } = props;
  // const image = `https://s3-us-west-2.amazonaws.com/bluerainimages/${src}`;
  return (
    <Media {...attributes} src={src} object alt="Generic placeholder image" className="img-fluid img-rounded" />
  );
};

Image.propTypes = propTypes;
Image.defaultProps = defaultProps;

export default Image;
