/**
 * Created by anser on 12/6/16.
 */

import React, { PropTypes } from 'react';

const propTypes = {
  tag: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
  className: PropTypes.string
};

const defaultProps = {
  tag: 'div'
};

const ListViewItemLabel = (props) => {
  const {
    tag: Tag,
    className,
    ...attributes
  } = props;

  return (
    <Tag {...attributes} className="list-group-item-body" />
  );
};

ListViewItemLabel.propTypes = propTypes;
ListViewItemLabel.defaultProps = defaultProps;

export default ListViewItemLabel;
