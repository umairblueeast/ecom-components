/**
 * Created by anser on 12/6/16.
 */


import React, { PropTypes } from 'react';

const propTypes = {
  tag: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
  className: PropTypes.string
};

const defaultProps = {
  tag: 'div'
};

const ListViewItemImage = (props) => {
  const {
    tag: Tag,
    ...attributes
  } = props;

  return (
    <Tag {...attributes} className="list-group-item-img" />
  );
};

ListViewItemImage.propTypes = propTypes;
ListViewItemImage.defaultProps = defaultProps;

export default ListViewItemImage;
