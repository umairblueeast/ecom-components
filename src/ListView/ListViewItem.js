/**
 * Created by anser on 12/6/16.
 */

import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import { Icon } from 'react-fa';
import classNames from 'classnames';
import { ListGroupItem } from 'reactstrap';

const propTypes = {
  tag: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
  className: PropTypes.string
};

const defaultProps = {
  tag: 'li'
};

const ListViewItem = (props) => {
  const {
    chevronRight,
    action,
    children,
    url,
    ...attributes
  } = props;

  // Classes
  const className = classNames('', {
    'list-group-item-action': action
  }, props.className);

  // Chevron
  let chevron = '';
  if (chevronRight === true) {
    chevron = <div className="list-group-item-icon"><Icon name="chevron-right" /></div>;
  }

  return (
    <ListGroupItem {...attributes} className={className}>
      {children}
      {chevron}
    </ListGroupItem>
  );
};

ListViewItem.propTypes = propTypes;
ListViewItem.defaultProps = defaultProps;

export default ListViewItem;
