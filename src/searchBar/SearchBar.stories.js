/**
 * Created by saad bin saeed on 3/1/17.
 */
import React from 'react';
import { storiesOf } from '@kadira/storybook';
import SearchBarComponent from './SearchBar.component';

const stories = storiesOf('Search Bar', module);

stories.addWithInfo('Default Search Bar', () => (
  <SearchBarComponent
    onChange={value => console.log('Parent :: onChange :: Value In Story Component : ', value)}
    onSubmit={value => console.log('Parent :: onSubmit :: Value In Story Component : ', value)}
  />
));
