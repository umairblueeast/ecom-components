/**
 * Created by saad bin saeed on 3/1/17.
 */
import Icon from 'react-fa';
import React, { PropTypes } from 'react';
import './SearchBar.css';

const propTypes = {
  onChange: PropTypes.func,
  onSubmit: PropTypes.func,
};

const defaultProps = {
  onChange: value => console.log('Default onchange function have been called : ', value),
  onSubmit: value => console.log('Default onSubmit function have been called : ', value),
};

class SearchBarComponent extends React.Component {

  constructor() {
    super();
    this.state = {
      value: '',
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }


  handleChange(event) {
    const { onChange } = this.props;
    onChange(event.target.value);
    this.setState({
      value: event.target.value
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    const { onSubmit } = this.props;
    onSubmit(this.state.value);
  }

  render() {
    return (
      <div id="custom-search-input">
        <form onSubmit={this.handleSubmit}>
          <div className="input-group">
            <input
              type="text" className="form-control input-lg" placeholder="Search here..."
              onChange={this.handleChange}
            />
            <span className="input-group-btn">
              <button type="submit" className="btn btn-info btn-lg">
                <Icon className="search-icon" name="search" />
              </button>
            </span>
          </div>
        </form>
      </div>
    );
  }

}

SearchBarComponent.propTypes = propTypes;
SearchBarComponent.defaultProps = defaultProps;

export default SearchBarComponent;
