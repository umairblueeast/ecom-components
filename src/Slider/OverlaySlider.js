/**
 * Created by atif on 12/8/16.
 */
import React, { PropTypes } from 'react';
import 'bluerain-bootstrap-theme/dist/css/components/overlay-slider.css';

const propTypes = {
  children : PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  settings : PropTypes.object
};
const defaultPropTypes = {
};

export default class OverlaySlider extends React.Component {

  render() {
    return (
      <div className="overlay-slider">
        <div className="slider-container">
          {this.props.children}
        </div>
      </div>
    );
  }
}

OverlaySlider.propTypes = propTypes;
OverlaySlider.defaultProps = defaultPropTypes;
