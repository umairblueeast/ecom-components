/**
 * Created by saad bin saeed on 2/28/17.
 */
import React from 'react';
import { storiesOf } from '@kadira/storybook';

import IconComponent from './Icon.component';


const stories = storiesOf('Icon', module);


stories.addWithInfo('Default Icon', () => (
  <IconComponent  />
));

stories.addWithInfo('Gradient Background', () => (
  <IconComponent
    gradient
    backgroundColors={['red', 'orange']}
  />
));

stories.addWithInfo('Image Background', () => (
  <IconComponent
    src="http://lorempixel.com/400/200/"
  />
));

stories.addWithInfo('Icon Changed', () => (
  <IconComponent
    name="star"
    gradient
    backgroundColors={['pink', 'red']}
  />
));
stories.addWithInfo('png icon', () => (
  <IconComponent
    png="https://thumb.ibb.co/e0yaWF/app_icon.png"
  />
));