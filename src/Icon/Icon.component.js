/**
 * Created by saad bin saeed on 2/28/17.
 */

import React, { PropTypes } from 'react';
import  tinycolor from 'tinycolor2';
import Icon from 'react-fa';



class IconComponent extends React.Component {

    static propTypes = {
        name: PropTypes.string,
        width: PropTypes.string,
        height: PropTypes.string,
        color: PropTypes.string,
        backgroundColors: PropTypes.array,
        gradient: PropTypes.bool,
        src: PropTypes.string
    };

    static defaultProps = {
        name: 'user',
        width: '120px',
        height: '120px',
        color: 'white',
        backgroundColors: ['black', 'yellow'],
        gradient: false,
        src : null,

    };

    render() {

        const {
            name,
            width,
            height,
            color,
            backgroundColors,
            gradient,
            src,
            png,
        } = this.props;

        let backgroundHexColor, iconGradient = 'linear-gradient(to right,';

        const iconHexColor = `${'#' + tinycolor(color).toHex()}`;
        const icon = {
            padding: '25px 0px',
        };

        const iconStyle = {
            width: width,
            height: height,
            borderRadius: '10px',
            color: iconHexColor,
            // display: "inline-block",
            textAlign: "center"
        };
        if(png){
            iconStyle.background = `${'url('+png +')'}`;
            iconStyle.backgroundSize = `${width + ' ' + height}`;
            return (
                <div style={iconStyle}>
                </div>
            );
        }

        if(src){
            iconStyle.background = `${'url('+src +')'}`;
        }else{
            if (gradient){
                backgroundColors.forEach((color) => {
                    iconGradient += color;
                    iconGradient += ',';
                });
                iconGradient = iconGradient.substring(0, iconGradient.length - 1);
                iconGradient += ')';
            }else {
                backgroundHexColor = `${'#' + tinycolor(backgroundColors[0]).toHex()}`;
            }
            iconStyle.background = gradient ? iconGradient : backgroundHexColor;

        }

        return (
            <div style={iconStyle}>
                <Icon size="5x" style={icon} name={name} />
            </div>
        );
    }

}
export default IconComponent;
