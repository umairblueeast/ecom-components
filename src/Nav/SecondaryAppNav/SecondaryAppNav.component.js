/**
 * Created by atif on 12/8/16.
 */

import React, { PropTypes } from 'react';
import 'bluerain-bootstrap-theme/dist/css/layouts/system-layout.css';
import Navbar from 'reactstrap/lib/Navbar';
import NavItem from 'reactstrap/lib/NavItem';
import { Link } from 'react-router';

const propTypes = {
  items : PropTypes.array
};

const defaultProps = {
  items : []
};

const SecondaryAppNav = (props) => {
  const {
    items
  } = props;

  return (
    <Navbar className="app-navbar secondary-nav navbar-light">
      <ul className="nav navbar-nav">
        { items.map((item, i) => {
          return (
            <NavItem className={item.class} key={i}>
              <Link to={item.url} className="nav-link" >{item.label}</Link>
            </NavItem>
          );
        })}
      </ul>
    </Navbar>
  );
};

SecondaryAppNav.propTypes = propTypes;
SecondaryAppNav.defaultProps = defaultProps;

export default SecondaryAppNav;
