import React from 'react';
import { storiesOf } from '@kadira/storybook';

import SecondaryAppNav from '../SecondaryAppNav.component';


const stories = storiesOf('Secondary App Nav', module);
const SecondaryAppNavData = [
  {
    label: 'Overview',
    url: '/home',
    class: 'active'
  },
  {
    label: 'Sites',
    url: '/home',
    class: ''
  },
  {
    label: 'Settings',
    url: '/home',
    class: ''

  },
];
// stories.addDecorator(withKnobs);

stories.addWithInfo('Simple Page Title', () => (
  <SecondaryAppNav items={SecondaryAppNavData} />
));
