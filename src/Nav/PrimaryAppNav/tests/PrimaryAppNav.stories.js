import React from 'react';
import { storiesOf } from '@kadira/storybook';

import PrimaryAppNav from '../PrimaryAppNav.component';


const stories = storiesOf('Primary App Nav', module);
// stories.addDecorator(withKnobs);

stories.addWithInfo('Simple Page Title', () => (
  <PrimaryAppNav title="Page Title" />
  ));

stories.addWithInfo('With Previous Page Link', () => (
  <PrimaryAppNav title="Page Title" previous={{ title: 'Home Page', url: '/home' }} />
  ));

stories.addWithInfo('With Page Icon', () => (
  <PrimaryAppNav
    title="Page Title"
    icon="https://s3-us-west-2.amazonaws.com/bluerainimages/swearing-male.svg"
  />
  ));

stories.addWithInfo('With Page Icon & Link', () => (
  <PrimaryAppNav
    title="Page Title"
    previous={{ title: 'Home Page', url: '/home' }}
    icon="https://s3-us-west-2.amazonaws.com/bluerainimages/swearing-male.svg"
  />
  ));

stories.addWithInfo('Toolbar', () => (
  <PrimaryAppNav
    title="Page Title"
    previous={{ title: 'Home Page', url: '/home' }}
    icon="https://s3-us-west-2.amazonaws.com/bluerainimages/swearing-male.svg"
  >
    <div className="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
      <div className="btn-group" role="group" aria-label="First group">
        <button type="button" className="btn btn-secondary">1</button>
        <button type="button" className="btn btn-secondary">2</button>
      </div>
      <div className="btn-group" role="group" aria-label="Second group">
        <button type="button" className="btn btn-secondary">5</button>
      </div>
    </div>
  </PrimaryAppNav>
  ));

