import React, { PropTypes } from 'react';
// import { withSystemNav } from 'bluerain-client-services';
import Icon from 'react-fa';

const propTypes = {
  onClick: PropTypes.func
};

export default class MainMenuButton extends React.Component {
  constructor(props) {
    super(props);
    this.handleClickAction = this.handleClickAction.bind(this);
  }

  handleClickAction() {
    this.props.onClick();
  }

  render() {
    const style = {
      marginRight: 15
    };
    return (
      <button onClick={this.handleClickAction} style={style}>
        <Icon name="bars" />
      </button>
    );
  }
}

MainMenuButton.propTypes = propTypes;
