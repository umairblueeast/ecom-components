/**
 * Muhammad Atif
 */
import React, { PropTypes } from 'react';
import Icon from 'react-fa';

// Why???
import 'bluerain-bootstrap-theme/dist/css/layouts/system-layout.css';

import { NavbarBrand, Nav } from 'reactstrap';
import { Link } from 'react-router';
import MainMenuButton from './MainMenuButton.component';
const propTypes = {
  title : PropTypes.string,
  openMenuAction: PropTypes.func,
  previous : PropTypes.shape({
    title: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired
  }),
  icon : PropTypes.string,
  toggleMenu: PropTypes.func
};

const defaultProps = {
  title : null,
  previous : null
};

export default class PrimaryAppNav extends React.Component {

  showPrevious() {
    if (this.props.previous) {
      return (
        <span>
          <span className="prev-page">
            <Link to={this.props.previous.url}>{this.props.previous.title}</Link>
          </span>
          <span className="prev-page-sperator">/</span>
        </span>
      );
    }
  }

  showPageIcon() {
    if (this.props.icon) {
      return (
        <span className="page-icon">
          <img src={this.props.icon} width="30" height="30" className="rounded" alt="Page Icon" />
        </span>
      );
    }
  }

  showNavIcon() {
    if (this.props.previous) {
      const style = {
        marginRight: 15
      };
      return (
        <Link to={this.props.previous.url} className="btn btn-secondary" style={style}>
          <Icon name="arrow-left" />
        </Link>
      );
    }
    return <MainMenuButton onClick={this.props.toggleMenu} />;
  }

  render() {
    return (
      <Nav tag="nav" className="app-navbar navbar-light">
        <div className="navbar primary-nav">
          <div className="hidden-sm-up">
            {this.showNavIcon()}
          </div>
          <NavbarBrand className="text-truncate text-xs-left text-sm-left" tag="div">
            <span className="hidden-xs-down">
              {this.showPageIcon()}{this.showPrevious()}
            </span>
            <span className="page-title">{this.props.title}</span>
          </NavbarBrand>
          <div className="navbar-actions">
            {this.props.children}
          </div>
        </div>
      </Nav>
    );
  }
}

PrimaryAppNav.propTypes = propTypes;
PrimaryAppNav.defaultProps = defaultProps;
