/**
 * Created by amna on 1/4/17.
 */

import { Link } from 'react-router';
const data = {
  title: 'Some title',
  color: 'deep-orange',
  icon: 'sun-o',
  description: '<span><Icon name="clock-o" /> Last Activity 4 hours</span>',
  linkTag: Link,
  href: '/hrefPath',
  to: '/toPath',
  onClick() {
    console.log('click performed');
  }
};

module.exports = data;
