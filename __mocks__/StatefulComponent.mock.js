
export const loadedData = {
  loading: false,
  error: null,
  query: 'I am data'
};

export const noData = {
  loading: false,
  error: null,
  query: null
};

export const loadingData = {
  loading: true,
  error: null,
  query: null
};

export const errorData = {
  loading: false,
  error: new Error('Test Error'),
  query: null
};

export const loadingAndErrorData = {
  loading: true,
  error: new Error('Test Error'),
  query: null
};

export const customDataField = {
  loading: false,
  error: false,
  viewer: 'I\'m inside viewer'
};

export const customCheckDataFunctionBad = {
  loading: false,
  error: false,
  viewer: {
    sites: {}
  }
};

export const customCheckDataFunctionEmpty = {
  loading: false,
  error: false,
  viewer: {
    sites: {
      edges: []
    }
  }
};

export const customCheckDataFunctionFilled = {
  loading: false,
  error: false,
  viewer: {
    sites: {
      edges: [{
        node: {
          id: '1'
        }
      }, {
        node: {
          id: '2'
        }
      }, {
        node: {
          id: '3'
        }
      }]
    }
  }
};
