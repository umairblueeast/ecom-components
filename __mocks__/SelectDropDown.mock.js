
const data = [
  { value: 1, label: 'Option One', icon: 'https://s3-us-west-2.amazonaws.com/bluerainimages/remote.svg' },
  { value: 2, label: 'Option Two', icon: 'https://s3-us-west-2.amazonaws.com/bluerainimages/air-conditioner.svg' },
  { value: 3, label: 'Option Three', icon: 'https://s3-us-west-2.amazonaws.com/bluerainimages/remote.svg' },
  { value: 4, label: 'Option Four', icon: 'https://s3-us-west-2.amazonaws.com/bluerainimages/air-conditioner.svg' }
];

module.exports = data;
