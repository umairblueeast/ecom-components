import { Link } from 'react-router';
const graphData = [

  {
    x: '2016-06-03T09:14:51Z',
    y: 3,
  },
  {
    x: '2016-07-03T09:14:51Z',
    y: 6,
  },
  {
    x: '2016-08-03T09:14:51Z',
    y: 4,
  },
  {
    x: '2016-09-03T09:14:51Z',
    y: 8,
  },
  {
    x: '2016-10-03T09:14:51Z',
    y: 1,
  },
  {
    x: '2016-11-03T09:14:51Z',
    y: 3,
  },
];
const data = {
  label: 'Average Duration',
  value: '12.1 h',
  data:  graphData,
  linkTag: Link,
  href: '/hrefPath',
  to: '/toPath',
  onClick() {
    console.log('click performed');
  }
};

module.exports = data;
