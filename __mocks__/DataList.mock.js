/**
 * Created by anser on 1/18/17.
 */

import { Link } from 'react-router';

export const withConnectedStatus = {
  tag: Link,
  url: '/somePage',
  action: true,
  chevronRight: true,
  label: 'Status',
  value: 'Connected',
  iconClass: 'circle text-success'
};

export const withDisConnectedStatus = {
  tag: Link,
  url: '/somePage',
  action: true,
  chevronRight: true,
  label: 'Status',
  value: 'Disconnected',
  iconClass: 'circle text-danger'
};

export const withoutAction = {
  tag: Link,
  url: '/somePage',
  action: false,
  chevronRight: true,
  label: 'Status',
  value: 'Connected',
  iconClass: 'circle text-success'
};

export const wifiProtected = {
  tag: Link,
  url: '/somePage',
  action: true,
  chevronRight: true,
  label: 'Status',
  iconClass: 'lock'
};

export const withoutStatusIcon = {
  tag: Link,
  url: '/somePage',
  action: true,
  chevronRight: true,
  label: 'Brand',
  value: 'Orient'
};

export const withoutNavigation = {
  action: false,
  chevronRight: false,
  label: 'Model',
  value: 'Ultron white, 1.5 Ton'
};

export const withoutLabel = {
  tag: Link,
  url: '/somePage',
  action: true,
  chevronRight: true,
  value: 'Connected',
  iconClass: 'circle text-success'
};

export const withoutValue = {
  tag: Link,
  url: '/somePage',
  action: true,
  chevronRight: true,
  label: 'Status',
};
