/**
 * Created by amna on 12/23/16.
 */

export const dataWithRoundImage = {
  initiator: 'Amna Mukhtar',
  description: 'turned A.C off',
  time: '2016-11-03T09:14:51Z',
  userProfileLink: '/toUserProfile',
  source: 'Remote',
  image: 'https://s3-us-west-2.amazonaws.com/bluerainimages/default-user.jpg',
  roundImage: true
};

export const dataWithImage = {
  initiator: 'Amna Mukhtar',
  description: 'changed the temperature to 26 °C',
  time: '2016-11-03T09:14:51Z',
  source: 'Remote',
  image: 'https://s3-us-west-2.amazonaws.com/bluerainimages/default-user.jpg',
  roundImage: false
};
