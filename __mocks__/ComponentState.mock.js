/**
 * Created by amna on 1/5/17.
 */

import { Link } from 'react-router';
const data = {
  image: 'swearing-male.svg',
  imageSize: 'lg',
  title: 'Err..',
  description: 'Looks like something broke.',
  card: true,
  linkText: 'Learn More',
  linkTag: Link
};

module.exports = data;
