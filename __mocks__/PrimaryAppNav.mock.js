/**
 * Created by amna on 1/6/17.
 */

const data = {
  title: 'Page Title',
  previous: { title: 'Home Page', url: '/home' },
  icon: 'https://s3-us-west-2.amazonaws.com/bluerainimages/default-user.jpg',
};

module.exports = data;
