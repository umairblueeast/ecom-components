/**
 * Created by amna on 12/22/16.
 */

import { Link } from 'react-router';
const data = {
  title: 'Title',
  subtitle: 'Subtitle',
  image: 'https://s3-us-west-2.amazonaws.com/bluerainimages/default-user.jpg',
  linkTag: Link,
  href: '/path',
  to: '/path',
  onClick() {
    console.log('click performed');
  }
};

module.exports = data;
